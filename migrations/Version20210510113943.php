<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210510113943 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tenant DROP CONSTRAINT fk_4e59c462172e74b7');
        $this->addSql('DROP INDEX uniq_4e59c462172e74b7');
        $this->addSql('ALTER TABLE tenant DROP main_certificate_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE tenant ADD main_certificate_id INT NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD CONSTRAINT fk_4e59c462172e74b7 FOREIGN KEY (main_certificate_id) REFERENCES certificate (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_4e59c462172e74b7 ON tenant (main_certificate_id)');
    }
}
