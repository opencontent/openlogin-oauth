<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240512214332 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE federation_authority ADD trust_anchor VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE federation_authority ADD open_id_provider VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE relying_party_client ADD federation_authority_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE relying_party_client ADD trust_mark TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE relying_party_client ADD trust_mark_iss VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE relying_party_client ADD trust_mark_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE relying_party_client DROP authority_hint');
        $this->addSql('ALTER TABLE relying_party_client ADD CONSTRAINT FK_D795FA4A961504F6 FOREIGN KEY (federation_authority_id) REFERENCES federation_authority (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D795FA4A961504F6 ON relying_party_client (federation_authority_id)');
        $this->addSql('ALTER TABLE tenant ADD web_site_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE tenant ADD privacy_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE tenant ADD logo_uri VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE relying_party_client DROP CONSTRAINT FK_D795FA4A961504F6');
        $this->addSql('DROP INDEX IDX_D795FA4A961504F6');
        $this->addSql('ALTER TABLE relying_party_client ADD authority_hint VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE relying_party_client DROP federation_authority_id');
        $this->addSql('ALTER TABLE relying_party_client DROP trust_mark');
        $this->addSql('ALTER TABLE relying_party_client DROP trust_mark_iss');
        $this->addSql('ALTER TABLE relying_party_client DROP trust_mark_id');
        $this->addSql('ALTER TABLE federation_authority DROP trust_anchor');
        $this->addSql('ALTER TABLE federation_authority DROP open_id_provider');
        $this->addSql('ALTER TABLE tenant DROP web_site_uri');
        $this->addSql('ALTER TABLE tenant DROP privacy_uri');
        $this->addSql('ALTER TABLE tenant DROP logo_uri');
    }
}
