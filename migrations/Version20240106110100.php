<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240106110100 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE federation_authority_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE relying_party_client_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE federation_authority (id INT NOT NULL, name VARCHAR(255) NOT NULL, uri VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE relying_party_client (id INT NOT NULL, tenant_id INT NOT NULL, authority_hint VARCHAR(255) NOT NULL, requested_acr JSON NOT NULL, spid_user_attributes JSON NOT NULL, redirect_uri VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D795FA4A9033212A ON relying_party_client (tenant_id)');
        $this->addSql('ALTER TABLE relying_party_client ADD CONSTRAINT FK_D795FA4A9033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE tenant ADD is_pa BOOLEAN DEFAULT true NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD country VARCHAR(255) DEFAULT \'IT\' NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD locality VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE tenant ALTER open_login_uri DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE federation_authority_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE relying_party_client_id_seq CASCADE');
        $this->addSql('ALTER TABLE relying_party_client DROP CONSTRAINT FK_D795FA4A9033212A');
        $this->addSql('DROP TABLE federation_authority');
        $this->addSql('DROP TABLE relying_party_client');
        $this->addSql('ALTER TABLE tenant DROP is_pa');
        $this->addSql('ALTER TABLE tenant DROP country');
        $this->addSql('ALTER TABLE tenant DROP locality');
        $this->addSql('ALTER TABLE tenant ALTER open_login_uri SET DEFAULT \'missing\'');
    }
}
