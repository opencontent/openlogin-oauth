<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210510115334 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE assertion_consumer_service ADD tenant_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE assertion_consumer_service ADD CONSTRAINT FK_C6097AD9033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_C6097AD9033212A ON assertion_consumer_service (tenant_id)');
        $this->addSql('ALTER TABLE attribute_consuming_service ADD tenant_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE attribute_consuming_service ADD CONSTRAINT FK_787EC669033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_787EC669033212A ON attribute_consuming_service (tenant_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE attribute_consuming_service DROP CONSTRAINT FK_787EC669033212A');
        $this->addSql('DROP INDEX IDX_787EC669033212A');
        $this->addSql('ALTER TABLE attribute_consuming_service DROP tenant_id');
        $this->addSql('ALTER TABLE assertion_consumer_service DROP CONSTRAINT FK_C6097AD9033212A');
        $this->addSql('DROP INDEX IDX_C6097AD9033212A');
        $this->addSql('ALTER TABLE assertion_consumer_service DROP tenant_id');
    }
}
