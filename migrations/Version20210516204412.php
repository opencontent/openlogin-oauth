<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210516204412 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE test_idp_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE test_idp (id INT NOT NULL, name VARCHAR(255) NOT NULL, identifier VARCHAR(255) NOT NULL, metadata TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE tenant ADD idp_test_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tenant ADD enabled_idp_test BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE tenant ADD idp_test_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE tenant ADD CONSTRAINT FK_4E59C462678644C3 FOREIGN KEY (idp_test_id) REFERENCES test_idp (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_4E59C462678644C3 ON tenant (idp_test_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE tenant DROP CONSTRAINT FK_4E59C462678644C3');
        $this->addSql('DROP SEQUENCE test_idp_id_seq CASCADE');
        $this->addSql('DROP TABLE test_idp');
        $this->addSql('DROP INDEX IDX_4E59C462678644C3');
        $this->addSql('ALTER TABLE tenant DROP idp_test_id');
        $this->addSql('ALTER TABLE tenant DROP enabled_idp_test');
        $this->addSql('ALTER TABLE tenant DROP idp_test_name');
    }
}
