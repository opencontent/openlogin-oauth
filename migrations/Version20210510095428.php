<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210510095428 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE assertion_consumer_service_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE attribute_consuming_service_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE certificate_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE single_logout_service_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE assertion_consumer_service (id INT NOT NULL, tenant_id INT DEFAULT NULL, index INT NOT NULL, url VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C6097AD9033212A ON assertion_consumer_service (tenant_id)');
        $this->addSql('CREATE TABLE attribute_consuming_service (id INT NOT NULL, tenant_id INT DEFAULT NULL, display_name VARCHAR(255) DEFAULT NULL, index INT NOT NULL, gender BOOLEAN DEFAULT NULL, company_name BOOLEAN DEFAULT NULL, registered_office BOOLEAN DEFAULT NULL, fiscal_number BOOLEAN DEFAULT NULL, iva_code BOOLEAN DEFAULT NULL, id_card BOOLEAN DEFAULT NULL, spid_code BOOLEAN DEFAULT NULL, name BOOLEAN DEFAULT NULL, family_name BOOLEAN DEFAULT NULL, place_of_birth BOOLEAN DEFAULT NULL, county_of_birth BOOLEAN DEFAULT NULL, date_of_birth BOOLEAN DEFAULT NULL, mobile_phone BOOLEAN DEFAULT NULL, email BOOLEAN DEFAULT NULL, address BOOLEAN DEFAULT NULL, expiration_date BOOLEAN DEFAULT NULL, digital_address BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_787EC669033212A ON attribute_consuming_service (tenant_id)');
        $this->addSql('CREATE TABLE certificate (id INT NOT NULL, index INT NOT NULL, key TEXT DEFAULT NULL, certificate TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE single_logout_service (id INT NOT NULL, tenant_id INT DEFAULT NULL, index INT DEFAULT NULL, url VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1E1AA86E9033212A ON single_logout_service (tenant_id)');
        $this->addSql('ALTER TABLE assertion_consumer_service ADD CONSTRAINT FK_C6097AD9033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE attribute_consuming_service ADD CONSTRAINT FK_787EC669033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE single_logout_service ADD CONSTRAINT FK_1E1AA86E9033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE tenant ADD main_certificate_id INT NOT NULL');
        $this->addSql('ALTER TABLE tenant DROP key');
        $this->addSql('ALTER TABLE tenant DROP main_certificate');
        $this->addSql('ALTER TABLE tenant DROP additional_certificates');
        $this->addSql('ALTER TABLE tenant DROP assertion_consumer_services');
        $this->addSql('ALTER TABLE tenant DROP attribute_consuming_services');
        $this->addSql('ALTER TABLE tenant DROP single_logout_services');
        $this->addSql('ALTER TABLE tenant ALTER comparison DROP NOT NULL');
        $this->addSql('ALTER TABLE tenant ALTER comparison TYPE VARCHAR(50)');
        $this->addSql('ALTER TABLE tenant ADD CONSTRAINT FK_4E59C462172E74B7 FOREIGN KEY (main_certificate_id) REFERENCES certificate (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4E59C462172E74B7 ON tenant (main_certificate_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE tenant DROP CONSTRAINT FK_4E59C462172E74B7');
        $this->addSql('DROP SEQUENCE assertion_consumer_service_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE attribute_consuming_service_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE certificate_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE single_logout_service_id_seq CASCADE');
        $this->addSql('DROP TABLE assertion_consumer_service');
        $this->addSql('DROP TABLE attribute_consuming_service');
        $this->addSql('DROP TABLE certificate');
        $this->addSql('DROP TABLE single_logout_service');
        $this->addSql('DROP INDEX UNIQ_4E59C462172E74B7');
        $this->addSql('ALTER TABLE tenant ADD key TEXT NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD main_certificate TEXT NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD additional_certificates JSON NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD assertion_consumer_services JSON NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD attribute_consuming_services JSON NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD single_logout_services JSON NOT NULL');
        $this->addSql('ALTER TABLE tenant DROP main_certificate_id');
        $this->addSql('ALTER TABLE tenant ALTER comparison SET NOT NULL');
        $this->addSql('ALTER TABLE tenant ALTER comparison TYPE VARCHAR(255)');
    }
}
