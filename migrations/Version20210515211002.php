<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210515211002 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tenant ADD open_login_uri VARCHAR(255) NOT NULL DEFAULT \'missing\'');
        $this->addSql('ALTER TABLE tenant ALTER fiscal_code DROP NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4E59C462570D17CA ON tenant (open_login_uri)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX UNIQ_4E59C462570D17CA');
        $this->addSql('ALTER TABLE tenant DROP open_login_uri');
        $this->addSql('ALTER TABLE tenant ALTER fiscal_code SET NOT NULL');
    }
}
