<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210510114231 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE assertion_consumer_service DROP CONSTRAINT fk_c6097ad9033212a');
        $this->addSql('DROP INDEX idx_c6097ad9033212a');
        $this->addSql('ALTER TABLE assertion_consumer_service DROP tenant_id');
        $this->addSql('ALTER TABLE attribute_consuming_service DROP CONSTRAINT fk_787ec669033212a');
        $this->addSql('DROP INDEX idx_787ec669033212a');
        $this->addSql('ALTER TABLE attribute_consuming_service DROP tenant_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE attribute_consuming_service ADD tenant_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE attribute_consuming_service ADD CONSTRAINT fk_787ec669033212a FOREIGN KEY (tenant_id) REFERENCES tenant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_787ec669033212a ON attribute_consuming_service (tenant_id)');
        $this->addSql('ALTER TABLE assertion_consumer_service ADD tenant_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE assertion_consumer_service ADD CONSTRAINT fk_c6097ad9033212a FOREIGN KEY (tenant_id) REFERENCES tenant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_c6097ad9033212a ON assertion_consumer_service (tenant_id)');
    }
}
