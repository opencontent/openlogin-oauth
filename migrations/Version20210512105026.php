<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210512105026 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE spid_session_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE spid_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE spid_session (id INT NOT NULL, spid_user_id INT DEFAULT NULL, session_id VARCHAR(255) NOT NULL, level INT NOT NULL, idp VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, attributes JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3B70D34A4C5AD50B ON spid_session (spid_user_id)');
        $this->addSql('CREATE TABLE spid_user (id INT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_18A86F1BF85E0677 ON spid_user (username)');
        $this->addSql('ALTER TABLE spid_session ADD CONSTRAINT FK_3B70D34A4C5AD50B FOREIGN KEY (spid_user_id) REFERENCES spid_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE spid_session DROP CONSTRAINT FK_3B70D34A4C5AD50B');
        $this->addSql('DROP SEQUENCE spid_session_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE spid_user_id_seq CASCADE');
        $this->addSql('DROP TABLE spid_session');
        $this->addSql('DROP TABLE spid_user');
    }
}
