<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240519143840 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE certificate ADD relying_party_client_federation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE certificate ADD relying_party_client_core_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE certificate ADD CONSTRAINT FK_219CDA4AA404B500 FOREIGN KEY (relying_party_client_federation_id) REFERENCES relying_party_client (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE certificate ADD CONSTRAINT FK_219CDA4AB21FE243 FOREIGN KEY (relying_party_client_core_id) REFERENCES relying_party_client (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_219CDA4AA404B500 ON certificate (relying_party_client_federation_id)');
        $this->addSql('CREATE INDEX IDX_219CDA4AB21FE243 ON certificate (relying_party_client_core_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE certificate DROP CONSTRAINT FK_219CDA4AA404B500');
        $this->addSql('ALTER TABLE certificate DROP CONSTRAINT FK_219CDA4AB21FE243');
        $this->addSql('DROP INDEX IDX_219CDA4AA404B500');
        $this->addSql('DROP INDEX IDX_219CDA4AB21FE243');
        $this->addSql('ALTER TABLE certificate DROP relying_party_client_federation_id');
        $this->addSql('ALTER TABLE certificate DROP relying_party_client_core_id');
    }
}
