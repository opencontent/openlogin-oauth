<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210510084726 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tenant ADD organization_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD ipa_code VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD fiscal_code VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD email VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD phone VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD key TEXT NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD main_certificate TEXT NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD additional_certificates JSON NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD comparison VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD assertion_consumer_services JSON NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD attribute_consuming_services JSON NOT NULL');
        $this->addSql('ALTER TABLE tenant ADD single_logout_services JSON NOT NULL');
        $this->addSql('ALTER TABLE tenant RENAME COLUMN name TO display_name');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE tenant ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE tenant DROP display_name');
        $this->addSql('ALTER TABLE tenant DROP organization_name');
        $this->addSql('ALTER TABLE tenant DROP ipa_code');
        $this->addSql('ALTER TABLE tenant DROP fiscal_code');
        $this->addSql('ALTER TABLE tenant DROP email');
        $this->addSql('ALTER TABLE tenant DROP phone');
        $this->addSql('ALTER TABLE tenant DROP key');
        $this->addSql('ALTER TABLE tenant DROP main_certificate');
        $this->addSql('ALTER TABLE tenant DROP additional_certificates');
        $this->addSql('ALTER TABLE tenant DROP comparison');
        $this->addSql('ALTER TABLE tenant DROP assertion_consumer_services');
        $this->addSql('ALTER TABLE tenant DROP attribute_consuming_services');
        $this->addSql('ALTER TABLE tenant DROP single_logout_services');
    }
}
