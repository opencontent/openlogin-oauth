FROM registry.gitlab.com/opencity-labs/php-caddy-prometheus:7.4-4.33.4

COPY Caddyfile /etc/caddy/Caddyfile

USER root

RUN apk update && apk upgrade openssl

RUN apk add --no-cache jq httpie gmp

# allow php to terminate gracefully during deployments
# https://www.goetas.com/blog/traps-on-the-way-of-blue-green-deployments/
RUN sed -i 's/;error_log = log\/php-fpm.log/error_log = \/proc\/self\/fd\/2/' /usr/local/etc/php-fpm.conf
RUN sed -i 's/;process_control_timeout = 0/process_control_timeout = 1m/' /usr/local/etc/php-fpm.conf
RUN sed -i 's/^access.log = \/proc\/self\/fd\/2/access.log = \/dev\/null/' /usr/local/etc/php-fpm.d/docker.conf

# Add utility to check healthness of php-fpm
ENV FCGI_STATUS_PATH=/php-status PHP_FPM_PM_STATUS_PATH=/php-status PHP_FPM_CLEAR_ENV=no
RUN curl https://raw.githubusercontent.com/renatomefi/php-fpm-healthcheck/master/php-fpm-healthcheck > /usr/local/bin/php-fpm-healthcheck && \
    chmod +x /usr/local/bin/php-fpm-healthcheck

RUN curl https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh> /usr/local/bin/wait-for-it && \
    chmod +x /usr/local/bin/wait-for-it


USER wodby
#ARG APP_ENV=prod
ENV PHP_FPM_USER=wodby
ENV PHP_FPM_GROUP=wodby

WORKDIR /var/www/html

COPY --chown=wodby:wodby ./ .

RUN mkdir -p var && chown wodby:wodby var -R

RUN composer install --prefer-dist --no-progress --no-interaction

RUN php bin/console update-idp-metadata

# il container wodby/php prevede una dir dove mettere script lanciati prima di far partire PHP-FPM
COPY docker/app/init.d/*.sh /docker-entrypoint-init.d/
