--
-- PostgreSQL database dump
--

-- Dumped from database version 13.10
-- Dumped by pg_dump version 13.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: admin; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.admin (
    id integer NOT NULL,
    username character varying(180) NOT NULL,
    roles json NOT NULL,
    password character varying(255) NOT NULL
);


--
-- Name: admin_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.admin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: assertion_consumer_service; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.assertion_consumer_service (
    id integer NOT NULL,
    tenant_id integer,
    index integer NOT NULL,
    url character varying(255) NOT NULL
);


--
-- Name: assertion_consumer_service_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.assertion_consumer_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: attribute_consuming_service; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.attribute_consuming_service (
    id integer NOT NULL,
    tenant_id integer,
    display_name character varying(255) DEFAULT NULL::character varying,
    index integer NOT NULL,
    gender boolean,
    company_name boolean,
    registered_office boolean,
    fiscal_number boolean,
    iva_code boolean,
    id_card boolean,
    spid_code boolean,
    name boolean,
    family_name boolean,
    place_of_birth boolean,
    county_of_birth boolean,
    date_of_birth boolean,
    mobile_phone boolean,
    email boolean,
    address boolean,
    expiration_date boolean,
    digital_address boolean
);


--
-- Name: attribute_consuming_service_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.attribute_consuming_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: certificate; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.certificate (
    id integer NOT NULL,
    tenant_id integer,
    index integer NOT NULL,
    key text,
    certificate text NOT NULL
);


--
-- Name: certificate_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.certificate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


--
-- Name: oauth2_access_token; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth2_access_token (
    identifier character(80) NOT NULL,
    client character varying(32) NOT NULL,
    expiry timestamp(0) without time zone NOT NULL,
    user_identifier character varying(128) DEFAULT NULL::character varying,
    scopes text,
    revoked boolean NOT NULL
);


--
-- Name: COLUMN oauth2_access_token.expiry; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.oauth2_access_token.expiry IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN oauth2_access_token.scopes; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.oauth2_access_token.scopes IS '(DC2Type:oauth2_scope)';


--
-- Name: oauth2_authorization_code; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth2_authorization_code (
    identifier character(80) NOT NULL,
    client character varying(32) NOT NULL,
    expiry timestamp(0) without time zone NOT NULL,
    user_identifier character varying(128) DEFAULT NULL::character varying,
    scopes text,
    revoked boolean NOT NULL
);


--
-- Name: COLUMN oauth2_authorization_code.expiry; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.oauth2_authorization_code.expiry IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN oauth2_authorization_code.scopes; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.oauth2_authorization_code.scopes IS '(DC2Type:oauth2_scope)';


--
-- Name: oauth2_client; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth2_client (
    identifier character varying(32) NOT NULL,
    name character varying(128) NOT NULL,
    secret character varying(128) DEFAULT NULL::character varying,
    redirect_uris text,
    grants text,
    scopes text,
    active boolean NOT NULL,
    allow_plain_text_pkce boolean DEFAULT false NOT NULL
);


--
-- Name: COLUMN oauth2_client.redirect_uris; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.oauth2_client.redirect_uris IS '(DC2Type:oauth2_redirect_uri)';


--
-- Name: COLUMN oauth2_client.grants; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.oauth2_client.grants IS '(DC2Type:oauth2_grant)';


--
-- Name: COLUMN oauth2_client.scopes; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.oauth2_client.scopes IS '(DC2Type:oauth2_scope)';


--
-- Name: oauth2_refresh_token; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth2_refresh_token (
    identifier character(80) NOT NULL,
    access_token character(80) DEFAULT NULL::bpchar,
    expiry timestamp(0) without time zone NOT NULL,
    revoked boolean NOT NULL
);


--
-- Name: COLUMN oauth2_refresh_token.expiry; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.oauth2_refresh_token.expiry IS '(DC2Type:datetime_immutable)';


--
-- Name: single_logout_service; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.single_logout_service (
    id integer NOT NULL,
    tenant_id integer,
    index integer,
    url character varying(255) NOT NULL,
    is_redirect boolean
);


--
-- Name: single_logout_service_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.single_logout_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: spid_session; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.spid_session (
    id integer NOT NULL,
    spid_user_id integer,
    session_id character varying(255) NOT NULL,
    level integer NOT NULL,
    idp character varying(255) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    attributes json NOT NULL
);


--
-- Name: spid_session_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.spid_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: spid_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.spid_user (
    id integer NOT NULL,
    username character varying(180) NOT NULL,
    roles json NOT NULL
);


--
-- Name: spid_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.spid_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tenant; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tenant (
    id integer NOT NULL,
    entity_id character varying(255) NOT NULL,
    display_name character varying(255) NOT NULL,
    organization_name character varying(255) NOT NULL,
    ipa_code character varying(255) NOT NULL,
    fiscal_code character varying(255),
    email character varying(255) NOT NULL,
    phone character varying(255) NOT NULL,
    comparison character varying(50) DEFAULT NULL::character varying,
    spid_level integer,
    login_index integer,
    open_login_uri character varying(255) DEFAULT 'missing'::character varying NOT NULL,
    custom_metadata text,
    idp_test_id integer
);


--
-- Name: tenant_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tenant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: test_idp; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.test_idp (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    identifier character varying(255) NOT NULL,
    metadata text NOT NULL
);


--
-- Name: test_idp_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.test_idp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Data for Name: admin; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.admin (id, username, roles, password) FROM stdin;
1	admin	["ROLE_ADMIN"]	$2y$13$UBTAgZ3dbxZOCtIRF.kN6eAWMJI36JSsQXiBuJmRt6uIRrzk//6C.
\.


--
-- Data for Name: assertion_consumer_service; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.assertion_consumer_service (id, tenant_id, index, url) FROM stdin;
1	1	0	http://auth.localtest.me/acs
\.


--
-- Data for Name: attribute_consuming_service; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.attribute_consuming_service (id, tenant_id, display_name, index, gender, company_name, registered_office, fiscal_number, iva_code, id_card, spid_code, name, family_name, place_of_birth, county_of_birth, date_of_birth, mobile_phone, email, address, expiration_date, digital_address) FROM stdin;
1	1	Default	0	t	t	t	t	t	t	t	t	t	t	t	t	t	t	t	t	t
\.


--
-- Data for Name: certificate; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.certificate (id, tenant_id, index, key, certificate) FROM stdin;
1	1	0	-----BEGIN PRIVATE KEY-----\r\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC0MRT/ZlnFl5Fx\r\nfp0Zes0wyJnk34HD4bd/cWNjFBvFpZ7e/BgPfwEcuTggQVrhH1z/lmhcP10s7Glp\r\nOibl7VpB8c8iHvI7V2TWPpGknrS1/J2Y1S34kmP/IuZiDKm+LwwuJmyFgvfhjVZq\r\nNMHqjCHI7KuzSar8c4O+Ue2Ejubruhc4i6FORQnlIcoyv6DBazJsqMt18y039ePM\r\nlVTzLTOni5iVN1Imx6eC8YAxb43ATM/B+KWUWx41Lc/CmGR3UErE2Zu12d9MPyxv\r\ni2dPFtbOG0QSabc99zQ56kBK9EQzAd9SS1mG0l4MFih+hglMGNKqF4NvSc+R1/x3\r\nHF+odEyLAgMBAAECggEBALIXVJ58hTuCazCWKCUXtxeBHIEV2oZEX30pD0bMiApX\r\nf+b5l7oxF2NROMTAc1bPDbZa7LhHtAqDekeTPwC/DxbgLI2XsmeuH66UVpQye37m\r\ntshn4jCsVwLCdprv0xByJYKzXUyzvdUCQSdlD0eGyplwON9Rbxpl/Q6soKkEJ/1P\r\n3V5pntY7X+smWt7hFmFwkv1gjQXfYjXzbTolw/hwu5/PtcxUgzTSkHCnumeFURW8\r\n1/5OLvjK2aTy/0oy1OEFOHBXnMrht9Duw0xFknvIBKJwKUHPE938FVmZlbb9LQ+2\r\nEI9PYk4SCZTVDj/pA2sEerYHW53d1+/kuVbiiSkpeYECgYEA3608rPILXMSdPjjJ\r\ntoTbzakZ7GL/tlSATVTFeS95rjV4sG6KwjamC5id6EJBeTM1sdwe1j8+RkW3Cvmw\r\nU4jsS7HM7GCcxl6KgRhaXSr+avDfQpvfhmvcj4AaBE1nxcdTF5XVTSmscBp3Vfrj\r\nEh03j411Loe8wdyrrsRgV7FMQsECgYEAzjsmDTNo0HhVAD9EhkUV2RJB5DzrI/8R\r\nPDEsVECbF5IM4JIKQKR1TBUrHTo0+X5kYXMJbv8CZKoaZj3tubicnFo/16v5GI3w\r\nqevteQEq0UcJjWSgd4nQO5zX9LB22uBbZfKhjmxU1xuwklVh18IwakvqLmeULSIZ\r\nSs1+6sVyPksCgYBBtO5CD8hutANNJ/l5eIXpYGeRHXPo5+HY1bI860lfd63AhniU\r\nnOEiPu8EL5J0PCrl2LfmUCXHFuuyea7mmy5u/ggo2Ci1zUf6rGC/RcX/PEsZkhiT\r\nsFPWg9cgtGxrmSY7o3RJepiBRgUQaOcThymvLerihFVazaxRZra2jArPAQKBgE6V\r\nsWlEVsNhpxpCAY2ck7e0wlEDeaKs1ld2cWrld3Pi3m9QlwGM6XmFc3lercMIi52q\r\npUSbfcD4JLkjDgxAMcVOipdXW8MkizlkwofijvtSM2yilJcYd7XPjWcbOx8qcQmH\r\nDFLmKuoRDhak3HTH9YT7yo/GPmt4W+TqK1uww1jDAoGBANfhvhkORwqASztccxDJ\r\nHEKYvfMvkwvuRVnf3xNVT8CwkSIxKGAI3BxObUDRLZAV6QAvWorhHdVyZcOSOWdI\r\nWNxRPQxdZ96rmF+vRbT80ByJt3Ak+wN2g/Wy0poEki2vIi1QgFqlkSzpwJDAWkV2\r\nlN1D/sQHGgULFXj/0aBLROgp\r\n-----END PRIVATE KEY-----	-----BEGIN CERTIFICATE-----\r\nMIICljCCAX4CCQD8y5XqdgTXijANBgkqhkiG9w0BAQsFADANMQswCQYDVQQGEwJJ\r\nVDAeFw0yMzAzMTMwOTQ3NDZaFw0yMzA0MTIwOTQ3NDZaMA0xCzAJBgNVBAYTAklU\r\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtDEU/2ZZxZeRcX6dGXrN\r\nMMiZ5N+Bw+G3f3FjYxQbxaWe3vwYD38BHLk4IEFa4R9c/5ZoXD9dLOxpaTom5e1a\r\nQfHPIh7yO1dk1j6RpJ60tfydmNUt+JJj/yLmYgypvi8MLiZshYL34Y1WajTB6owh\r\nyOyrs0mq/HODvlHthI7m67oXOIuhTkUJ5SHKMr+gwWsybKjLdfMtN/XjzJVU8y0z\r\np4uYlTdSJsengvGAMW+NwEzPwfillFseNS3Pwphkd1BKxNmbtdnfTD8sb4tnTxbW\r\nzhtEEmm3Pfc0OepASvREMwHfUktZhtJeDBYofoYJTBjSqheDb0nPkdf8dxxfqHRM\r\niwIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQBLXWxumnei2BVGNOC5K1so9GYh8FlJ\r\nr/ifv5MZC4XDkBFLUcdxIzngkDwq1KkzAdL1lvLCPBPTB6s6mnvRg0TGMpop9hHZ\r\nXsJKsuP+grdAJqsHoVYIbiC+lWIk3rg333djZpefumzcDSzgHwZTvSKz0kgOKei8\r\nqhtiQ2wdJeXdH4fKyPnpx26LJ3MPd1FHycZ4lXp9z32hekrQfyMIJKPa4+0nDQJr\r\nwi2jvq7CI6Ja9usR8r0aWaRdwZr1gQgN9IizeEbOBNMFvoLM9paKTbQWmbyzefKZ\r\nCAve/QpgAUV6ctjSr/lweIhm9whb4pX6Pah6xiUl8cYpXVlOnsEOhMub\r\n-----END CERTIFICATE-----
\.


--
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.doctrine_migration_versions (version, executed_at, execution_time) FROM stdin;
DoctrineMigrations\\Version20210508084923	\N	\N
DoctrineMigrations\\Version20210509093117	\N	\N
DoctrineMigrations\\Version20210509212357	\N	\N
DoctrineMigrations\\Version20210509212440	\N	\N
DoctrineMigrations\\Version20210510084726	\N	\N
DoctrineMigrations\\Version20210510095428	\N	\N
DoctrineMigrations\\Version20210510113943	\N	\N
DoctrineMigrations\\Version20210510114231	\N	\N
DoctrineMigrations\\Version20210510115334	\N	\N
DoctrineMigrations\\Version20210510115614	\N	\N
DoctrineMigrations\\Version20210510115817	\N	\N
DoctrineMigrations\\Version20210510120201	\N	\N
DoctrineMigrations\\Version20210510133159	\N	\N
DoctrineMigrations\\Version20210510141629	\N	\N
DoctrineMigrations\\Version20210512105026	2021-05-13 09:35:05	90
DoctrineMigrations\\Version20210515211002	2021-05-15 21:46:42	66
DoctrineMigrations\\Version20210515215955	2021-05-15 22:23:18	17
DoctrineMigrations\\Version20210516204412	2021-05-17 08:29:26	134
DoctrineMigrations\\Version20210517083353	2021-05-17 08:40:18	16
\.


--
-- Data for Name: oauth2_access_token; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.oauth2_access_token (identifier, client, expiry, user_identifier, scopes, revoked) FROM stdin;
\.


--
-- Data for Name: oauth2_authorization_code; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.oauth2_authorization_code (identifier, client, expiry, user_identifier, scopes, revoked) FROM stdin;
\.


--
-- Data for Name: oauth2_client; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.oauth2_client (identifier, name, secret, redirect_uris, grants, scopes, active, allow_plain_text_pkce) FROM stdin;
whoami	whoami	a2741e9ef38d8f5928fc740dffb4a59dc230ad335d3e49d7bc01301046f0b80ccc50d47de108c10dd403ec97fbd5f9458211ef537f1f2592f09946eead3ba80c	http://whoami.localtest.me/_oauth	\N	profile email	t	t
whoami-php	whoami-php	d7302782d331d3d41e56b21630848f43675d8d3e2d270969922354aae0e4fcc86deb46ba46e88b104e8b32de089c2a1368e3f6ca48bdcbbc306e4a47da11fd5e	http://whoami-php.localtest.me/_oauth	\N	profile email	t	f
\.


--
-- Data for Name: oauth2_refresh_token; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.oauth2_refresh_token (identifier, access_token, expiry, revoked) FROM stdin;
\.


--
-- Data for Name: single_logout_service; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.single_logout_service (id, tenant_id, index, url, is_redirect) FROM stdin;
1	1	0	http://auth.localtest.me/slo	f
\.


--
-- Data for Name: spid_session; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.spid_session (id, spid_user_id, session_id, level, idp, created_at, attributes) FROM stdin;
\.


--
-- Data for Name: spid_user; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.spid_user (id, username, roles) FROM stdin;
\.


--
-- Data for Name: tenant; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.tenant (id, entity_id, display_name, organization_name, ipa_code, fiscal_code, email, phone, comparison, spid_level, login_index, open_login_uri, custom_metadata, idp_test_id) FROM stdin;
1	http://auth.localtest.me/	openlogin	Ambiente di sviluppo di OpenLogin	opnlgn_1	OPNLGN77T05G224F	info@opencontent.it	+390461917437	\N	2	0	http://auth.localtest.me/	\N	1
\.


--
-- Data for Name: test_idp; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.test_idp (id, name, identifier, metadata) FROM stdin;
1	Local IDP	local	<md:EntityDescriptor xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata" entityID="http://spid.localtest.me"><md:IDPSSODescriptor protocolSupportEnumeration="urn:oasis:names:tc:SAML:2.0:protocol" WantAuthnRequestsSigned="true"><md:KeyDescriptor use="signing"><ds:KeyInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#"><ds:X509Data><ds:X509Certificate>MIICljCCAX4CCQD8y5XqdgTXijANBgkqhkiG9w0BAQsFADANMQswCQYDVQQGEwJJVDAeFw0yMzAzMTMwOTQ3NDZaFw0yMzA0MTIwOTQ3NDZaMA0xCzAJBgNVBAYTAklUMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtDEU/2ZZxZeRcX6dGXrNMMiZ5N+Bw+G3f3FjYxQbxaWe3vwYD38BHLk4IEFa4R9c/5ZoXD9dLOxpaTom5e1aQfHPIh7yO1dk1j6RpJ60tfydmNUt+JJj/yLmYgypvi8MLiZshYL34Y1WajTB6owhyOyrs0mq/HODvlHthI7m67oXOIuhTkUJ5SHKMr+gwWsybKjLdfMtN/XjzJVU8y0zp4uYlTdSJsengvGAMW+NwEzPwfillFseNS3Pwphkd1BKxNmbtdnfTD8sb4tnTxbWzhtEEmm3Pfc0OepASvREMwHfUktZhtJeDBYofoYJTBjSqheDb0nPkdf8dxxfqHRMiwIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQBLXWxumnei2BVGNOC5K1so9GYh8FlJr/ifv5MZC4XDkBFLUcdxIzngkDwq1KkzAdL1lvLCPBPTB6s6mnvRg0TGMpop9hHZXsJKsuP+grdAJqsHoVYIbiC+lWIk3rg333djZpefumzcDSzgHwZTvSKz0kgOKei8qhtiQ2wdJeXdH4fKyPnpx26LJ3MPd1FHycZ4lXp9z32hekrQfyMIJKPa4+0nDQJrwi2jvq7CI6Ja9usR8r0aWaRdwZr1gQgN9IizeEbOBNMFvoLM9paKTbQWmbyzefKZCAve/QpgAUV6ctjSr/lweIhm9whb4pX6Pah6xiUl8cYpXVlOnsEOhMub</ds:X509Certificate></ds:X509Data></ds:KeyInfo></md:KeyDescriptor><md:SingleLogoutService Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" Location="http://spid.localtest.me/slo"/><md:SingleLogoutService Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect" Location="http://spid.localtest.me/slo"/><md:NameIDFormat>urn:oasis:names:tc:SAML:2.0:nameid-format:transient</md:NameIDFormat><md:SingleSignOnService Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" Location="http://spid.localtest.me/sso"/><md:SingleSignOnService Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect" Location="http://spid.localtest.me/sso"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="spidCode"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="name"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="familyName"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="placeOfBirth"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="countyOfBirth"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="dateOfBirth"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="gender"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="companyName"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="registeredOffice"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="fiscalNumber"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="ivaCode"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="idCard"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="mobilePhone"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="email"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="address"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="expirationDate"/><saml:Attribute xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Name="digitalAddress"/></md:IDPSSODescriptor></md:EntityDescriptor>
\.


--
-- Name: admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.admin_id_seq', 2, true);


--
-- Name: assertion_consumer_service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.assertion_consumer_service_id_seq', 2, true);


--
-- Name: attribute_consuming_service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.attribute_consuming_service_id_seq', 2, true);


--
-- Name: certificate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.certificate_id_seq', 2, true);


--
-- Name: single_logout_service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.single_logout_service_id_seq', 2, true);


--
-- Name: spid_session_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.spid_session_id_seq', 1, true);


--
-- Name: spid_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.spid_user_id_seq', 1, true);


--
-- Name: tenant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tenant_id_seq', 2, true);


--
-- Name: test_idp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.test_idp_id_seq', 3, true);


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (id);


--
-- Name: assertion_consumer_service assertion_consumer_service_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.assertion_consumer_service
    ADD CONSTRAINT assertion_consumer_service_pkey PRIMARY KEY (id);


--
-- Name: attribute_consuming_service attribute_consuming_service_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.attribute_consuming_service
    ADD CONSTRAINT attribute_consuming_service_pkey PRIMARY KEY (id);


--
-- Name: certificate certificate_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.certificate
    ADD CONSTRAINT certificate_pkey PRIMARY KEY (id);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: oauth2_access_token oauth2_access_token_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth2_access_token
    ADD CONSTRAINT oauth2_access_token_pkey PRIMARY KEY (identifier);


--
-- Name: oauth2_authorization_code oauth2_authorization_code_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth2_authorization_code
    ADD CONSTRAINT oauth2_authorization_code_pkey PRIMARY KEY (identifier);


--
-- Name: oauth2_client oauth2_client_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth2_client
    ADD CONSTRAINT oauth2_client_pkey PRIMARY KEY (identifier);


--
-- Name: oauth2_refresh_token oauth2_refresh_token_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth2_refresh_token
    ADD CONSTRAINT oauth2_refresh_token_pkey PRIMARY KEY (identifier);


--
-- Name: single_logout_service single_logout_service_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.single_logout_service
    ADD CONSTRAINT single_logout_service_pkey PRIMARY KEY (id);


--
-- Name: spid_session spid_session_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.spid_session
    ADD CONSTRAINT spid_session_pkey PRIMARY KEY (id);


--
-- Name: spid_user spid_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.spid_user
    ADD CONSTRAINT spid_user_pkey PRIMARY KEY (id);


--
-- Name: tenant tenant_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tenant
    ADD CONSTRAINT tenant_pkey PRIMARY KEY (id);


--
-- Name: test_idp test_idp_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.test_idp
    ADD CONSTRAINT test_idp_pkey PRIMARY KEY (id);


--
-- Name: idx_1e1aa86e9033212a; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_1e1aa86e9033212a ON public.single_logout_service USING btree (tenant_id);


--
-- Name: idx_219cda4a9033212a; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_219cda4a9033212a ON public.certificate USING btree (tenant_id);


--
-- Name: idx_3b70d34a4c5ad50b; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_3b70d34a4c5ad50b ON public.spid_session USING btree (spid_user_id);


--
-- Name: idx_454d9673c7440455; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_454d9673c7440455 ON public.oauth2_access_token USING btree (client);


--
-- Name: idx_4dd90732b6a2dd68; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_4dd90732b6a2dd68 ON public.oauth2_refresh_token USING btree (access_token);


--
-- Name: idx_4e59c462678644c3; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_4e59c462678644c3 ON public.tenant USING btree (idp_test_id);


--
-- Name: idx_509fef5fc7440455; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_509fef5fc7440455 ON public.oauth2_authorization_code USING btree (client);


--
-- Name: idx_787ec669033212a; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_787ec669033212a ON public.attribute_consuming_service USING btree (tenant_id);


--
-- Name: idx_c6097ad9033212a; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_c6097ad9033212a ON public.assertion_consumer_service USING btree (tenant_id);


--
-- Name: uniq_18a86f1bf85e0677; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX uniq_18a86f1bf85e0677 ON public.spid_user USING btree (username);


--
-- Name: uniq_4e59c462570d17ca; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX uniq_4e59c462570d17ca ON public.tenant USING btree (open_login_uri);


--
-- Name: uniq_4e59c46281257d5d; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX uniq_4e59c46281257d5d ON public.tenant USING btree (entity_id);


--
-- Name: uniq_880e0d76f85e0677; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX uniq_880e0d76f85e0677 ON public.admin USING btree (username);


--
-- Name: single_logout_service fk_1e1aa86e9033212a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.single_logout_service
    ADD CONSTRAINT fk_1e1aa86e9033212a FOREIGN KEY (tenant_id) REFERENCES public.tenant(id);


--
-- Name: certificate fk_219cda4a9033212a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.certificate
    ADD CONSTRAINT fk_219cda4a9033212a FOREIGN KEY (tenant_id) REFERENCES public.tenant(id);


--
-- Name: spid_session fk_3b70d34a4c5ad50b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.spid_session
    ADD CONSTRAINT fk_3b70d34a4c5ad50b FOREIGN KEY (spid_user_id) REFERENCES public.spid_user(id);


--
-- Name: oauth2_access_token fk_454d9673c7440455; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth2_access_token
    ADD CONSTRAINT fk_454d9673c7440455 FOREIGN KEY (client) REFERENCES public.oauth2_client(identifier) ON DELETE CASCADE;


--
-- Name: oauth2_refresh_token fk_4dd90732b6a2dd68; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth2_refresh_token
    ADD CONSTRAINT fk_4dd90732b6a2dd68 FOREIGN KEY (access_token) REFERENCES public.oauth2_access_token(identifier) ON DELETE SET NULL;


--
-- Name: tenant fk_4e59c462678644c3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tenant
    ADD CONSTRAINT fk_4e59c462678644c3 FOREIGN KEY (idp_test_id) REFERENCES public.test_idp(id);


--
-- Name: oauth2_authorization_code fk_509fef5fc7440455; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth2_authorization_code
    ADD CONSTRAINT fk_509fef5fc7440455 FOREIGN KEY (client) REFERENCES public.oauth2_client(identifier) ON DELETE CASCADE;


--
-- Name: attribute_consuming_service fk_787ec669033212a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.attribute_consuming_service
    ADD CONSTRAINT fk_787ec669033212a FOREIGN KEY (tenant_id) REFERENCES public.tenant(id);


--
-- Name: assertion_consumer_service fk_c6097ad9033212a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.assertion_consumer_service
    ADD CONSTRAINT fk_c6097ad9033212a FOREIGN KEY (tenant_id) REFERENCES public.tenant(id);


--
-- PostgreSQL database dump complete
--

