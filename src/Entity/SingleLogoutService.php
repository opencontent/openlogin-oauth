<?php

namespace App\Entity;

use App\Repository\SingleLogoutServiceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SingleLogoutServiceRepository::class)
 */
class SingleLogoutService
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $index;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity=Tenant::class, inversedBy="singleLogoutServices", cascade={"persist", "remove"})
     */
    private $tenant;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRedirect;

    public function __toString(): string
    {
        return (string)$this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIndex(): ?int
    {
        return $this->index;
    }

    public function setIndex(?int $index): self
    {
        $this->index = $index;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getIsRedirect(): ?bool
    {
        return $this->isRedirect;
    }

    public function setIsRedirect(?bool $isRedirect): self
    {
        $this->isRedirect = $isRedirect;

        return $this;
    }
}
