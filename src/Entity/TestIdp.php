<?php

namespace App\Entity;

use App\Repository\TestIdpRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TestIdpRepository::class)
 */
class TestIdp
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $identifier;

    /**
     * @ORM\Column(type="text")
     */
    private $metadata;

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMetadata(): ?string
    {
        return $this->metadata;
    }

    public function setMetadata(string $metadata): self
    {
        $this->metadata = $metadata;

        return $this;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function isProxy(): bool
    {
        return strpos($this->getIdentifier(), 'proxy-') !== false;
    }

    public function setIdentifier(string $identifier): self
    {
        $identifier = strtolower(trim($identifier));
        $chars = array("ä", "ö", "ü", "ß");
        $replacements = array("ae", "oe", "ue", "ss");
        $identifier = str_replace($chars, $replacements, $identifier);
        $pattern = array("/([éèëê])/", "/([óòöô])/", "/([úùüû])/");
        $replacements = array("e", "o", "u");
        $identifier = preg_replace($pattern, $replacements, $identifier);
        $pattern = array(":", "!", "?", ".", "/", "'");
        $identifier = str_replace($pattern, "", $identifier);
        $pattern = array("/[^a-z0-9-]/", "/-+/");
        $identifier = preg_replace($pattern, "-", $identifier);
        $this->identifier = $identifier;

        return $this;
    }
}
