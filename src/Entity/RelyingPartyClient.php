<?php

namespace App\Entity;

use App\Cie\Core\Util;
use App\Repository\RelyingPartyClientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RelyingPartyClientRepository::class)
 */
class RelyingPartyClient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Tenant::class, inversedBy="relyingPartyClient", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Tenant $tenant;


    /**
     * @ORM\Column(type="json")
     */
    private array $requestedAcr = [];

    /**
     * @ORM\Column(type="json")
     */
    private array $spidUserAttributes = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $redirectUri;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $trustMark;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $trustMarkIss;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $trustMarkId;

    /**
     * @ORM\ManyToOne(targetEntity=FederationAuthority::class, inversedBy="relyingPartyClients")
     */
    private $federationAuthority;

    /**
     * @ORM\OneToMany(targetEntity=Certificate::class, mappedBy="relyingPartyClientFederation", cascade={"persist", "remove"})
     */
    private $federationCertificates;

    /**
     * @ORM\OneToMany(targetEntity=Certificate::class, mappedBy="relyingPartyClientCore", cascade={"persist", "remove"})
     */
    private $coreCertificates;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sub;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;


    public function __construct()
    {
        $this->federationCertificates = new ArrayCollection();
        $this->coreCertificates = new ArrayCollection();
    }

    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getRequestedAcr(): ?array
    {
        return $this->requestedAcr;
    }

    public function setRequestedAcr(array $requestedAcr): self
    {
        $this->requestedAcr = $requestedAcr;

        return $this;
    }

    public function getSpidUserAttributes(): ?array
    {
        return $this->spidUserAttributes;
    }

    public function setSpidUserAttributes(array $spidUserAttributes): self
    {
        $this->spidUserAttributes = $spidUserAttributes;

        return $this;
    }

    public function getRedirectUri(): ?string
    {
        return $this->redirectUri;
    }

    public function setRedirectUri(?string $redirectUri): self
    {
        $this->redirectUri = $redirectUri;

        return $this;
    }

    public function generateConfig(): array
    {

        //$certs = $this->getTenant()->getCertificates()->toArray();
        $federationCert = $this->federationCertificates->first();
        $coreCert = $this->coreCertificates->first();

        $fedPrivateCert = $federationCert->getKey();
        $fedPublicCert  = $federationCert->getCertificate();
        $corePrivateCert = $coreCert->getKey();
        $corePublicCert  = $coreCert->getCertificate();


        return  [
            'trust_mark' => $this->getTrustMark(),
            'trust_mark_iss' => $this->getTrustMarkIss(),
            'trust_mark_id' => $this->getTrustMarkId(),
            'cert_private' => Util::dumpFile($fedPrivateCert, md5($fedPrivateCert) . '.pem'), // '/var/www/html/data/oidc/cert/rp.pem'
            'cert_public' => Util::dumpFile($fedPublicCert, md5($fedPublicCert) . '.crt'), // '/var/www/html/data/oidc/cert/rp.crt'
            'cert_private_core' => Util::dumpFile($corePrivateCert, md5($corePrivateCert) . '.pem'), // '/var/www/html/data/oidc/cert/rp.pem'
            'cert_public_core' => Util::dumpFile($corePublicCert, md5($corePublicCert) . '.crt'), // '/var/www/html/data/oidc/cert/rp.crt'
            'client_id' => rtrim($this->getSub(), '/'),
            'base_url' => $this->getTenant()->getOpenLoginUri(),
            'client_name' => rtrim($this->getSub(), '/'), //$this->getTenant()->getOrganizationName(), // "Name of an example organization"
            'organization_name' => $this->getTenant()->getOrganizationName(),
            'authority_hint' => $this->getFederationAuthority() ? $this->getFederationAuthority()->getUri() : '',
            'contact' => $this->getEmail() ?? '',
            'is_pa' => $this->getTenant()->isIsPA() ?: true,
            'code' => $this->getTenant()->getIpaCode(),
            'code_type' => 'IPACode',
            'organization_identifier' => 'PA:IT-' . $this->getTenant()->getIpaCode(),
            'country_name' => 'IT',
            'locality_name' => $this->getTenant()->getLocality() ?: 'Rome',
            'email' => $this->getEmail() ?? '',
            'telephone' => $this->getTenant()->getPhone(),
            'website' => $this->getTenant()->getWebSiteUri(),
            'privacy' => $this->getTenant()->getPrivacyUri(),
            'logo' => $this->getTenant()->getLogoUri(),
            'requested_acr' => [2, 1],
            'spid_user_attributes' => ['given_name', 'family_name', 'email', 'https://attributes.eid.gov.it/fiscal_number'],
            'redirect_uri' => rtrim($this->getTenant()->getOpenLoginUri(), '/') . '/oidc/login-redirect',
            'response_handler' => 'SPID_CIE_OIDC_PHP\\Response\\ResponseHandlerPlain',
        ];
    }

    public function getTrustMark(): ?string
    {
        return $this->trustMark;
    }

    public function setTrustMark(?string $trustMark): self
    {
        $this->trustMark = $trustMark;

        return $this;
    }

    public function getTrustMarkIss(): ?string
    {
        return $this->trustMarkIss;
    }

    public function setTrustMarkIss(?string $trustMarkIss): self
    {
        $this->trustMarkIss = $trustMarkIss;

        return $this;
    }

    public function getTrustMarkId(): ?string
    {
        return $this->trustMarkId;
    }

    public function setTrustMarkId(?string $trustMarkId): self
    {
        $this->trustMarkId = $trustMarkId;

        return $this;
    }

    public function getFederationAuthority(): ?FederationAuthority
    {
        return $this->federationAuthority;
    }

    public function setFederationAuthority(?FederationAuthority $federationAuthority): self
    {
        $this->federationAuthority = $federationAuthority;

        return $this;
    }

    /**
     * @return Collection<int, Certificate>
     */
    public function getFederationCertificates(): Collection
    {
        return $this->federationCertificates;
    }

    public function addFederationCertificate(Certificate $federationCertificate): self
    {
        if (!$this->federationCertificates->contains($federationCertificate)) {
            $this->federationCertificates[] = $federationCertificate;
            $federationCertificate->setRelyingPartyClientFederation($this);
        }

        return $this;
    }

    public function removeFederationCertificate(Certificate $federationCertificate): self
    {
        if ($this->federationCertificates->removeElement($federationCertificate)) {
            // set the owning side to null (unless already changed)
            if ($federationCertificate->getRelyingPartyClientFederation() === $this) {
                $federationCertificate->setRelyingPartyClientFederation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Certificate>
     */
    public function getCoreCertificates(): Collection
    {
        return $this->coreCertificates;
    }

    public function addCoreCertificate(Certificate $coreCertificate): self
    {
        if (!$this->coreCertificates->contains($coreCertificate)) {
            $this->coreCertificates[] = $coreCertificate;
            $coreCertificate->setRelyingPartyClientCore($this);
        }

        return $this;
    }

    public function removeCoreCertificate(Certificate $coreCertificate): self
    {
        if ($this->coreCertificates->removeElement($coreCertificate)) {
            // set the owning side to null (unless already changed)
            if ($coreCertificate->getRelyingPartyClientCore() === $this) {
                $coreCertificate->setRelyingPartyClientCore(null);
            }
        }

        return $this;
    }

    public function getSub(): ?string
    {
        return $this->sub;
    }

    public function setSub(?string $sub): self
    {
        $this->sub = $sub;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
