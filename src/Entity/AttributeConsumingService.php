<?php

namespace App\Entity;

use App\Repository\AttributeConsumingServiceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AttributeConsumingServiceRepository::class)
 */
class AttributeConsumingService
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $displayName;

    /**
     * @ORM\Column(type="integer")
     */
    private $index;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $companyName;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $registeredOffice;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $fiscalNumber;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $ivaCode;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $idCard;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $spidCode;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $familyName;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $placeOfBirth;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $countyOfBirth;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $dateOfBirth;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $mobilePhone;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $expirationDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $digitalAddress;

    /**
     * @ORM\ManyToOne(targetEntity=Tenant::class, inversedBy="attributeConsumingServices", cascade={"persist", "remove"})
     */
    private $tenant;

    public function __toString(): string
    {
        return (string)$this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIndex(): ?int
    {
        return $this->index;
    }

    public function setIndex(int $index): self
    {
        $this->index = $index;

        return $this;
    }

    public function getGender(): ?bool
    {
        return $this->gender;
    }

    public function setGender(?bool $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getCompanyName(): ?bool
    {
        return $this->companyName;
    }

    public function setCompanyName(?bool $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getRegisteredOffice(): ?bool
    {
        return $this->registeredOffice;
    }

    public function setRegisteredOffice(?bool $registeredOffice): self
    {
        $this->registeredOffice = $registeredOffice;

        return $this;
    }

    public function getFiscalNumber(): ?bool
    {
        return $this->fiscalNumber;
    }

    public function setFiscalNumber(?bool $fiscalNumber): self
    {
        $this->fiscalNumber = $fiscalNumber;

        return $this;
    }

    public function getIvaCode(): ?bool
    {
        return $this->ivaCode;
    }

    public function setIvaCode(?bool $ivaCode): self
    {
        $this->ivaCode = $ivaCode;

        return $this;
    }

    public function getIdCard(): ?bool
    {
        return $this->idCard;
    }

    public function setIdCard(?bool $idCard): self
    {
        $this->idCard = $idCard;

        return $this;
    }

    public function getSpidCode(): ?bool
    {
        return $this->spidCode;
    }

    public function setSpidCode(?bool $spidCode): self
    {
        $this->spidCode = $spidCode;

        return $this;
    }

    public function getName(): ?bool
    {
        return $this->name;
    }

    public function setName(?bool $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFamilyName(): ?bool
    {
        return $this->familyName;
    }

    public function setFamilyName(?bool $familyName): self
    {
        $this->familyName = $familyName;

        return $this;
    }

    public function getPlaceOfBirth(): ?bool
    {
        return $this->placeOfBirth;
    }

    public function setPlaceOfBirth(?bool $placeOfBirth): self
    {
        $this->placeOfBirth = $placeOfBirth;

        return $this;
    }

    public function getCountyOfBirth(): ?bool
    {
        return $this->countyOfBirth;
    }

    public function setCountyOfBirth(?bool $countyOfBirth): self
    {
        $this->countyOfBirth = $countyOfBirth;

        return $this;
    }

    public function getDateOfBirth(): ?bool
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(?bool $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getMobilePhone(): ?bool
    {
        return $this->mobilePhone;
    }

    public function setMobilePhone(?bool $mobilePhone): self
    {
        $this->mobilePhone = $mobilePhone;

        return $this;
    }

    public function getEmail(): ?bool
    {
        return $this->email;
    }

    public function setEmail(?bool $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAddress(): ?bool
    {
        return $this->address;
    }

    public function setAddress(?bool $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getExpirationDate(): ?bool
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(?bool $expirationDate): self
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    public function getDigitalAddress(): ?bool
    {
        return $this->digitalAddress;
    }

    public function setDigitalAddress(?bool $digitalAddress): self
    {
        $this->digitalAddress = $digitalAddress;

        return $this;
    }

    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    public function setDisplayName(?string $displayName): self
    {
        $this->displayName = $displayName;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }
}
