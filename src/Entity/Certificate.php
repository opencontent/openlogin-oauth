<?php

namespace App\Entity;

use App\Repository\CertificateRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CertificateRepository::class)
 */
class Certificate
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $index;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $key;

    /**
     * @ORM\Column(type="text")
     */
    private $certificate;

    /**
     * @ORM\ManyToOne(targetEntity=Tenant::class, inversedBy="certificates", cascade={"persist", "remove"})
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity=RelyingPartyClient::class, inversedBy="federationCertificates")
     */
    private $relyingPartyClientFederation;

    /**
     * @ORM\ManyToOne(targetEntity=RelyingPartyClient::class, inversedBy="coreCertificates")
     */
    private $relyingPartyClientCore;


    public function __toString(): string
    {
        return (string)$this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIndex(): ?int
    {
        return $this->index;
    }

    public function setIndex(int $index): self
    {
        $this->index = $index;

        return $this;
    }

    public function getKey(): ?string
    {
        return $this->key;
    }

    public function setKey(?string $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function getCertificate(): ?string
    {
        return $this->certificate;
    }

    public function setCertificate(string $certificate): self
    {
        $this->certificate = $certificate;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getRelyingPartyClientFederation(): ?RelyingPartyClient
    {
        return $this->relyingPartyClientFederation;
    }

    public function setRelyingPartyClientFederation(?RelyingPartyClient $relyingPartyClientFederation): self
    {
        $this->relyingPartyClientFederation = $relyingPartyClientFederation;

        return $this;
    }

    public function getRelyingPartyClientCore(): ?RelyingPartyClient
    {
        return $this->relyingPartyClientCore;
    }

    public function setRelyingPartyClientCore(?RelyingPartyClient $relyingPartyClientCore): self
    {
        $this->relyingPartyClientCore = $relyingPartyClientCore;

        return $this;
    }
}
