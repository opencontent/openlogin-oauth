<?php

namespace App\Entity;

use App\Repository\SpidUserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=SpidUserRepository::class)
 */
class SpidUser implements UserInterface, JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\OneToMany(targetEntity=SpidSession::class, mappedBy="spidUser", cascade={"persist", "remove"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $sessions;

    public function __construct()
    {
        $this->sessions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * This method can be removed in Symfony 6.0 - is not needed for apps that do not check user passwords.
     *
     * @see UserInterface
     */
    public function getPassword(): ?string
    {
        return null;
    }

    /**
     * This method can be removed in Symfony 6.0 - is not needed for apps that do not check user passwords.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|SpidSession[]
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(SpidSession $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->setSpidUser($this);
        }

        return $this;
    }

    public function removeSession(SpidSession $session): self
    {
        if ($this->sessions->removeElement($session)) {
            // set the owning side to null (unless already changed)
            if ($session->getSpidUser() === $this) {
                $session->setSpidUser(null);
            }
        }

        return $this;
    }

    public function getSessionsCount() :int
    {
        return $this->sessions->count();
    }

    public function getSessionsAsString() :string
    {
        $data = [];
        foreach ($this->sessions as $item){
            $data[] = (string)$item;
        }

        return implode(', ', $data);
    }

    public function jsonSerialize(): array
    {
        $data = [];
        $session = $this->getSessions()->first();
        if ($session instanceof SpidSession) {
            $attributes = $session->getAttributes();
            $attributes['session'] = $session->getSessionId();
            $attributes['spid-level'] = $session->getLevel();
            $attributes['idp-entity-id'] = $session->getIdp();
            $attributes['provider'] = 'spid';

            $data = array_change_key_case($attributes, CASE_LOWER);
            if (!isset($data['email'])){
                $data['email'] = '-';
            }
        }

        return $data;
    }

    public function getUserIdentifier(): string
    {
        return $this->getUsername();
    }
}
