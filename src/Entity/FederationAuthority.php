<?php

namespace App\Entity;

use App\Repository\FederationAuthorityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FederationAuthorityRepository::class)
 */
class FederationAuthority
{
  /**
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $name;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $uri;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $openIdProvider;

  /**
   * @ORM\OneToMany(targetEntity=RelyingPartyClient::class, mappedBy="federationAuthority")
   */
  private $relyingPartyClients;

  public function __construct()
  {
    $this->relyingPartyClients = new ArrayCollection();
  }

  public function __toString(): string
  {
    return $this->name;
  }

  public function getId(): ?int
  {
    return $this->id;
  }

  public function getName(): ?string
  {
    return $this->name;
  }

  public function setName(string $name): self
  {
    $this->name = $name;

    return $this;
  }

  public function getUri(): ?string
  {
    return $this->uri;
  }

  public function setUri(string $uri): self
  {
    $this->uri = $uri;

    return $this;
  }

  public function getOpenIdProvider(): ?string
  {
    return $this->openIdProvider;
  }

  public function setOpenIdProvider(?string $openIdProvider): self
  {
    $this->openIdProvider = $openIdProvider;

    return $this;
  }

  /**
   * @return Collection<int, RelyingPartyClient>
   */
  public function getRelyingPartyClients(): Collection
  {
    return $this->relyingPartyClients;
  }

  public function addRelyingPartyClient(RelyingPartyClient $relyingPartyClient): self
  {
    if (!$this->relyingPartyClients->contains($relyingPartyClient)) {
      $this->relyingPartyClients[] = $relyingPartyClient;
      $relyingPartyClient->setFederationAuthority($this);
    }

    return $this;
  }

  public function removeRelyingPartyClient(RelyingPartyClient $relyingPartyClient): self
  {
    if ($this->relyingPartyClients->removeElement($relyingPartyClient)) {
      // set the owning side to null (unless already changed)
      if ($relyingPartyClient->getFederationAuthority() === $this) {
        $relyingPartyClient->setFederationAuthority(null);
      }
    }

    return $this;
  }
}
