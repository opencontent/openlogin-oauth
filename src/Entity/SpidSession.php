<?php

namespace App\Entity;

use App\Repository\SpidSessionRepository;
use DateTime;
use DateTimeInterface;
use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SpidSessionRepository::class)
 */
class SpidSession
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sessionId;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $idp;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=SpidUser::class, inversedBy="sessions", cascade={"persist", "remove"})
     */
    private $spidUser;

    /**
     * @ORM\Column(type="json")
     */
    private $attributes = [];

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    public function __toString()
    {
        return $this->sessionId
            . ' (' . $this->createdAt->setTimezone(new DateTimeZone('Europe/Rome'))->format('c') . ')';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    public function setSessionId(string $sessionId): self
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getIdp(): ?string
    {
        return $this->idp;
    }

    public function setIdp(string $idp): self
    {
        $this->idp = $idp;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getSpidUser(): ?SpidUser
    {
        return $this->spidUser;
    }

    public function setSpidUser(?SpidUser $spidUser): self
    {
        $this->spidUser = $spidUser;

        return $this;
    }

    public function getAttributes(): ?array
    {
        return $this->attributes;
    }

    public function getAttributesAsString(): string
    {
        $attributes = (array)$this->attributes;
        $data = array_map(function ($value, $key) {
            return $key . ': ' . $value;
        }, array_values($attributes), array_keys($attributes));
        return implode(', ', $data);
    }

    public function setAttributes(array $attributes): self
    {
        $this->attributes = $attributes;

        return $this;
    }

    public function getSpidUsername(): ?string
    {
        return $this->getSpidUser()->getUsername();
    }
}
