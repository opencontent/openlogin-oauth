<?php

namespace App\Entity;

use App\Repository\TenantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TenantRepository::class)
 */
class Tenant
{
  /**
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255, unique=true)
   */
  private $openLoginUri;

  /**
   * @ORM\Column(type="string", length=255, unique=true)
   */
  private $entityId;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $displayName;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $organizationName;

  /**
   * @ORM\Column(type="string")
   */
  private $ipaCode;

  /**
   * @ORM\Column(type="string", nullable=true)
   */
  private $fiscalCode;

  /**
   * @ORM\Column(type="string")
   */
  private $email;

  /**
   * @ORM\Column(type="string")
   */
  private $phone;

  /**
   * @ORM\OneToMany(targetEntity=Certificate::class, mappedBy="tenant", cascade={"persist", "remove"})
   */
  private $certificates;

  /**
   * one of: "exact", "minimum", "better" or "maximum"
   * @ORM\Column(type="string", length=50, nullable=true)
   */
  private $comparison;

  /**
   * @ORM\OneToMany(targetEntity=AssertionConsumerService::class, mappedBy="tenant", cascade={"persist", "remove"})
   */
  private $assertionConsumerServices;

  /**
   * @ORM\OneToMany(targetEntity=AttributeConsumingService::class, mappedBy="tenant", cascade={"persist", "remove"})
   */
  private $attributeConsumingServices;

  /**
   * @ORM\OneToMany(targetEntity=SingleLogoutService::class, mappedBy="tenant", cascade={"persist", "remove"})
   */
  private $singleLogoutServices;

  /**
   * @ORM\Column(type="integer", nullable=true)
   */
  private $spidLevel;

  /**
   * @ORM\Column(type="integer", nullable=true)
   */
  private $loginIndex;

  /**
   * @ORM\Column(type="text", nullable=true)
   */
  private $customMetadata;

  /**
   * @ORM\ManyToOne(targetEntity=TestIdp::class)
   */
  private $idpTest;

  /**
   * @ORM\OneToOne(targetEntity=RelyingPartyClient::class, mappedBy="tenant", cascade={"persist", "remove"})
   */
  private $relyingPartyClient;

  /**
   * @ORM\Column(type="boolean", options={"default" : 1})
   */
  private bool $isPA = true;

  /**
   * @ORM\Column(type="string", length=255, options={"default" : "IT"})
   */
  private string $country = 'IT';

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private ?string $locality;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private ?string $webSiteUri;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private ?string $privacyUri;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private ?string $logoUri;

  public function __construct()
  {
    $this->certificates = new ArrayCollection();
    $this->assertionConsumerServices = new ArrayCollection();
    $this->attributeConsumingServices = new ArrayCollection();
    $this->singleLogoutServices = new ArrayCollection();
  }

  public function __toString(): string
  {
    return $this->displayName;
  }


  public function getMetadataHtmlLink(): string
  {
    $openLoginUri = rtrim($this->openLoginUri, '/').'/';
    $html = "<a href=\"{$openLoginUri}metadata\"'>$this->entityId</a>";
    foreach ($this->getAssertionConsumerServices() as $assertionConsumerService) {
      if ($this->getLoginIndex() === $assertionConsumerService->getIndex()) {
        $html .= '<br><small style="font-size: 0.7em">ACS: '.$assertionConsumerService->getUrl().'</small>';
      }
    }
    foreach ($this->getSingleLogoutServices() as $singleLogoutService) {
      if ($this->getLoginIndex() === $singleLogoutService->getIndex()) {
        $html .= '<br><small style="font-size: 0.7em">SLO: '.$singleLogoutService->getUrl().'</small>';
      }
    }

    return $html;
  }

  public function getHasCustomMetadataAsHtml(): string
  {
    return $this->customMetadata !== null ? '<i class="fa fa-check"></i>' : '';
  }

  public function getHasTestIdpAsHtml(): string
  {
    $name = '';
    if ($this->getIdpTest()) {
      $name = (string)$this->getIdpTest();
      if ($this->isProxy()) {
        $name .= ' <i class="fa fa-arrow-alt-circle-right"></i>';
      }
    }

    return $name;
  }


  public function getId(): ?int
  {
    return $this->id;
  }

  /**
   * @return mixed
   */
  public function getOpenLoginUri(): ?string
  {
    return $this->openLoginUri;
  }

  /**
   * @param mixed $openLoginUri
   * @return Tenant
   */
  public function setOpenLoginUri($openLoginUri): self
  {
    $this->openLoginUri = rtrim($openLoginUri, '/').'/';

    return $this;
  }

  /**
   * @return mixed
   */
  public function getEntityId(): ?string
  {
    return $this->entityId;
  }

  /**
   * @param mixed $entityId
   * @return Tenant
   */
  public function setEntityId($entityId): self
  {
    $this->entityId = $entityId;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getDisplayName(): ?string
  {
    return $this->displayName;
  }

  /**
   * @param mixed $displayName
   * @return Tenant
   */
  public function setDisplayName($displayName): self
  {
    $this->displayName = $displayName;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getOrganizationName(): ?string
  {
    return $this->organizationName;
  }

  /**
   * @param mixed $organizationName
   * @return Tenant
   */
  public function setOrganizationName($organizationName): self
  {
    $this->organizationName = $organizationName;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getIpaCode(): ?string
  {
    return $this->ipaCode;
  }

  /**
   * @param mixed $ipaCode
   * @return Tenant
   */
  public function setIpaCode($ipaCode): self
  {
    $this->ipaCode = $ipaCode;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getFiscalCode(): ?string
  {
    return $this->fiscalCode;
  }

  /**
   * @param mixed $fiscalCode
   * @return Tenant
   */
  public function setFiscalCode($fiscalCode): self
  {
    $this->fiscalCode = $fiscalCode;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getEmail(): ?string
  {
    return $this->email;
  }

  /**
   * @param mixed $email
   * @return Tenant
   */
  public function setEmail($email): self
  {
    $this->email = $email;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getPhone(): ?string
  {
    return $this->phone;
  }

  /**
   * @param mixed $phone
   * @return Tenant
   */
  public function setPhone($phone): self
  {
    $this->phone = $phone;

    return $this;
  }

  /**
   * @return Collection|Certificate[]
   */
  public function getCertificates(): Collection
  {
    return $this->certificates;
  }

  public function addCertificate(Certificate $certificate): self
  {
    if (!$this->certificates->contains($certificate)) {
      $this->certificates[] = $certificate;
      $certificate->setTenant($this);
    }

    return $this;
  }

  public function removeCertificate(Certificate $certificate): self
  {
    if ($this->certificates->removeElement($certificate)) {
      // set the owning side to null (unless already changed)
      if ($certificate->getTenant() === $this) {
        $certificate->setTenant(null);
      }
    }

    return $this;
  }

  public function getComparison(): ?string
  {
    return $this->comparison;
  }

  public function setComparison(?string $comparison): self
  {
    $this->comparison = $comparison;

    return $this;
  }

  /**
   * @return Collection|AssertionConsumerService[]
   */
  public function getAssertionConsumerServices(): Collection
  {
    return $this->assertionConsumerServices;
  }

  public function addAssertionConsumerService(AssertionConsumerService $assertionConsumerService): self
  {
    if (!$this->assertionConsumerServices->contains($assertionConsumerService)) {
      $this->assertionConsumerServices[] = $assertionConsumerService;
      $assertionConsumerService->setTenant($this);
    }

    return $this;
  }

  public function removeAssertionConsumerService(AssertionConsumerService $assertionConsumerService): self
  {
    if ($this->assertionConsumerServices->removeElement($assertionConsumerService)) {
      // set the owning side to null (unless already changed)
      if ($assertionConsumerService->getTenant() === $this) {
        $assertionConsumerService->setTenant(null);
      }
    }

    return $this;
  }

  /**
   * @return Collection|AttributeConsumingService[]
   */
  public function getAttributeConsumingServices(): Collection
  {
    return $this->attributeConsumingServices;
  }

  public function addAttributeConsumingService(AttributeConsumingService $attributeConsumingService): self
  {
    if (!$this->attributeConsumingServices->contains($attributeConsumingService)) {
      $this->attributeConsumingServices[] = $attributeConsumingService;
      $attributeConsumingService->setTenant($this);
    }

    return $this;
  }

  public function removeAttributeConsumingService(AttributeConsumingService $attributeConsumingService): self
  {
    if ($this->attributeConsumingServices->removeElement($attributeConsumingService)) {
      // set the owning side to null (unless already changed)
      if ($attributeConsumingService->getTenant() === $this) {
        $attributeConsumingService->setTenant(null);
      }
    }

    return $this;
  }

  /**
   * @return Collection|SingleLogoutService[]
   */
  public function getSingleLogoutServices(): Collection
  {
    return $this->singleLogoutServices;
  }

  public function addSingleLogoutService(SingleLogoutService $singleLogoutService): self
  {
    if (!$this->singleLogoutServices->contains($singleLogoutService)) {
      $this->singleLogoutServices[] = $singleLogoutService;
      $singleLogoutService->setTenant($this);
    }

    return $this;
  }

  public function removeSingleLogoutService(SingleLogoutService $singleLogoutService): self
  {
    if ($this->singleLogoutServices->removeElement($singleLogoutService)) {
      // set the owning side to null (unless already changed)
      if ($singleLogoutService->getTenant() === $this) {
        $singleLogoutService->setTenant(null);
      }
    }

    return $this;
  }

  public function getSpidLevel(): ?int
  {
    return $this->spidLevel;
  }

  public function setSpidLevel(?int $spidLevel): self
  {
    $this->spidLevel = $spidLevel;

    return $this;
  }

  public function getLoginIndex(): ?int
  {
    return $this->loginIndex;
  }

  public function setLoginIndex(?int $loginIndex): self
  {
    $this->loginIndex = $loginIndex;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getCustomMetadata(): ?string
  {
    return $this->customMetadata;
  }

  /**
   * @param mixed $customMetadata
   * @return Tenant
   */
  public function setCustomMetadata($customMetadata): self
  {
    $this->customMetadata = $customMetadata;

    return $this;
  }

  public function hasCustomMetadata(): bool
  {
    return $this->customMetadata !== null;
  }

  public function getIdpTest(): ?TestIdp
  {
    return $this->idpTest;
  }

  public function setIdpTest(?TestIdp $idpTest): self
  {
    $this->idpTest = $idpTest;

    return $this;
  }

  public function isProxy(): bool
  {
    return $this->getIdpTest() && $this->getIdpTest()->isProxy();
  }

  public function getProxyIdentifier(): ?string
  {
    return $this->isProxy() ? $this->getIdpTest()->getIdentifier() : null;
  }

  public function getIsMerge(): string
  {
    return $this->getCertificates()->count() > 1 ? '<i class="fa fa-check"></i>' : '';
  }


  public function isMerge(): bool
  {
    return $this->getCertificates()->count() > 1;
  }

  public function getRelyingPartyClient(): ?RelyingPartyClient
  {
    return $this->relyingPartyClient;
  }

  public function setRelyingPartyClient(RelyingPartyClient $relyingPartyClient): self
  {
    // set the owning side of the relation if necessary
    if ($relyingPartyClient->getTenant() !== $this) {
      $relyingPartyClient->setTenant($this);
    }

    $this->relyingPartyClient = $relyingPartyClient;

    return $this;
  }

  public function isIsPA(): ?bool
  {
    return $this->isPA;
  }

  public function setIsPA(bool $isPA): self
  {
    $this->isPA = $isPA;

    return $this;
  }

  public function getCountry(): ?string
  {
    return $this->country;
  }

  public function setCountry(string $country): self
  {
    $this->country = $country;

    return $this;
  }

  public function getLocality(): ?string
  {
    return $this->locality;
  }

  public function setLocality(?string $locality): self
  {
    $this->locality = $locality;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getWebSiteUri(): ?string
  {
    return $this->webSiteUri;
  }

  /**
   * @param mixed $webSiteUri
   */
  public function setWebSiteUri(?string $webSiteUri): self
  {
    $this->webSiteUri = $webSiteUri;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getPrivacyUri(): ?string
  {
    return $this->privacyUri;
  }

  /**
   * @param mixed $privacyUri
   */
  public function setPrivacyUri(?string $privacyUri): self
  {
    $this->privacyUri = $privacyUri;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getLogoUri(): ?string
  {
    return $this->logoUri;
  }

  /**
   * @param mixed $logoUri
   */
  public function setLogoUri(?string $logoUri): self
  {
    $this->logoUri = $logoUri;
    return $this;
  }


}
