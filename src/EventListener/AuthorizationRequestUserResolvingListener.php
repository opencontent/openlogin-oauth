<?php

namespace App\EventListener;

use League\Bundle\OAuth2ServerBundle\Event\AuthorizationRequestResolveEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Twig\Environment;

final class AuthorizationRequestUserResolvingListener
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function onAuthorizationRequest(AuthorizationRequestResolveEvent $event): void
    {
        if ($event->getUser() instanceof UserInterface){
            $event->resolveAuthorization(true);
        }
    }
}
