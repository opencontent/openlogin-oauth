<?php

namespace App\Security;

use App\Repository\SpidUserRepository;
use App\Spid\SpidClientFactory;
use League\Bundle\OAuth2ServerBundle\Manager\ClientFilter;
use League\Bundle\OAuth2ServerBundle\Manager\ClientManagerInterface;
use League\Bundle\OAuth2ServerBundle\Model\Client;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class OidcCieAuthenticator extends AbstractLoginFormAuthenticator
{
    use TargetPathTrait;

    public const CIE_ATTRIBUTES_MAP = [
      'https://attributes.eid.gov.it/fiscal_number' => 'fiscalNumber',
      'given_name' => 'name',
      'family_name' => 'familyName',
    ];

    public const LOGIN_ROUTE = 'spid_login';

    private UrlGeneratorInterface $urlGenerator;

    private SpidUserProvider $spidUserProvider;

    private SessionInterface $session;

    private SpidUserRepository $spidUserRepository;

    private SpidClientFactory $spidClientFactory;

    private ClientManagerInterface $clientManager;
    private LoggerInterface $logger;

    public function __construct(
        UrlGeneratorInterface $urlGenerator,
        SpidUserProvider $spidUserProvider,
        RequestStack $requestStack,
        SpidUserRepository $spidUserRepository,
        SpidClientFactory $spidClientFactory,
        ClientManagerInterface $clientManager,
        LoggerInterface $logger
    ) {
        $this->urlGenerator = $urlGenerator;
        $this->spidUserProvider = $spidUserProvider;
        $this->session = $requestStack->getSession();
        $this->spidUserRepository = $spidUserRepository;
        $this->spidClientFactory = $spidClientFactory;
        $this->clientManager = $clientManager;
        $this->logger = $logger;
    }

    public function supports(Request $request): bool
    {
        if (!$this->session->isStarted()) {
            $this->session->start();
        }
        $auth = $this->session->get('auth');
        if (
            empty($auth)
            ||  empty($auth['userinfo'])
            //&& $auth['redirect_uri'] !== null
            || empty($auth['state'])
        ) {
            return false;
        }

      return true;
    }

    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }

    public function authenticate(Request $request): PassportInterface
    {

        $auth = $this->session->get('auth');
        $this->logger->info('cie_authenticator_auth', [
            'session_auth' => $auth,
        ]);
        $userInfo = (array)$auth['userinfo'];
        $this->logger->info('cie_authenticator_user_info', [
            'user_info' => $userInfo,
        ]);

        $userIdentifier = $userInfo['https://attributes.eid.gov.it/fiscal_number'];

        $attributes = $this->normalizeAttributes($userInfo);

        $this->spidUserRepository->storeSession(
            $userIdentifier,
            $userInfo['sub'],
            2, // Da verificare cosa passare
            $auth['ta_id'] . ' --- ' . $auth['op_id'],
            $attributes
        );

        return new SelfValidatingPassport(
            new UserBadge($userIdentifier, function ($userIdentifier) {
                return $this->spidUserProvider->loadUserByUsername($userIdentifier);
            })
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        $message = strtr($exception->getMessageKey(), $exception->getMessageData());

        return new Response($message, 403);
    }

    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        if ($request->attributes->get('_route') === 'spid_logout' && $request->query->has('redirect')) {
            /** @var Client[] $test */
            $clients = $this->clientManager->list(ClientFilter::create());
            foreach ($clients as $client) {
                foreach ($client->getRedirectUris() as $redirectUri) {
                    $uri = parse_url((string)$redirectUri, PHP_URL_HOST);
                    $requestUri = parse_url((string)$request->query->get('redirect'), PHP_URL_HOST);
                    if ($uri === $requestUri) {
                        return new RedirectResponse($request->query->get('redirect'));
                    }
                }
            }
        }

        return parent::start($request, $authException); // TODO: Change the autogenerated stub
    }


    private function normalizeAttributes(array $attributes): array
    {
      $result = [];
      foreach ($attributes as $key => $value) {
        $result[$key] = str_replace(["\r\n", "\r", "\n"], ' ', $value);
      }

      foreach (self::CIE_ATTRIBUTES_MAP as $k => $v) {
        if (!empty($result[$k])) {
          $result[$v] = $result[$k];
        }
      }

      return $result;
    }
}