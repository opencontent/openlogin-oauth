<?php

namespace App\Security;

use App\Entity\SpidUser;
use App\Repository\SpidUserRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class SpidUserProvider implements UserProviderInterface
{

    private $spidUserRepository;

    public function __construct(SpidUserRepository $spidUserRepository)
    {
        $this->spidUserRepository = $spidUserRepository;
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        if (!$user instanceof SpidUser) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function loadUserByUsername(string $username): UserInterface
    {
        return $this->spidUserRepository->findOneBy(['username' => $username]);
    }

    public function supportsClass(string $class): bool
    {
        return $class == SpidUser::class;
    }

}