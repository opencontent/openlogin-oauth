<?php

/**
 * spid-cie-oidc-php
 * https://github.com/italia/spid-cie-oidc-php
 *
 * 2022 Michele D'Amico (damikael)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author     Michele D'Amico <michele.damico@linfaservice.it>
 * @license    http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 */

namespace App\Cie\Federation;

use App\Cie\Core\Util;
use App\Cie\Core\JWT;
use Exception;

/**
 *  Handle EntityStatement
 *
 *  [OpenID Connect Federation Entity Statement](https://openid.net/specs/openid-connect-federation-1_0.html#rfc.section.3.1)
 *
 */
class EntityStatement
{
    /**
     *  creates a new EntityStatement instance
     *
     * @param string $token entity statement JWS token
     * @param string $iss issuer
     * @return void
     * @throws Exception
     */
    public function __construct($token = null, $iss = null)
    {
        if ($token != null) {
            $this->token = $token;
            $this->iss = $iss;
            $this->payload = JWT::getJWSPayload($this->token);
            $this->validate($token);
        }
    }

    /**
     *  creates the JWT to be returned from .well-known/openid-federation endpoint
     *
     * @param array $config base configuration
     * @param bool $json
     * @return mixed
     * @throws \Exception
     * @codeCoverageIgnore
     */
    public static function makeFromConfig(array $config, $json = false)
    {
        $crt = $config['cert_public'];
        $coreCrt = $config['cert_public_core'];

        $crt_jwk_fed_sig = JWT::getCertificateJWK($crt);
        $crt_jwk_core_sig = JWT::getCertificateJWK($coreCrt);
        $crt_jwk_core_enc = JWT::getCertificateJWK($coreCrt, 'enc');

        /*
         "federation_entity": {
            "organization_name": "Comune di Lunano",
            "homepage_uri": "https://lunano.mycity.it",
            "policy_uri": "https://lunano.mycity.it/privacy",
            "logo_uri": "https://mycity.s3.sbg.io.cloud.ovh.net/3379820/Lunano-Stemma-removebg-preview.png",
            },
        */
        $payload = array(
            "sub" => $config['client_id'],
            "metadata" => array(
                "federation_entity" => [
                    "federation_resolve_endpoint" => rtrim($config['base_url'], '/') . '/oidc/resolve',
                    "organization_name" => $config["organization_name"], // Deve coincidere esattamente con il nome dell'ente registrato sul sito di federazione (https://federazione.servizicie.interno.gov.it/)
                    "homepage_uri" => $config["website"], //"https://www.comune.verbania.it/", // Fixme: inserire in database
                    "policy_uri"=> $config["privacy"], //"https://www.comune.verbania.it/Privacy", // Fixme: inserire in database
                    "logo_uri" => $config["logo"], //"https://www.comune.verbania.it/var/opencityverbania/storage/images/1298-13-ita-IT/OpenCity_header_logo.png",  // Fixme: inserire in database
                    "contacts"=> [
                      $config['contact'] //"istituzionale.verbania@legalmail.it"  // Fixme: inserire in database
                    ]
                ],
                "openid_relying_party" => array(
                    "application_type" => "web",
                    "client_registration_types" => array( "automatic" ),
                    "client_name" => $config['client_name'], // "Name of an example organization"
                    "organization_name" => $config['organization_name'],
                    "contacts" => array( $config['contact'] ),
                    "grant_types" => array( "refresh_token", "authorization_code" ),
                    "jwks" => array(
                        "keys" => array( $crt_jwk_core_sig, $crt_jwk_core_enc )
                    ),
                    "redirect_uris" => array( rtrim($config['base_url'], '/') . '/oidc/rp/redirect' ),
                    "response_types" => array( "code" ),
                    "client_id" => $config['client_id'],
                    "id_token_signed_response_alg" => "RS256",
                    "userinfo_signed_response_alg" => "RS256",
                    "userinfo_encrypted_response_alg" => "RSA-OAEP",
                    "userinfo_encrypted_response_enc" => "A128CBC-HS256",
                    "token_endpoint_auth_method" => "private_key_jwt",
                    //"subject_type" => "pairwise",
                    //"jwks_uri" => rtrim($config['client_id'], '/') . '/oidc/rp/jwks.json',
                    //"signed_jwks_uri" => rtrim($config['client_id'], '/') . '/oidc/rp/jwks.jose',
                ),
            ),
            "iss" => $config['client_id'],
            "iat" => strtotime("now"),
            "exp" => strtotime("+30 minutes"),
            "jwks" => array(
                "keys" => array( $crt_jwk_fed_sig )
            ),
            "authority_hints" => array(
                $config['authority_hint']
            ),
            "trust_marks" => [
                [
                    "trust_mark" => $config['trust_mark'],
                    "iss" => $config['trust_mark_iss'],
                    "id" => $config['trust_mark_id']
                ]
            ],
            /*"trust_marks" => array(
                array(
                    "trust_mark" => "eyJraWQiOiJkZWZhdWx0UlNBU2lnbiIsInR5cCI6InRydXN0LW1hcmsrand0IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJodHRwczovL2xvZ2luLXFhLmJvYXQub3BlbmNvbnRlbnQuaW8iLCJpc3MiOiJodHRwczovL3ByZXByb2Qub2lkYy5yZWdpc3RyeS5zZXJ2aXppY2llLmludGVybm8uZ292Lml0Iiwib3JnYW5pemF0aW9uX3R5cGUiOiJwdWJsaWMiLCJpZCI6Imh0dHBzOi8vcHJlcHJvZC5vaWRjLnJlZ2lzdHJ5LnNlcnZpemljaWUuaW50ZXJuby5nb3YuaXQvb3BlbmlkX3JlbHlpbmdfcGFydHkvcHVibGljIiwiZXhwIjoxNzQ1NjAxNjYwLCJpYXQiOjE3MTQwNjU2NjB9.ZowXUSMuLCIzV4x2x_tVJMSukmx7ppQ5VBjSpgCa5QiqCXpQ3gKNTykU03SNFFFsBw5wrMZ5lw_4OefvX3O9ZR5hOPzm8Qz_7zHp78092tAfqolUrHwlEMsicLzvFSCVKTB_fTesvIEp7NQ1gOJiMLy8oGnCQSFS0D1mZ0zctmb1Qp1hg21v7ufUmi5ixgpljyaadhWpI56leiz3rM5_OOXdjS3WW7XeX-cxPVLPEZtd4tWFRm78L5y4jySqKDIXhRnimvVfKFYi-PB9C5GrygvjDewMMiGcqtzqbpzDq7QC5d4p1LGJlCW_1YUZsiuOpsk53jpudVp0ErzdJG-e6A",
                    "iss" => "https://preprod.oidc.registry.servizicie.interno.gov.it",
                    "id" => "https://preprod.oidc.registry.servizicie.interno.gov.it/openid_provider/public"
                )
            ),*/

        );

        $header = array(
            "typ" => "entity-statement+jwt",
            "alg" => "RS256",
            "kid" => $crt_jwk_fed_sig['kid']
        );

        $key = $config['cert_private'];
        $key_jwk = JWT::getKeyJWK($key);
        $jws = JWT::makeJWS($header, $payload, $key_jwk);

        return $json ? json_encode($payload) : $jws;
    }

    /**
     *  initialize the entity statement payload from object
     *
     * @param object $object the entity statement object
     * @throws Exception
     * @return EntityStatement
     */
    public function initFromObject(object $object)
    {
        // copy by value, not assign by ref
        $this->payload = json_decode(json_encode($object));
        return $this;
    }

    /**
     *  return entity statement payload
     *
     * @throws Exception
     * @return mixed the entity statement payload
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     *  validate token
     *
     * @param string $token entity statement JWS token
     * @throws Exception
     * @return mixed
     * @codeCoverageIgnore
     */
    public function validate()
    {
        $jwks = $this->payload->jwks;

        // verify signature
        /*
        if (!JWT::isSignatureVerified($this->token, $jwks)) {
            throw new \Exception("signature verification failed");
        }
        */

        // verify if token is not expired and valid
        if (!JWT::isValid($this->token)) {
            throw new \Exception("entity statement not valid");
        }

        // if issuer is correct
        /*
        if ($this->payload->iss != $this->iss) {
            throw new \Exception("issuer not valid");
        }
        */
    }

    /**
     *  apply policy from federation entity statement
     *
     * @param EntityStatement $federation_entity_statement the federation entity statement containing policy
     * @throws Exception
     * @return mixed
     * @codeCoverageIgnore
     */
    public function applyPolicy(EntityStatement $federation_entity_statement)
    {
        $payload = $federation_entity_statement->getPayload();
        $policy = $payload->metadata_policy;

        foreach ($policy as $entity_type => $entity_policy) {
            if ($this->payload->metadata->$entity_type != null) {
                foreach ($entity_policy as $policy_claim => $policy_rule) {
                    if ($this->payload->metadata->$entity_type->$policy_claim != null) {
                        foreach ($policy_rule as $policy_modifier => $policy_value) {
                            switch ($policy_modifier) {
                                case 'value':
                                    $this->applyPolicyModifierValue($entity_type, $policy_claim, $policy_value);
                                    break;
                                case 'add':
                                    $this->applyPolicyModifierAdd($entity_type, $policy_claim, $policy_value);
                                    break;
                                case 'default':
                                    $this->applyPolicyModifierDefault($entity_type, $policy_claim, $policy_value);
                                    break;
                                case 'one_of':
                                    $this->applyPolicyModifierOneOf($entity_type, $policy_claim, $policy_value);
                                    break;
                                case 'subset_of':
                                    $this->applyPolicyModifierSubsetOf($entity_type, $policy_claim, $policy_value);
                                    break;
                                case 'superset_of':
                                    $this->applyPolicyModifierSupersetOf($entity_type, $policy_claim, $policy_value);
                                    break;
                                case 'essential':
                                    $this->applyPolicyModifierEssential($entity_type, $policy_claim, $policy_value);
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }


    private function applyPolicyModifierValue($entity_type, $claim, $policy)
    {
        $this->payload->metadata->$entity_type->$claim = $policy;
    }

    private function applyPolicyModifierAdd($entity_type, $claim, $policy)
    {
        $claim_val = $this->payload->metadata->$entity_type->$claim;
        if (!is_array($policy)) {
            $policy = [$policy];
        }
        foreach ($policy as $p) {
            if (is_array($claim_val) && !in_array($p, $claim_val)) {
                $this->payload->metadata->$entity_type->$claim[] = $p;
            } else {
                $this->payload->metadata->$entity_type->$claim = $p;
            }
        }
    }

    private function applyPolicyModifierDefault($entity_type, $claim, $policy)
    {
        $claim_val = $this->payload->metadata->$entity_type->$claim;
        if ($claim_val == null || $claim_val == '' || $claim_val == array()) {
            $this->payload->metadata->$entity_type->$claim = $policy;
        }
    }

    private function applyPolicyModifierOneOf($entity_type, $claim, $policy)
    {
        $claim_val = $this->payload->metadata->$entity_type->$claim;
        if ($claim_val != null && !in_array($claim_val, $policy)) {
            throw new \Exception("Failed trust policy (" . $claim . " must be one of " . json_encode($policy) . ")");
        }
    }

    private function applyPolicyModifierSubsetOf($entity_type, $claim, $policy)
    {
        $claim_val = $this->payload->metadata->$entity_type->$claim;
        if (!is_array($claim_val) || !(array_intersect($claim_val, $policy) === $claim_val)) {
            throw new \Exception("Failed trust policy (" . $claim . " must be subset of " . json_encode($policy) . ")");
        }
    }

    private function applyPolicyModifierSupersetOf($entity_type, $claim, $policy)
    {
        $claim_val = $this->payload->metadata->$entity_type->$claim;
        if (!is_array($claim_val) || !(array_intersect($policy, $claim_val) === $policy)) {
            throw new \Exception("Failed trust policy (" . $claim . " must be superset of " . json_encode($policy) . ")");
        }
    }

    private function applyPolicyModifierEssential($entity_type, $claim, $policy)
    {
        $claim_val = $this->payload->metadata->$entity_type->$claim;
        if ($policy == true && ($claim_val == null || $claim_val == '')) {
            throw new \Exception("Failed trust policy (" . $claim . " must have a value)");
        }
    }
}
