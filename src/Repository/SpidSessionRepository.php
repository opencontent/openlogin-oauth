<?php

namespace App\Repository;

use App\Entity\SpidSession;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SpidSession|null find($id, $lockMode = null, $lockVersion = null)
 * @method SpidSession|null findOneBy(array $criteria, array $orderBy = null)
 * @method SpidSession[]    findAll()
 * @method SpidSession[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpidSessionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SpidSession::class);
    }

    // /**
    //  * @return SpidSession[] Returns an array of SpidSession objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SpidSession
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
