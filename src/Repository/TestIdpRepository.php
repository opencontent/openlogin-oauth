<?php

namespace App\Repository;

use App\Entity\TestIdp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TestIdp|null find($id, $lockMode = null, $lockVersion = null)
 * @method TestIdp|null findOneBy(array $criteria, array $orderBy = null)
 * @method TestIdp[]    findAll()
 * @method TestIdp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestIdpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TestIdp::class);
    }

    // /**
    //  * @return TestIdp[] Returns an array of TestIdp objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TestIdp
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
