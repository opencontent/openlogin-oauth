<?php

namespace App\Repository;

use App\Entity\SingleLogoutService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SingleLogoutService|null find($id, $lockMode = null, $lockVersion = null)
 * @method SingleLogoutService|null findOneBy(array $criteria, array $orderBy = null)
 * @method SingleLogoutService[]    findAll()
 * @method SingleLogoutService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SingleLogoutServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SingleLogoutService::class);
    }

    // /**
    //  * @return SingleLogoutService[] Returns an array of SingleLogoutService objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SingleLogoutService
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
