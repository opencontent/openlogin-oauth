<?php

namespace App\Repository;

use App\Entity\AssertionConsumerService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AssertionConsumerService|null find($id, $lockMode = null, $lockVersion = null)
 * @method AssertionConsumerService|null findOneBy(array $criteria, array $orderBy = null)
 * @method AssertionConsumerService[]    findAll()
 * @method AssertionConsumerService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AssertionConsumerServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AssertionConsumerService::class);
    }

    // /**
    //  * @return AssertionConsumerService[] Returns an array of AssertionConsumerService objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AssertionConsumerService
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
