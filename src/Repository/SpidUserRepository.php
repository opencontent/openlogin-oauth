<?php

namespace App\Repository;

use App\Entity\SpidSession;
use App\Entity\SpidUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SpidUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method SpidUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method SpidUser[]    findAll()
 * @method SpidUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpidUserRepository extends ServiceEntityRepository
{
    private $spidSessionRepository;

    public function __construct(ManagerRegistry $registry, SpidSessionRepository $spidSessionRepository)
    {
        parent::__construct($registry, SpidUser::class);
        $this->spidSessionRepository = $spidSessionRepository;
    }

/*
        foreach ($this->sp->getAttributes() as $key => $value) {
            if ($key === 'fiscalNumber') {
                $headers['X-Forwarded-User'] = str_replace(["\r\n", "\r", "\n"], ' ', $value);
            }
            $headers['X-Forwarded-User-' . $key] = str_replace(["\r\n", "\r", "\n"], ' ', $value);
        }
        $headers['X-Forwarded-User-Provider'] = AuthHelper::PROVIDER_SPID;
        $headers['X-Forwarded-User-Session'] = $_SESSION['spidSession']['sessionID'];
        $headers['X-Forwarded-User-Spid-Level'] = $_SESSION['spidSession']['level'];
        $headers['X-Forwarded-User-Spid-Idp'] = $_SESSION['spidSession']['idpEntityID']
 */


    public function storeSession($userIdentifier, $sessionId, $level, $idp, $attributes)
    {
        $user = $this->findOneBy(['username' => $userIdentifier]);
        if (!$user instanceof SpidUser){
            $user = new SpidUser();
            $user->setUsername($userIdentifier);
        }
        $session = $this->spidSessionRepository->findOneBy(['sessionId' => $sessionId]);
        if (!$session instanceof SpidSession){
            $session = (new SpidSession())
                ->setSessionId($sessionId)
                ->setLevel((int)$level)
                ->setIdp($idp)
                ->setAttributes($attributes);
            $user->addSession($session);
        }
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    // /**
    //  * @return SpidUser[] Returns an array of SpidUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SpidUser
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
