<?php

namespace App\Repository;

use App\Entity\AttributeConsumingService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AttributeConsumingService|null find($id, $lockMode = null, $lockVersion = null)
 * @method AttributeConsumingService|null findOneBy(array $criteria, array $orderBy = null)
 * @method AttributeConsumingService[]    findAll()
 * @method AttributeConsumingService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AttributeConsumingServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AttributeConsumingService::class);
    }

    // /**
    //  * @return AttributeConsumingService[] Returns an array of AttributeConsumingService objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AttributeConsumingService
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
