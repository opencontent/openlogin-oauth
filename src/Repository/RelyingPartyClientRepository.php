<?php

namespace App\Repository;

use App\Entity\RelyingPartyClient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RelyingPartyClient>
 *
 * @method RelyingPartyClient|null find($id, $lockMode = null, $lockVersion = null)
 * @method RelyingPartyClient|null findOneBy(array $criteria, array $orderBy = null)
 * @method RelyingPartyClient[]    findAll()
 * @method RelyingPartyClient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RelyingPartyClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RelyingPartyClient::class);
    }

    public function add(RelyingPartyClient $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(RelyingPartyClient $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return RelyingPartyClient[] Returns an array of RelyingPartyClient objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RelyingPartyClient
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
