<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OAuth2ClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $readOnlyOptions = [];
        if ($options['is_update']){
            $readOnlyOptions = ['attr' => ['readonly' => true,]];
        }
        $builder
            ->add('name')
            ->add('identifier', TextType::class, $readOnlyOptions)
//            ->add('secret', TextType::class, ['required' => false])
            ->add('redirect_uri', UrlType::class)
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'is_update' => false
        ]);
    }
}
