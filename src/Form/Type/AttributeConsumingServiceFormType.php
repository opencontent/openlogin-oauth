<?php

namespace App\Form\Type;

use App\Entity\AttributeConsumingService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AttributeConsumingServiceFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('displayName')
            ->add('index')
            ->add('gender')
            ->add('companyName')
            ->add('registeredOffice')
            ->add('fiscalNumber')
            ->add('ivaCode')
            ->add('idCard')
            ->add('spidCode')
            ->add('name')
            ->add('familyName')
            ->add('placeOfBirth')
            ->add('countyOfBirth')
            ->add('dateOfBirth')
            ->add('mobilePhone')
            ->add('email')
            ->add('address')
            ->add('expirationDate')
            ->add('digitalAddress')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AttributeConsumingService::class,
        ]);
    }
}
