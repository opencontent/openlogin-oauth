<?php

namespace App\Form\Type;

use App\Entity\Tenant;
use League\Bundle\OAuth2ServerBundle\Manager\ClientFilter;
use League\Bundle\OAuth2ServerBundle\Manager\ClientManagerInterface;
use League\Bundle\OAuth2ServerBundle\Model\AbstractClient;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConfigType extends AbstractType
{
    private $clients;

    public function __construct(ClientManagerInterface $clientManager)
    {
        $this->clients = $clientManager->list(ClientFilter::create());
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $clients =
        $builder
            ->add('tenant', EntityType::class, [
                'class' => Tenant::class,
                'choice_label' => 'displayName'
            ])
            ->add('oauth2_client', ChoiceType::class, [
                'choices' => $this->clients,
                'choice_value' => 'identifier',
                'choice_label' => function(?AbstractClient $client) {
                    return $client ? $client->getName() : '';
                },
            ])
            ->add('openlogin_container_name', TextType::class, [
                'attr' => ['placeholder' => 'openlogin-oauth'],
                'data' => 'openlogin-oauth',
            ])
            ->add('application_host', TextType::class, [
                'required' => false,
                'attr' => ['placeholder' => 'www2.stanzadelcittadino.it'],
            ])
            ->add('application_path_prefix', TextType::class, [
                'required' => false,
                'attr' => ['placeholder' => '/comune-di-bugliano/auth/login-open'],
            ])
            ->add('application_logout_redirect', TextType::class, [
                'required' => false,
                'attr' => ['placeholder' => 'https://www2.stanzadelcittadino.it/comune-di-bugliano'],
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
