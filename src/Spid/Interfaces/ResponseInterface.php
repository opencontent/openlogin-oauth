<?php

namespace App\Spid\Interfaces;

interface ResponseInterface
{
    // Validates a received response.
    // Throws exceptions on missing or invalid values.
    // returns false if response code is not success
    // returns true otherwise
    public function validate($xml, $hasAssertion): bool;
}
