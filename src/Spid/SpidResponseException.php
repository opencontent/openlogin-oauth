<?php

namespace App\Spid;

use Throwable;

class SpidResponseException extends SpidException
{
    const DEFAULT_MESSAGE = 'Errore nell\'elaborazione o nella validazione della risposta ricevuta dal gestore';

    const MESSAGES = [
        'ErrorCode nr08' => 'Formato della richiesta non conforme alle specifiche SAML',
        'ErrorCode nr09' => 'Parametro version non presente, malformato o diverso da 2.0',
        'ErrorCode nr11' => 'ID (Identificatore richiesta) non presente, malformato o non conforme',
        'ErrorCode nr12' => 'Autenticazione SPID non conforme o non specificata',
        'ErrorCode nr13' => 'IssueInstant non presente, malformato o non coerente con l\'orario di arrivo della richiesta',
        'ErrorCode nr14' => 'destination non presente, malformata o non coincidente con il Gestore delle identità ricevente la richiesta',
        'ErrorCode nr15' => 'attributo isPassive presente e attualizzato al valore true',
        'ErrorCode nr16' => 'AssertionConsumerService non correttamente valorizzato',
        'ErrorCode nr17' => 'Attributo Format dell\'elemento NameIDPolicy assente o non valorizzato secondo specifica',
        'ErrorCode nr18' => 'AttributeConsumerServiceIndex malformato o che riferisce a un valore non registrato nei metadati',
        'ErrorCode nr19' => 'Autenticazione fallita per ripetuta sottomissione di credenziali errate',
        'ErrorCode nr20' => 'Utente privo di credenziali compatibili con il livello richiesto dal fornitore del servizio',
        'ErrorCode nr21' => 'Timeout durante l\'autenticazione utente',
        'ErrorCode nr22' => 'Utente nega il consenso all\'invio di dati al SP in caso di sessione vigente',
        'ErrorCode nr23' => 'Utente con identità sospesa/revocata o con credenziali bloccate',
        'ErrorCode nr25' => 'Processo di autenticazione annullato dall\'utente',
    ];

    private $originalMessage;

    public function __construct($message = "", $errorCode = '', $code = 0, $previous = null)
    {
        $code = $code > 0 ?: str_replace('ErrorCode nr', '', $errorCode);
        $this->originalMessage = $message;
        $message = $this->decodeMessage($errorCode);
        parent::__construct($message, (int)$code, $previous);
    }

    /**
     * @return mixed|string
     */
    public function getOriginalMessage(): string
    {
        return $this->originalMessage ?? '';
    }

    private function decodeMessage($errorCode): string
    {
        if (empty($errorCode)) {
            return self::DEFAULT_MESSAGE;
        }

        if (isset(self::MESSAGES[$errorCode])){
            return self::DEFAULT_MESSAGE . ': ' . self::MESSAGES[$errorCode];
        }
        return self::DEFAULT_MESSAGE . ': ' . $errorCode;
    }

    public static function throw($error)
    {
        if (is_string($error)){
            throw new self($error);
        }
        if ($error instanceof SpidResponseException){
            throw $error;
        }

        if ($error instanceof Throwable){
            throw new SpidResponseException($error->getMessage(), '', $error->getCode(), $error);
        }
    }
}