<?php

namespace App\Spid;

use App\Entity\Certificate;
use App\Entity\Tenant;
use App\Entity\TestIdp;
use App\Repository\TenantRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\RequestStack;

class SpidClientFactory
{
    /**
     * @var TenantRepository
     */
    private $tenantRepository;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var Saml
     */
    private $client;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var LoggerInterface
     */
    private $logger;

    private $projectDir;

    private $dataDir;

    private $tenant;

    private $session;

    public function __construct(
        TenantRepository $tenantRepository,
        RequestStack $requestStack,
        Filesystem $filesystem,
        LoggerInterface $logger,
        $projectDir,
        $dataDir
    )
    {
        $this->tenantRepository = $tenantRepository;
        $this->requestStack = $requestStack;
        $this->filesystem = $filesystem;
        $this->logger = $logger;
        $this->projectDir = $projectDir;
        $this->dataDir = rtrim($dataDir, '/');
        $this->session = $requestStack->getSession();
        $this->filesystem->mkdir($this->dataDir);
    }

    /**
     * @return Saml
     * @throws SpidException
     */
    public function makeClient(): Saml
    {
        if (!$this->session->isStarted()) {
            $this->session->start();
        }
        if ($this->client === null) {
            $this->client = new Saml($this->buildSettings(), $autoconfigure = true);
        }

        return $this->client;
    }

    /**
     * @return array
     * @throws SpidException
     */
    private function buildSettings(): array
    {
        $tenant = $this->getTenantFromRequest();
        if (!$tenant instanceof Tenant) {
            $this->logger->error('Tenant not found');
            throw new SpidException("Saml settings cannot be loaded");
        }

        $this->logger->debug("Found tenant from request", ['id' => $tenant->getId(), 'entityid' => $tenant->getEntityId()]);
        $index = (int)$tenant->getLoginIndex();

        $mainCertificate = false;
        $certificates = $tenant->getCertificates();
        foreach ($certificates as $certificate){
            if ($certificate->getIndex() === $index){
                $mainCertificate = $certificate;
            }
        }
        if (!$mainCertificate instanceof Certificate){
            throw new SpidException("Certificate $index not found");
        }
        $keyFileName = md5($tenant->getId() . $mainCertificate->getKey());
        $keyFilePath = $this->dataDir . DIRECTORY_SEPARATOR . $keyFileName . '.key';
        if (!$this->filesystem->exists($keyFilePath)) {
            $this->filesystem->dumpFile($keyFilePath, $mainCertificate->getKey());
        }
        $certFileName = md5($tenant->getId() . $mainCertificate->getCertificate());
        $certFilePath = $this->dataDir . DIRECTORY_SEPARATOR . $certFileName . '.cert';
        if (!$this->filesystem->exists($certFilePath)) {
            $this->filesystem->dumpFile($certFilePath, $mainCertificate->getCertificate());
        }

        $additionalCertFiles = [];
        if ($certificates->count() > 1) {
            foreach ($certificates as $certificate) {
                if ($certificate->getIndex() !== $index) {
                    $additionalCertFileName = md5($tenant->getId() . $certificate->getCertificate());
                    $additionalCertFilePath = $this->dataDir . DIRECTORY_SEPARATOR . $additionalCertFileName . '.cert';
                    if (!$this->filesystem->exists($additionalCertFilePath)) {
                        $this->filesystem->dumpFile($additionalCertFilePath, $certificate->getCertificate());
                    }
                    $additionalCertFiles[] = $additionalCertFilePath;
                }
            }
        }

        $acsList = [];
        foreach ($tenant->getAssertionConsumerServices() as $assertionConsumerService) {
            $acsList[(int)$assertionConsumerService->getIndex()] = $assertionConsumerService->getUrl();
        }
        ksort($acsList);

        $sloList = [];
        foreach ($tenant->getSingleLogoutServices() as $singleLogoutService) {
            $sloList[(int)$singleLogoutService->getIndex()] = [
                $singleLogoutService->getUrl(),
                $singleLogoutService->getIsRedirect() ? 'REDIRECT' : ''
            ];
        }
        ksort($sloList);

        $attributeList = [];
        foreach ($tenant->getAttributeConsumingServices() as $attributeConsumingService) {
            $attributeFields = [
                "gender" => true,
                "companyName" => true,
                "registeredOffice" => true,
                "fiscalNumber" => true,
                "ivaCode" => true,
                "idCard" => true,
                "spidCode" => true,
                "name" => true,
                "familyName" => true,
                "placeOfBirth" => true,
                "countyOfBirth" => true,
                "dateOfBirth" => true,
                "mobilePhone" => true,
                "email" => true,
                "address" => true,
                "expirationDate" => true,
                "digitalAddress" => true,
            ];
            if (!$attributeConsumingService->getGender()) {
                unset($attributeFields['gender']);
            }
            if (!$attributeConsumingService->getCompanyName()) {
                unset($attributeFields['companyName']);
            }
            if (!$attributeConsumingService->getRegisteredOffice()) {
                unset($attributeFields['registeredOffice']);
            }
            if (!$attributeConsumingService->getFiscalNumber()) {
                unset($attributeFields['fiscalNumber']);
            }
            if (!$attributeConsumingService->getIvaCode()) {
                unset($attributeFields['ivaCode']);
            }
            if (!$attributeConsumingService->getIdCard()) {
                unset($attributeFields['idCard']);
            }
            if (!$attributeConsumingService->getSpidCode()) {
                unset($attributeFields['spidCode']);
            }
            if (!$attributeConsumingService->getName()) {
                unset($attributeFields['name']);
            }
            if (!$attributeConsumingService->getFamilyName()) {
                unset($attributeFields['familyName']);
            }
            if (!$attributeConsumingService->getPlaceOfBirth()) {
                unset($attributeFields['placeOfBirth']);
            }
            if (!$attributeConsumingService->getCountyOfBirth()) {
                unset($attributeFields['countyOfBirth']);
            }
            if (!$attributeConsumingService->getDateOfBirth()) {
                unset($attributeFields['dateOfBirth']);
            }
            if (!$attributeConsumingService->getMobilePhone()) {
                unset($attributeFields['mobilePhone']);
            }
            if (!$attributeConsumingService->getEmail()) {
                unset($attributeFields['email']);
            }
            if (!$attributeConsumingService->getAddress()) {
                unset($attributeFields['address']);
            }
            if (!$attributeConsumingService->getExpirationDate()) {
                unset($attributeFields['expirationDate']);
            }
            if (!$attributeConsumingService->getDigitalAddress()) {
                unset($attributeFields['digitalAddress']);
            }
            $attributeList[(int)$attributeConsumingService->getIndex()] = array_keys($attributeFields);
        }
        ksort($attributeList);

        $metadataFolder = $this->projectDir . DIRECTORY_SEPARATOR . 'spid' . DIRECTORY_SEPARATOR . 'idp_metadata' . DIRECTORY_SEPARATOR;
        $idpTest = $tenant->getIdpTest();
        if ($idpTest instanceof TestIdp){
            $idpTestFilePath = $metadataFolder . $idpTest->getIdentifier() . '.xml';
            if (!$this->filesystem->exists($idpTestFilePath)) {
                $this->filesystem->dumpFile($idpTestFilePath, $idpTest->getMetadata());
            }
        }

        $settings = [
            'sp_entityid' => $tenant->getEntityId(),
            'sp_key_file' => $keyFilePath,
            'sp_cert_file' => $certFilePath,
            'sp_comparison' => $tenant->getComparison() ?? 'minimum',
            'sp_level' => $tenant->getSpidLevel() ?? 2,
            'sp_assertionconsumerservice' => array_values($acsList),
            'sp_singlelogoutservice' => array_values($sloList),
            'sp_attributeconsumingservice' => $attributeList,
            'sp_org_name' => (string)$tenant->getOrganizationName(),
            'sp_org_display_name' => (string)$tenant->getDisplayName(),
            'sp_contact_ipa_code' => (string)$tenant->getIpaCode(),
            'sp_contact_fiscal_code' => (string)$tenant->getFiscalCode(),
            'sp_contact_email' => (string)$tenant->getEmail(),
            'sp_contact_phone' => (string)$tenant->getPhone(),
            'idp_metadata_folder' => $metadataFolder,
            'accepted_clock_skew_seconds' => 100,
            'additional_cert_files' => $additionalCertFiles,
        ];

        $this->logger->debug("Build settings from tenant", ['id' => $tenant->getId(), 'settings' => $settings]);

        return $settings;
    }

    public function getTenantFromRequest(): ?Tenant
    {
        if ($this->tenant === null) {
            $currentHost = rtrim($this->requestStack->getCurrentRequest()->getSchemeAndHttpHost(), '/') . '/';
            $this->logger->debug('Find tenant from request', ['host' => $currentHost]);
            $this->tenant = $this->tenantRepository->findOneBy(['openLoginUri' => $currentHost]);
        }

        $defaultTenant = (int)getenv('DEFAULT_TENANT_ID');
        if ($defaultTenant > 0 && $this->tenant === null){
            $this->logger->debug('Use default tenant', ['default_tenant_id' => $defaultTenant]);
            $this->tenant = $this->tenantRepository->find($defaultTenant);
        }

        return $this->tenant;
    }

    public static function encrypt($string): string
    {
        $cipher = 'aes-256-cbc';
        $ivLength = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivLength);

        return base64_encode($iv . openssl_encrypt($string, $cipher, getenv('APP_SECRET'), OPENSSL_RAW_DATA, $iv));
    }

    public static function getIdpInfoList(): array
    {
        return [
            [
                "ipa_entity_code" => "idp_1",
                "entity_id" => "https://identity.infocert.it",
                "entity_name" => "Infocert ID",
                "metadata_url" => "https://identity.infocert.it/metadata/metadata.xml",
                "entity_type" => "IdP",
                "_identifier" => 'infocertid',
            ],
            [
                "ipa_entity_code" => "idp_2",
                "entity_id" => "https://posteid.poste.it",
                "entity_name" => "Poste ID",
                "metadata_url" => "https://posteid.poste.it/jod-fs/metadata/metadata.xml",
                "entity_type" => "IdP",
                "_identifier" => 'posteid',
            ],
            [
                "ipa_entity_code" => "idp_3",
                "entity_id" => "https://login.id.tim.it/affwebservices/public/saml2sso",
                "entity_name" => "Tim ID",
                "metadata_url" => "https://login.id.tim.it/spid-services/MetadataBrowser/idp",
                "entity_type" => "IdP",
                "_identifier" => 'timid',
            ],
            [
                "ipa_entity_code" => "idp_4",
                "entity_id" => "https://identity.sieltecloud.it",
                "entity_name" => "Sielte ID",
                "metadata_url" => "https://identity.sieltecloud.it/simplesaml/metadata.xml",
                "entity_type" => "IdP",
                "_identifier" => 'sielteid',
            ],
            [
                "ipa_entity_code" => "idp_5",
                "entity_id" => "https://loginspid.aruba.it",
                "entity_name" => "Aruba ID",
                "metadata_url" => "https://loginspid.aruba.it/metadata",
                "entity_type" => "IdP",
                "_identifier" => 'arubaid',
            ],
            [
                "ipa_entity_code" => "idp_6",
                "entity_id" => "https://idp.namirialtsp.com/idp",
                "entity_name" => "Namirial ID",
                "metadata_url" => "https://idp.namirialtsp.com/idp/metadata",
                "entity_type" => "IdP",
                "_identifier" => 'namirialid',
            ],
            [
                "ipa_entity_code" => "idp_7",
                "entity_id" => "https://spid.register.it",
                "entity_name" => "SPIDItalia Register.it",
                "metadata_url" => "https://spid.register.it/login/metadata",
                "entity_type" => "IdP",
                "_identifier" => 'spiditalia',
            ],
//            [
//                "ipa_entity_code" => "idp_8",
//                "entity_id" => "https://spid.intesa.it",
//                "entity_name" => "Intesa ID",
//                "metadata_url" => "https://spid.intesa.it/metadata/metadata.xml",
//                "entity_type" => "IdP",
//                "_identifier" => 'intesaid',
//            ],
            [
                "ipa_entity_code" => "idp_9",
                "entity_id" => "https://id.lepida.it/idp/shibboleth",
                "entity_name" => "Lepida ID",
                "metadata_url" => "https://id.lepida.it/idp/shibboleth",
                "entity_type" => "IdP",
                "_identifier" => 'lepidaid',
            ],
            [
                "ipa_entity_code" => "idp_10",
                "entity_id" => "https://spid.teamsystem.com/idp",
                "entity_name" => "TeamSystem ID",
                "metadata_url" => "https://spid.teamsystem.com/idp",
                "entity_type" => "IdP",
                "_identifier" => 'teamsystemid',
            ],
            [
                "ipa_entity_code" => "idp_11",
                "entity_id" => "https://id.eht.eu",
                "entity_name" => "EtnaHitech S.C.p.A.",
                "metadata_url" => "https://id.eht.eu/metadata.xml",
                "entity_type" => "IdP",
                "_identifier" => 'etnaid',
            ],
            [
                "ipa_entity_code" => "idp_12",
                "entity_id" => "https://loginspid.infocamere.it",
                "entity_name" => "InfoCamere S.C.p.A.",
                "metadata_url" => "https://loginspid.infocamere.it/metadata",
                "entity_type" => "IdP",
                "_identifier" => 'infocamereid',
            ],
            [
                "ipa_entity_code" => "idp_13",
                "entity_id" => "https://idp.intesigroup.com ",
                "entity_name" => "Intesi Group S.p.A.",
                "metadata_url" => "https://spid.intesigroup.com/metadata/metadata.xml",
                "entity_type" => "IdP",
                "_identifier" => 'intesigroupspid',
            ],
        ];
    }
}
