<?php

namespace App\Spid\Saml\Out;

use App\Spid\Interfaces\RequestInterface;
use App\Spid\Saml\Settings;
use App\Spid\Saml\SignatureUtils;
use App\Spid\SpidException as Exception;
use SimpleXMLElement;

class AuthnRequest extends Base implements RequestInterface
{
    private $currentBinding;

    /**
     * @throws Exception
     */
    public function generateXml()
    {
        $id = $this->generateID();
        $issueInstant = $this->generateIssueInstant();
        $entityId = $this->idp->sp->settings['sp_entityid'];

        $destination = $this->idp->metadata['idpEntityId'];

        $assertID = $this->idp->assertID;
        $attrID = $this->idp->attrID;
        $level = $this->idp->level;
        $comparison = $this->idp->sp->settings['sp_comparison'] ?? "exact";
        $proxy = "";
        $force = ($level > 1 || $comparison == "minimum") ? "true" : "false";
        $requestedAuthnContext = <<<RAC
    <samlp:RequestedAuthnContext Comparison="$comparison">
        <saml:AuthnContextClassRef>https://www.spid.gov.it/SpidL$level</saml:AuthnContextClassRef>
    </samlp:RequestedAuthnContext>
RAC;

        if ($this->idp->isFederaGateway()) {
            $proxy = '<samlp:Scoping ProxyCount="2"><samlp:RequesterID>' . $destination . '</samlp:RequesterID></samlp:Scoping>';
            $force = "false";
            $requestedAuthnContext = <<<RAC
    <samlp:RequestedAuthnContext Comparison="exact">
        <saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">urn:oasis:names:tc:SAML:2.0:ac:classes:SecureRemotePassword</saml:AuthnContextClassRef>
        <saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">urn:oasis:names:tc:SAML:2.0:ac:classes:Smartcard</saml:AuthnContextClassRef>
    </samlp:RequestedAuthnContext>
RAC;
            $destination = $this->currentBinding ? parent::getBindingLocation($this->currentBinding) : $destination;
            $destination = str_replace('&', '&amp;', $destination);
            $attrID = null;
        }

        $authnRequestXml = <<<XML
<samlp:AuthnRequest xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol"
    xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
    ID="$id" 
    Version="2.0"
    IssueInstant="$issueInstant"
    Destination="$destination"
    ForceAuthn="$force">
    <saml:Issuer
        NameQualifier="$entityId"
        Format="urn:oasis:names:tc:SAML:2.0:nameid-format:entity">$entityId</saml:Issuer>
    <samlp:NameIDPolicy Format="urn:oasis:names:tc:SAML:2.0:nameid-format:transient" />
    $requestedAuthnContext
    $proxy
</samlp:AuthnRequest>
XML;
        $xml = new SimpleXMLElement($authnRequestXml);

        if (!is_null($assertID)) {
            if ($this->idp->isFederaGateway()) {
                $xml->addAttribute('AssertionConsumerServiceURL', $this->idp->sp->settings['sp_assertionconsumerservice'][$assertID]);
            }else{
                $xml->addAttribute('AssertionConsumerServiceIndex', $assertID);
            }
        }
        if (!is_null($attrID)) {
            $xml->addAttribute('AttributeConsumingServiceIndex', $attrID);
        }

        $this->xml = $xml->asXML();
    }

    /**
     * @throws Exception
     */
    public function redirectUrl($redirectTo = null): string
    {
        $location = parent::getBindingLocation(Settings::BINDING_REDIRECT);
        if (is_null($this->xml)) {
            $this->currentBinding = Settings::BINDING_REDIRECT;
            $this->generateXml();
        }
        return parent::redirect($location, $redirectTo);
    }

    /**
     * @throws Exception
     */
    public function httpPost($redirectTo = null): string
    {
        $location = parent::getBindingLocation(Settings::BINDING_POST);
        if (is_null($this->xml)) {
            $this->currentBinding = Settings::BINDING_POST;
            $this->generateXml();
        }
        $this->xml = SignatureUtils::signXml($this->xml, $this->idp->sp->settings);
        return parent::postForm($location, $redirectTo);
    }
}
