<?php

namespace App\Spid\Saml\In;

use App\Spid\Session;
use App\Spid\SpidException as Exception;
use DOMDocument;

class FederaResponse extends Response
{
    protected function validateIssuer($xml)
    {
        if (trim($xml->getElementsByTagName('Issuer')->item(0)->nodeValue) != trim($_SESSION['idpEntityId'])) {
            throw new Exception(
                "Invalid Issuer attribute, expected " . $_SESSION['idpEntityId'] .
                " but received " . $xml->getElementsByTagName('Issuer')->item(0)->nodeValue
            );
        } elseif ($xml->getElementsByTagName('Issuer')->item(0)->hasAttribute('Format')
            && $xml->getElementsByTagName('Issuer')->item(0)->getAttribute('Format') != 'urn:oasis:names:tc:SAML:2.0:nameid-format:entity') {
            throw new Exception(
                "Invalid Issuer attribute, expected 'urn:oasis:names:tc:SAML:2.0:nameid-format:" .
                "entity'" . " but received " . $xml->getElementsByTagName('Issuer')->item(0)->getAttribute('Format')
            );
        }
    }

    protected function validateAssertionIssuer($xml)
    {
        if ($xml->getElementsByTagName('Issuer')->item(1)->nodeValue != $_SESSION['idpEntityId']) {
            throw new Exception(
                "Invalid Issuer attribute, expected " . $_SESSION['idpEntityId'] .
                " but received " . $xml->getElementsByTagName('Issuer')->item(1)->nodeValue
            );
        }
    }

    protected function validateAssertionNameQualifier($xml)
    {
        if ($xml->getElementsByTagName('NameID')->length == 0) {
            throw new Exception("Missing NameID attribute");
        } elseif (
            $xml->getElementsByTagName('NameID')->item(0)->getAttribute('Format') != 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient'
            && $xml->getElementsByTagName('NameID')->item(0)->getAttribute('Format') != 'urn:oasis:names:tc:SAML:2.0:assertion#CODFISC' // cns
        ) {
            throw new Exception(
                "Invalid NameID attribute, expected " .
                "'urn:oasis:names:tc:SAML:2.0:nameid-format:transient'" . " but received " .
                $xml->getElementsByTagName('NameID')->item(0)->getAttribute('Format')
            );
        } elseif (strpos($_SESSION['idpEntityId'], $xml->getElementsByTagName('NameID')->item(0)->getAttribute('NameQualifier')) === false) {
            throw new Exception(
                "Invalid NameQualifier attribute, expected " . $_SESSION['idpEntityId'] .
                " but received " . $xml->getElementsByTagName('NameID')->item(0)->getAttribute('NameQualifier')
            );
        }
    }

    protected function spidSession(DOMDocument $xml): Session
    {
        $session = new Session();

        $attributes = [];
        $attributeStatements = $xml->getElementsByTagName('AttributeStatement');
        $attributeMap = [
            'sesso' => 'gender',
            'CodiceFiscale' => 'fiscalNumber',
            'nome' => 'name',
            'cognome' => 'familyName',
            'luogoNascita' => 'placeOfBirth',
            'provinciaNascita' => 'countyOfBirth',
            'dataNascita' => 'dateOfBirth',
            'emailAddressPersonale' => 'email',
            'address' => 'address',
            'emailAddress' => 'digitalAddress',
            'expirationDate' => 'expirationDate',
            'companyName' => 'companyName',
            'registeredOffice' => 'registeredOffice',
            'ivaCode' => 'ivaCode',
            'idCard' => 'idCard',
            'spidCode' => 'spidCode',
            'cellulare' => 'mobilePhone',
            // cns
            'NOME' => 'name',
            'COGNOME' => 'familyName',
            'CODICE_FISCALE' => 'fiscalNumber',
            'EMAIL' => 'email',
        ];

        if ($attributeStatements->length > 0) {
            foreach ($attributeStatements as $attributeStatement) {
                foreach ($attributeStatement->childNodes as $attr) {
                    if ($attr->hasAttributes()) {
                        $name = $attr->attributes->getNamedItem('Name')->value;
                        if (isset($attributeMap[$name])) {
                            $name = $attributeMap[$name];
                        }
                        $attributes[$name] = trim($attr->nodeValue);
                    }
                }
            }
        }

        $session->sessionID = $_SESSION['RequestID'];
        $session->idp = $_SESSION['idpName'];
        $session->idpEntityID = $xml->getElementsByTagName('Issuer')->item(0)->nodeValue;
        $session->attributes = $attributes;
        $session->level = substr($xml->getElementsByTagName('AuthnContextClassRef')->item(0)->nodeValue, -1);
        return $session;
    }

    protected function validateAssertionAuthnStatement($xml)
    {

    }
}