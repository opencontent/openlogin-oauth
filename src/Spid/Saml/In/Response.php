<?php

namespace App\Spid\Saml\In;

use App\Spid\Interfaces\ResponseInterface;
use App\Spid\Session;
use App\Spid\Saml;
use DOMDocument;
use App\Spid\SpidResponseException as Exception;

class Response implements ResponseInterface
{
    protected $saml;

    public function __construct(Saml $saml)
    {
        $this->saml = $saml;
    }

    /**
     * @throws Exception
     */
    public function validate($xml, $hasAssertion): bool
    {
        $accepted_clock_skew_seconds = $this->saml->settings['accepted_clock_skew_seconds'] ?? 0;

        $this->validateVersion($xml);

        $this->validateIssueInstant($xml, $accepted_clock_skew_seconds);

        $this->validateInResponseTo($xml);

        $this->validateDestination($xml);

        $this->validateIssuer($xml);

        if ($hasAssertion) {
            $this->validateAssertion($xml, $accepted_clock_skew_seconds);
            $this->validateAssertionIssuer($xml);
            $this->validateAssertionConditions($xml, $accepted_clock_skew_seconds);
            $this->validateAssertionAudience($xml);
            $this->validateSubjectConfirmationData($xml, $accepted_clock_skew_seconds);
            $this->validateAssertionNameQualifier($xml);
            if ($xml->getElementsByTagName('Attribute')->length == 0) {
                throw new Exception("Missing Attribute Element");
            }
            if ($xml->getElementsByTagName('AttributeValue')->length == 0) {
                throw new Exception("Missing AttributeValue Element");
            }
        }

        $this->validateStatus($xml, $hasAssertion);
        $this->validateAssertionAuthnStatement($xml);

        // Response OK
        $session = $this->spidSession($xml);
        $_SESSION['spidSession'] = (array)$session;
        unset($_SESSION['RequestID']);
        unset($_SESSION['idpName']);
        unset($_SESSION['idpEntityId']);
        unset($_SESSION['acsUrl']);
        return true;
    }

    protected function validateVersion($xml)
    {
        $root = $xml->getElementsByTagName('Response')->item(0);
        if ($root->getAttribute('Version') == "") {
            throw new Exception("Missing Version attribute", 'ErrorCode nr09');
        } elseif ($root->getAttribute('Version') != '2.0') {
            throw new Exception("Invalid Version attribute", 'ErrorCode nr09');
        }
    }

    protected function validateIssueInstant($xml, $accepted_clock_skew_seconds)
    {
        $root = $xml->getElementsByTagName('Response')->item(0);
        if ($root->getAttribute('IssueInstant') == "") {
            throw new Exception("Missing IssueInstant attribute on Response", 'ErrorCode nr13');
        } elseif (!$this->validateDate($root->getAttribute('IssueInstant'))) {
            throw new Exception("Invalid IssueInstant attribute on Response", 'ErrorCode nr13');
        } elseif (strtotime($root->getAttribute('IssueInstant')) >
            strtotime('now') + $accepted_clock_skew_seconds) {
            throw new Exception("IssueInstant attribute on Response is in the future", 'ErrorCode nr13');
        }
    }

    protected function validateInResponseTo($xml)
    {
        $root = $xml->getElementsByTagName('Response')->item(0);
        if ($root->getAttribute('InResponseTo') == "" || !isset($_SESSION['RequestID'])) {
            throw new Exception(
                "Missing InResponseTo attribute, or request ID was not saved correctly " .
                "for comparison",
                'ErrorCode nr11'
            );
        } elseif ($root->getAttribute('InResponseTo') != $_SESSION['RequestID']) {
            throw new Exception(
                "Invalid InResponseTo attribute, expected " . $_SESSION['RequestID'] .
                " but received " . $root->getAttribute('InResponseTo'),
                'ErrorCode nr11'
            );
        }
    }

    protected function validateDestination($xml)
    {
        $root = $xml->getElementsByTagName('Response')->item(0);
        if ($root->getAttribute('Destination') == "") {
            throw new Exception("Missing Destination attribute", 'ErrorCode nr14');
        } elseif ($root->getAttribute('Destination') != $_SESSION['acsUrl']) {
            throw new Exception(
                "Invalid Destination attribute, expected " . $_SESSION['acsUrl'] .
                " but received " . $root->getAttribute('Destination'),
                'ErrorCode nr14'
            );
        }
    }

    protected function validateIssuer($xml)
    {
        if ($xml->getElementsByTagName('Issuer')->length == 0) {
            throw new Exception("Missing Issuer attribute");
            //check item 0, this the Issuer element child of Response
        } elseif ($xml->getElementsByTagName('Issuer')->item(0)->nodeValue != $_SESSION['idpEntityId']) {
            throw new Exception(
                "Invalid Issuer attribute, expected " . $_SESSION['idpEntityId'] .
                " but received " . $xml->getElementsByTagName('Issuer')->item(0)->nodeValue
            );
        } elseif ($xml->getElementsByTagName('Issuer')->item(0)->hasAttribute('Format')
            && $xml->getElementsByTagName('Issuer')->item(0)->getAttribute('Format') !=
            'urn:oasis:names:tc:SAML:2.0:nameid-format:entity') {
            throw new Exception(
                "Invalid Issuer attribute, expected 'urn:oasis:names:tc:SAML:2.0:nameid-format:" .
                "entity'" . " but received " . $xml->getElementsByTagName('Issuer')->item(0)->getAttribute('Format')
            );
        }
    }

    protected function validateStatus($xml, $hasAssertion)
    {
        if ($xml->getElementsByTagName('Status')->length <= 0) {
            throw new Exception("Missing Status element");
        } elseif ($xml->getElementsByTagName('Status')->item(0) == null) {
            throw new Exception("Missing Status element");
        } elseif ($xml->getElementsByTagName('StatusCode')->item(0) == null) {
            throw new Exception("Missing StatusCode element");
        } elseif ($xml->getElementsByTagName('StatusCode')->item(0)->getAttribute('Value') ==
            'urn:oasis:names:tc:SAML:2.0:status:Success') {
            if ($hasAssertion && $xml->getElementsByTagName('AuthnStatement')->length <= 0) {
                throw new Exception("Missing AuthnStatement element");
            }
        } elseif ($xml->getElementsByTagName('StatusCode')->item(0)->getAttribute('Value') !=
            'urn:oasis:names:tc:SAML:2.0:status:Success') {
            $errorCode = $xml->getElementsByTagName('StatusMessage')->length > 0 ?
                $xml->getElementsByTagName('StatusMessage')->item(0)->nodeValue : 'autenticazione fallita';
            throw new Exception(
                'StatusCode is not Success',
                $errorCode
            );
        } elseif ($xml->getElementsByTagName('StatusCode')->item(1)->getAttribute('Value') ==
            'urn:oasis:names:tc:SAML:2.0:status:AuthnFailed') {
            throw new Exception("AuthnFailed AuthnStatement element");
        } else {
            // Status code != success
            throw new Exception("StatusCode is not success");
        }
    }

    protected function validateDate($date): bool
    {
        if (preg_match('/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(\.\d+)?Z$/', $date, $parts)) {
            $time = gmmktime($parts[4], $parts[5], $parts[6], $parts[2], $parts[3], $parts[1]);

            $input_time = strtotime($date);
            if ($input_time === false) {
                return false;
            }

            return $input_time == $time;
        } else {
            return false;
        }
    }

    protected function validateAssertion($xml, $accepted_clock_skew_seconds)
    {
        if ($xml->getElementsByTagName('Assertion')->item(0)->getAttribute('ID') == "" ||
            $xml->getElementsByTagName('Assertion')->item(0)->getAttribute('ID') == null) {
            throw new Exception("Missing ID attribute on Assertion");
        } elseif ($xml->getElementsByTagName('Assertion')->item(0)->getAttribute('Version') != '2.0') {
            throw new Exception("Invalid Version attribute on Assertion");
        } elseif ($xml->getElementsByTagName('Assertion')->item(0)->getAttribute('IssueInstant') == "") {
            throw new Exception("Invalid IssueInstant attribute on Assertion", 'ErrorCode nr13');
        } elseif (!$this->validateDate(
            $xml->getElementsByTagName('Assertion')->item(0)->getAttribute('IssueInstant')
        )) {
            throw new Exception("Invalid IssueInstant attribute on Assertion", 'ErrorCode nr13');
        } elseif (strtotime($xml->getElementsByTagName('Assertion')->item(0)->getAttribute('IssueInstant')) >
            strtotime('now') + $accepted_clock_skew_seconds) {
            throw new Exception("IssueInstant attribute on Assertion is in the future", 'ErrorCode nr13');
        }
    }

    protected function validateAssertionIssuer($xml)
    {
        // check item 1, this must be the Issuer element child of Assertion
        if ($xml->getElementsByTagName('Issuer')->item(1)->nodeValue != $_SESSION['idpEntityId']) {
            throw new Exception(
                "Invalid Issuer attribute, expected " . $_SESSION['idpEntityId'] .
                " but received " . $xml->getElementsByTagName('Issuer')->item(1)->nodeValue
            );
        } elseif ($xml->getElementsByTagName('Issuer')->item(1)->getAttribute('Format') !=
            'urn:oasis:names:tc:SAML:2.0:nameid-format:entity') {
            throw new Exception(
                "Invalid Issuer attribute, expected 'urn:oasis:names:tc:SAML:2.0:nameid-format:" .
                "entity'" . " but received " . $xml->getElementsByTagName('Issuer')->item(1)->getAttribute('Format')
            );
        }
    }

    protected function validateAssertionConditions($xml, $accepted_clock_skew_seconds)
    {
        if ($xml->getElementsByTagName('Conditions')->length == 0) {
            throw new Exception("Missing Conditions attribute");
        } elseif ($xml->getElementsByTagName('Conditions')->item(0)->getAttribute('NotBefore') == "") {
            throw new Exception("Missing NotBefore attribute");
        } elseif (!$this->validateDate(
            $xml->getElementsByTagName('Conditions')->item(0)->getAttribute('NotBefore')
        )) {
            throw new Exception("Invalid NotBefore attribute");
        } elseif (strtotime($xml->getElementsByTagName('Conditions')->item(0)->getAttribute('NotBefore')) >
            strtotime('now') + $accepted_clock_skew_seconds) {
            throw new Exception("NotBefore attribute is in the future");
        } elseif ($xml->getElementsByTagName('Conditions')->item(0)->getAttribute('NotOnOrAfter') == "") {
            throw new Exception("Missing NotOnOrAfter attribute");
        } elseif (!$this->validateDate(
            $xml->getElementsByTagName('Conditions')->item(0)->getAttribute('NotOnOrAfter')
        )) {
            throw new Exception("Invalid NotOnOrAfter attribute");
        } elseif (strtotime($xml->getElementsByTagName('Conditions')->item(0)->getAttribute('NotOnOrAfter')) <=
            strtotime('now') - $accepted_clock_skew_seconds) {
            throw new Exception("NotOnOrAfter attribute is in the past");
        }

    }

    protected function validateAssertionAudience($xml)
    {
        if ($xml->getElementsByTagName('AudienceRestriction')->length == 0) {
            throw new Exception("Missing AudienceRestriction attribute");
        }

        if ($xml->getElementsByTagName('Audience')->length == 0) {
            throw new Exception("Missing Audience attribute");
        } elseif ($xml->getElementsByTagName('Audience')->item(0)->nodeValue !=
            $this->saml->settings['sp_entityid']) {
            throw new Exception(
                "Invalid Audience attribute, expected " . $this->saml->settings['sp_entityid'] .
                " but received " . $xml->getElementsByTagName('Audience')->item(0)->nodeValue
            );
        }
    }

    protected function validateAssertionNameQualifier($xml)
    {
        if ($xml->getElementsByTagName('NameID')->length == 0) {
            throw new Exception("Missing NameID attribute");
        } elseif ($xml->getElementsByTagName('NameID')->item(0)->getAttribute('Format') !=
            'urn:oasis:names:tc:SAML:2.0:nameid-format:transient') {
            throw new Exception(
                "Invalid NameID attribute, expected " .
                "'urn:oasis:names:tc:SAML:2.0:nameid-format:transient'" . " but received " .
                $xml->getElementsByTagName('NameID')->item(0)->getAttribute('Format')
            );
        } elseif ($xml->getElementsByTagName('NameID')->item(0)->getAttribute('NameQualifier') !=
            $_SESSION['idpEntityId']) {
            throw new Exception(
                "Invalid NameQualifier attribute, expected " . $_SESSION['idpEntityId'] .
                " but received " . $xml->getElementsByTagName('NameID')->item(0)->getAttribute('NameQualifier')
            );
        }
    }

    protected function validateSubjectConfirmationData($xml, $accepted_clock_skew_seconds)
    {
        if ($xml->getElementsByTagName('SubjectConfirmationData')->length == 0) {
            throw new Exception("Missing SubjectConfirmationData attribute");
        } elseif ($xml->getElementsByTagName('SubjectConfirmationData')->item(0)->getAttribute('InResponseTo') !=
            $_SESSION['RequestID']) {
            throw new Exception(
                "Invalid SubjectConfirmationData attribute, expected " . $_SESSION['RequestID'] .
                " but received " .
                $xml->getElementsByTagName('SubjectConfirmationData')->item(0)->getAttribute('InResponseTo')
            );
        } elseif (strtotime(
                $xml->getElementsByTagName('SubjectConfirmationData')->item(0)->getAttribute('NotOnOrAfter')
            ) <= strtotime('now') - $accepted_clock_skew_seconds) {
            throw new Exception("Invalid NotOnOrAfter attribute");
        } elseif ($xml->getElementsByTagName('SubjectConfirmationData')->item(0)->getAttribute('Recipient') !=
            $_SESSION['acsUrl']) {
            throw new Exception(
                "Invalid Recipient attribute, expected " . $_SESSION['acsUrl'] .
                " but received " .
                $xml->getElementsByTagName('SubjectConfirmationData')->item(0)->getAttribute('Recipient')
            );
        } elseif ($xml->getElementsByTagName('SubjectConfirmation')->item(0)->getAttribute('Method') !=
            'urn:oasis:names:tc:SAML:2.0:cm:bearer') {
            throw new Exception(
                "Invalid Method attribute, expected 'urn:oasis:names:tc:SAML:2.0:cm:bearer'" .
                " but received " .
                $xml->getElementsByTagName('SubjectConfirmation')->item(0)->getAttribute('Method')
            );
        }
    }

    protected function spidSession(DOMDocument $xml): Session
    {
        $session = new Session();

        $attributes = [];
        $attributeStatements = $xml->getElementsByTagName('AttributeStatement');

        if ($attributeStatements->length > 0) {
            foreach ($attributeStatements->item(0)->childNodes as $attr) {
                if ($attr->hasAttributes()) {
                    $attributes[$attr->attributes->getNamedItem('Name')->value] = trim($attr->nodeValue);
                }
            }
        }

        $session->sessionID = $_SESSION['RequestID'];
        $session->idp = $_SESSION['idpName'];
        $session->idpEntityID = $xml->getElementsByTagName('Issuer')->item(0)->nodeValue;
        $session->attributes = $attributes;
        $session->level = substr($xml->getElementsByTagName('AuthnContextClassRef')->item(0)->nodeValue, -1);
        return $session;
    }

    protected function validateAssertionAuthnStatement($xml)
    {
        if ($xml->getElementsByTagName('AuthnContextClassRef')->length > 0) {
            $responseClassRef = (string)$xml->getElementsByTagName('AuthnContextClassRef')->item(0)->nodeValue;

            if (empty($responseClassRef) || strpos($responseClassRef, 'https://www.spid.gov.it/SpidL') === false){
                throw new Exception('Wrong AuthnContextClassRef value');
            }

            //$requestComparison = $this->saml->settings['sp_comparison'] ?? 'minimum';
            $requestLevel = $this->saml->settings['sp_level'] ?? 0;

            $responseLevel = intval(str_replace(
                'https://www.spid.gov.it/SpidL',
                '',
                $responseClassRef
            ));
            if ($responseLevel < $requestLevel){
                throw new Exception('Wrong response AuthnContextClassRef');
            }
        } else {
            throw new Exception('Missing AuthnContextClassRef');
        }
    }
}
