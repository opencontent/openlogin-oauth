<?php

namespace App\Spid\Saml;

use App\Spid\Interfaces\IdpInterface;
use App\Spid\Saml\Out\AuthnRequest;
use App\Spid\Saml\Out\LogoutRequest;
use App\Spid\Session;
use App\Spid\Saml\Out\LogoutResponse;
use App\Spid\SpidException as Exception;
use RobRichards\XMLSecLibs\XMLSecurityKey;

class Idp implements IdpInterface
{
    public $idpFileName;

    public $metadata;

    public $sp;

    public $assertID;

    public $attrID;

    public $level = 1;

    public $session;

    public function __construct($sp)
    {
        $this->sp = $sp;
    }

    /**
     * @throws Exception
     */
    public function loadFromXml($xmlFile): Idp
    {
        if (strpos($xmlFile, $this->sp->settings['idp_metadata_folder']) !== false) {
            $fileName = $xmlFile;
        } else {
            $fileName = $this->sp->settings['idp_metadata_folder'] . $xmlFile . ".xml";
        }
        if (!file_exists($fileName)) {
            throw new Exception("Metadata file $fileName not found", 1);
        }
        if (!is_readable($fileName)) {
            throw new Exception("Metadata file $fileName is not readable. Please check file permissions.", 1);
        }
        $xml = simplexml_load_file($fileName);

        $xml->registerXPathNamespace('md', 'urn:oasis:names:tc:SAML:2.0:metadata');
        $xml->registerXPathNamespace('ds', 'http://www.w3.org/2000/09/xmldsig#');

        $metadata = [];
        $idpSSO = [];
        foreach ($xml->xpath('//md:SingleSignOnService') as $index => $item) {
            $idpSSO[$index]['location'] = $item->attributes()->Location->__toString();
            $idpSSO[$index]['binding'] = $item->attributes()->Binding->__toString();
        }

        $idpSLO = [];
        foreach ($xml->xpath('//md:SingleLogoutService') as $index => $item) {
            $idpSLO[$index]['location'] = $item->attributes()->Location->__toString();
            $idpSLO[$index]['binding'] = $item->attributes()->Binding->__toString();
        }

        $metadata['idpEntityId'] = $xml->attributes()->entityID->__toString();
        $metadata['idpSSO'] = $idpSSO;
        $metadata['idpSLO'] = $idpSLO;
        $metadata['idpCertValue'] = self::formatCert(
            $xml->xpath('//md:IDPSSODescriptor//ds:X509Certificate')[0]->__toString()
        );

        $metadata['idpCertValues'] = [];
        $certificates = $xml->xpath('//md:IDPSSODescriptor//ds:X509Certificate');
        foreach ($certificates as $certificate){
            $metadata['idpCertValues'][] = self::formatCert((string)$certificate);
        }

        $this->idpFileName = $xmlFile;
        $this->metadata = $metadata;
        return $this;
    }

    private static function formatCert($cert)
    {
        //$cert = str_replace(" ", "\n", $cert);
        $x509cert = str_replace(["\x0D", "\r", "\n"], "", trim($cert));
        if (!empty($x509cert)) {
            $x509cert = str_replace('-----BEGIN CERTIFICATE-----', "", $x509cert);
            $x509cert = str_replace('-----END CERTIFICATE-----', "", $x509cert);
            $x509cert = str_replace(' ', '', $x509cert);

            $x509cert = "-----BEGIN CERTIFICATE-----\n" .
                chunk_split($x509cert, 64, "\n") .
                "-----END CERTIFICATE-----\n";
        }
        return $x509cert;
    }

    public function authnRequest($ass, $attr, $binding, $level = 1, $redirectTo = null, $shouldRedirect = true): string
    {
        $this->assertID = $ass;
        $this->attrID = $attr;
        $this->level = $level;

        $authn = new AuthnRequest($this);
        $url = $binding == Settings::BINDING_REDIRECT ?
            $authn->redirectUrl($redirectTo) :
            $authn->httpPost($redirectTo);

        $_SESSION['RequestID'] = $authn->id;
        $_SESSION['idpName'] = $this->idpFileName;
        $_SESSION['idpEntityId'] = $this->metadata['idpEntityId'];
        $_SESSION['acsUrl'] = $this->sp->settings['sp_assertionconsumerservice'][$ass];

        if ($binding === Settings::BINDING_POST) {
          echo $url;
          die();
        }

        if (!$shouldRedirect) {
            return $url;
        }

        header('Pragma: no-cache');
        header('Cache-Control: no-cache, must-revalidate');
        header('Location: ' . $url);
        exit("");
    }

    public function logoutRequest(Session $session, $slo, $binding, $redirectTo = null, $shouldRedirect = true): string
    {
        $this->session = $session;

        if ($this->isFederaGateway() && empty($this->metadata['idpSLO'])){
            $scheme = parse_url($this->session->idpEntityID, PHP_URL_SCHEME);
            $host = parse_url($this->session->idpEntityID, PHP_URL_HOST);
            $spid = $this->sp->settings['sp_entityid'];
            $spidScheme = parse_url($spid, PHP_URL_SCHEME);
            $spidHost = parse_url($spid, PHP_URL_HOST);
            $spurl = $this->sp->settings['sp_singlelogoutservice'][0][0] ?? "$spidScheme://$spidHost/slo";
            $url = "$scheme://$host/logout/?spid=$spid&spurl=$spurl";
        }else {
            $logoutRequest = new LogoutRequest($this);
            $url = ($binding == Settings::BINDING_REDIRECT) ?
                $logoutRequest->redirectUrl($redirectTo) :
                $logoutRequest->httpPost($redirectTo);

            $_SESSION['RequestID'] = $logoutRequest->id;
            $_SESSION['idpName'] = $this->idpFileName;
            $_SESSION['idpEntityId'] = $this->metadata['idpEntityId'];
            $_SESSION['sloUrl'] = reset($this->sp->settings['sp_singlelogoutservice'][$slo]);

            if (!$shouldRedirect || $binding == Settings::BINDING_POST) {
                return $url;
                exit;
            }
        }
        header('Pragma: no-cache');
        header('Cache-Control: no-cache, must-revalidate');
        header('Location: ' . $url);
        exit("");
    }

    public function logoutResponse(): string
    {
        $binding = Settings::BINDING_POST;
        $redirectTo = $this->sp->settings['sp_entityid'];

        $logoutResponse = new LogoutResponse($this);
        $url = ($binding == Settings::BINDING_REDIRECT) ?
            $logoutResponse->redirectUrl($redirectTo) :
            $logoutResponse->httpPost($redirectTo);
        unset($_SESSION);

        if ($binding == Settings::BINDING_POST) {
            return $url;
            exit;
        }

        header('Pragma: no-cache');
        header('Cache-Control: no-cache, must-revalidate');
        header('Location: ' . $url);
        exit("");
    }

    public function isFederaGateway(): bool
    {
        return strpos($this->idpFileName, 'federa-') !== false;
    }

    public function getSignAlg(): string
    {
        if ($this->isFederaGateway()){
            return XMLSecurityKey::RSA_SHA1;
        }
        return XMLSecurityKey::RSA_SHA256;
    }

    public function hasRedirect(): bool
    {
      $sso = $this->metadata['idpSSO'];
      foreach ($sso as $item) {
        if (isset($item['binding']) && $item['binding'] === Settings::BINDING_REDIRECT) {
          return true;
        }
      }
      return false;
    }
}
