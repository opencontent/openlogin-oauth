<?php

namespace App\Controller\Admin;

use App\Entity\FederationAuthority;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class FederationAuthorityCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return FederationAuthority::class;
    }

}
