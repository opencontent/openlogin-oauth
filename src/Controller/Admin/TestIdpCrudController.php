<?php

namespace App\Controller\Admin;

use App\Entity\TestIdp;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class TestIdpCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return TestIdp::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setEntityLabelInSingular('Additional IDP');
        $crud->setPageTitle(Crud::PAGE_INDEX, 'Additional IDentity Providers');
        $crud->setHelp(Crud::PAGE_INDEX, 'Available identifier prefixes: <proxy-> to skip idp select, <federa-> to use Federa settings');

        return parent::configureCrud($crud);
    }

}
