<?php

namespace App\Controller\Admin;

use App\Entity\Admin;
use App\Entity\FederationAuthority;
use App\Entity\RelyingPartyClient;
use App\Entity\SpidSession;
use App\Entity\SpidUser;
use App\Entity\Tenant;
use App\Entity\TestIdp;
use App\Form\Type\ConfigType;
use App\Form\Type\OAuth2ClientType;
use App\Spid\Saml\SignatureUtils;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use Exception;
use League\Bundle\OAuth2ServerBundle\Manager\ClientFilter;
use League\Bundle\OAuth2ServerBundle\Manager\ClientManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use League\Bundle\OAuth2ServerBundle\Model\AbstractClient;
use League\Bundle\OAuth2ServerBundle\Model\Client;
use League\Bundle\OAuth2ServerBundle\ValueObject\Grant;
use League\Bundle\OAuth2ServerBundle\ValueObject\RedirectUri;
use League\Bundle\OAuth2ServerBundle\ValueObject\Scope;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(AdminUrlGenerator::class);
        return $this->redirect($routeBuilder->setController(TenantCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('OpenLogin');
    }

    public function configureAssets(): Assets
    {
        $assets = Assets::new();
        $assets->addHtmlContentToHead(
            '<style>' .
            '.form-widget input.form-control, .form-widget select.form-control, .form-widget textarea.form-control{max-width: none;!important}' .
            '.form-widget-compound {width: 100%;}' .
            '</style>'
        );
        return $assets;
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::section('Spid');
        yield MenuItem::linkToCrud('Tenants', 'fas fa-link', Tenant::class);
        yield MenuItem::linkToCrud('Additional IDPs', 'fas fa-check', TestIdp::class);
        yield MenuItem::linktoRoute('Oauth2 Clients', 'fas fa-laptop-code', 'admin_list_clients');
        yield MenuItem::linktoRoute('Config builder', 'fas fa-code', 'admin_config_builder');
        yield MenuItem::section('Cie');
        yield MenuItem::linkToCrud('Federation authorities', 'fas fa-university', FederationAuthority::class);
        yield MenuItem::linkToCrud('RelyIng Party Clients', 'fas fa-id-card', RelyingPartyClient::class);
        yield MenuItem::section('Users');
        yield MenuItem::linkToCrud('Admins', 'fas fa-user-secret', Admin::class);
        yield MenuItem::linkToCrud('Users', 'fas fa-users', SpidUser::class);
        yield MenuItem::linkToCrud('Sessions', 'fas fa-clock', SpidSession::class);

    }

    /**
     * @Route("/admin/clients", name="admin_list_clients")
     *
     * @param Environment $twig
     * @param ClientManagerInterface $clientManager
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function listClients(Environment $twig, ClientManagerInterface $clientManager): Response
    {
        $clients = $clientManager->list(ClientFilter::create());

        return new Response(
            $twig->render('admin/list_clients.html.twig', [
                'clients' => $clients,
            ])
        );
    }

    /**
     * @Route("/admin/clients/edit/{identifier}", defaults={"identifier" = null}, name="admin_edit_client")
     *
     * @param Request $request
     * @param Environment $twig
     * @param ClientManagerInterface $clientManager
     * @param null $identifier
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function editClient(
        Request $request,
        Environment $twig,
        ClientManagerInterface $clientManager,
        $identifier = null
    ): Response {
        $client = $identifier ? $clientManager->find($identifier) : null;
        $editData = $client ? [
            'name' => $client->getName(),
            'identifier' => $client->getIdentifier(),
            'redirect_uri' => $client->getRedirectUris()[0],
        ] : null;
        $form = $this->createForm(OAuth2ClientType::class, $editData, ['is_update' => !!$client]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $secret = $data['secret'] ?? hash('sha512', random_bytes(32));
            $identifier = $data['identifier'] ?? hash('md5', random_bytes(16));
            $name = $data['name'] ?? $identifier;

            $redirectUriStrings = [$data['redirect_uri']];
            $grantStrings = [];
            $scopeStrings = ['profile', 'email'];

            if (!$client) {
                $client = (new Client($name, $identifier, $secret));
            } else {
                $client->setName($name);
            }

            $client->setActive(true)
                ->setRedirectUris(
                    ...array_map(static function (string $redirectUri): RedirectUri {
                    return new RedirectUri($redirectUri);
                }, $redirectUriStrings)
                )
                ->setGrants(
                    ...array_map(static function (string $grant): Grant {
                    return new Grant($grant);
                }, $grantStrings)
                )
                ->setScopes(
                    ...array_map(static function (string $scope): Scope {
                    return new Scope($scope);
                }, $scopeStrings)
                )//->setAllowPlainTextPkce(true)
            ;

            try {
                $clientManager->save($client);
                $this->addFlash('info', 'OAuth2 client stored successfully.');
            } catch (Exception $e) {
                $this->addFlash('error', $e->getMessage());
            }

            return $this->redirectToRoute('admin_list_clients');
        }

        return new Response(
            $twig->render('admin/edit_client.html.twig', [
                'client_form' => $form->createView(),
            ])
        );
    }

    /**
     * @Route("/admin/clients/delete/{identifier}", name="admin_delete_client")
     *
     * @param $identifier
     * @param Request $request
     * @param Environment $twig
     * @param ClientManagerInterface $clientManager
     * @return Response
     */
    public function deleteClient(
        $identifier,
        Request $request,
        Environment $twig,
        ClientManagerInterface $clientManager
    ): Response {
        $client = $clientManager->find($identifier);
        if ($client) {
            try {
                $clientManager->remove($client);
                $this->addFlash('info', "OAuth2 client $identifier deleted successfully.");
            } catch (Exception $e) {
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->redirectToRoute('admin_list_clients');
    }

    /**
     * @Route("/admin/config-builder", name="admin_config_builder")
     *
     * @param Request $request
     * @param Environment $twig
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function configBuilder(Request $request, Environment $twig): Response
    {
        $data = [];
        $form = $this->createForm(ConfigType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $client = $data['oauth2_client'];
            if ($client instanceof AbstractClient) {
                $redirectUrl = parse_url((string)$client->getRedirectUris()[0]);

                $basePath = false;
                if (isset($redirectUrl['path'])) {
                    $basePath = str_replace('/_oauth', '', $redirectUrl['path']);
                }
                if (empty($data['application_host'])) {
                    $data['application_host'] = $redirectUrl['host'];
                }
                if (empty($data['application_path_prefix']) && $basePath) {
                    $data['application_path_prefix'] = $basePath;
                }
                if (empty($data['application_logout_redirect'])) {
                    $redirectUrl = $redirectUrl['scheme'] . '://' . $redirectUrl['host'];
                    if (strpos($basePath, 'login-open') !== false) {
                        $redirectUrl .= str_replace('/auth/login-open', '', $basePath);
                    }
                    $data['application_logout_redirect'] = $redirectUrl;
                }
            }
            $bytes = openssl_random_pseudo_bytes(16);
            $data['secret'] = bin2hex($bytes);
            $data['has_instances_yml'] = strpos($data['application_path_prefix'], 'login-open') !== false;
        }
        return new Response(
            $twig->render('admin/config_builder.html.twig', [
                'config_form' => $form->createView(),
                'data' => $data,
            ])
        );
    }

    /**
     ** @Route("/admin/certificate", name="admin_generate_certificate", methods={"POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function generateCertificate(Request $request): Response
    {
        try {
            $fallbackValue = 'open-login';
            $payload = json_decode((string)$request->getContent(), true);
            $settings = [
                'dn' => [
                    'organizationName' => $payload['organizationName'] ?? $fallbackValue,
                    'organizationIdentifier' => $payload['organizationIdentifier'] ?? 'PA:IT-' . $fallbackValue,
                    'countryName' => $payload['countryName'] ?? 'IT',
                    'localityName' => $payload['localityName'] ?? $fallbackValue,
                    'commonName' => $payload['commonName'] ?? $fallbackValue,
                    'emailAddress' => $payload['emailAddress'] ?? $fallbackValue . '@example.com',
                    'uri' => $payload['uri'] ?? $fallbackValue,
                ],
            ];
            $baseConfigPath = $this->get('parameter_bag')->get('kernel.project_dir') . '/config/openssl.cnf';
            $configData = file_get_contents($baseConfigPath);
            foreach ($settings['dn'] as $key => $value) {
                $configData = str_replace('{' . $key . '}', $value, $configData);
            }
            $configPath = $baseConfigPath . '.' . time();
            file_put_contents($configPath, $configData);
            $settings['options'] = [
                'config' => $configPath,
                'req_extensions' => 'req_ext',
                'digest_alg' => 'sha256',
            ];
            $responseData = SignatureUtils::generateKeyCert($settings);
            @unlink($configPath);
            return new JsonResponse($responseData);
        } catch (Throwable $e) {
            return new JsonResponse([
                'error' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => explode(PHP_EOL, $e->getTraceAsString()),
            ], 500);
        }
    }

    /**
     ** @Route("/admin/certificate/{tenant}", name="admin_generate_tenant_certificate", methods={"GET"})
     *
     * @param Request $request
     * @param Tenant $tenant
     * @return Response
     */
    public function generateTenantCertificate(Request $request, Tenant $tenant): Response
    {

        try {
            $fallbackValue = 'open-login';
            $settings = [
                'dn' => [
                    'organizationName' => $tenant->getOrganizationName() ?? $fallbackValue,
                    'organizationIdentifier' => 'PA:IT-' . $tenant->getIpaCode(),
                    'countryName' => $tenant->getCountry() ?? 'IT',
                    'localityName' => $tenant->getLocality() ?? $fallbackValue,
                    'commonName' => $tenant->getOrganizationName() ?? $fallbackValue,
                    'emailAddress' => $tenant->getEmail() ?? $fallbackValue . '@example.com',
                    'uri' => $tenant->getEntityId(),
                ],
            ];
            $baseConfigPath = $this->get('parameter_bag')->get('kernel.project_dir') . '/config/openssl.cnf';
            $configData = file_get_contents($baseConfigPath);
            foreach ($settings['dn'] as $key => $value) {
                $configData = str_replace('{' . $key . '}', $value, $configData);
            }
            $configPath = $baseConfigPath . '.' . time();
            file_put_contents($configPath, $configData);
            $settings['options'] = [
                'config' => $configPath,
                'req_extensions' => 'req_ext',
                'digest_alg' => 'sha256',
            ];
            $responseData = SignatureUtils::generateKeyCert($settings);
            @unlink($configPath);
            return new JsonResponse($responseData);
        } catch (Throwable $e) {
            return new JsonResponse([
                'error' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => explode(PHP_EOL, $e->getTraceAsString()),
            ], 500);
        }
    }
}
