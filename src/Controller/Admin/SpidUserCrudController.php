<?php

namespace App\Controller\Admin;

use App\Entity\SpidUser;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;

class SpidUserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SpidUser::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $fields = parent::configureFields($pageName);
        if ($pageName === Crud::PAGE_INDEX) {
            $fields[] = Field::new('sessionsCount')->setLabel('Sessions');
        }else{
            $fields[] = Field::new('sessionsAsString')->setLabel('Sessions');
        }

        return $fields;
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->add(Crud::PAGE_INDEX, Crud::PAGE_DETAIL);
        $actions->disable(Action::EDIT, Action::NEW);
        return $actions;
    }


}
