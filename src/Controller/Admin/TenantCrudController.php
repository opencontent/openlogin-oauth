<?php

namespace App\Controller\Admin;

use App\Entity\Tenant;
use App\Form\Type\AssertionConsumerServiceFormType;
use App\Form\Type\AttributeConsumingServiceFormType;
use App\Form\Type\CertificateFormType;
use App\Form\Type\SingleLogoutServiceFormType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class TenantCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Tenant::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPaginatorPageSize(50);
        $crud->setDefaultSort(['organizationName' => 'asc']);
        return $crud;
    }

    public function configureAssets(Assets $assets): Assets
    {
        $assets
            ->addHtmlContentToHead(
                '<style>' .
                '.form-widget input.form-control, .form-widget select.form-control, .form-widget textarea.form-control{max-width: none;!important}' .
                '.form-widget-compound {width: 100%;}' .
                '.create-certificate-link {position: absolute;right: -175px;top: 4px;}' .
                '.generate-acs-link {position: absolute;right: -230px;top: 4px;}' .
                '.generate-slo-link {position: absolute;right: -230px;top: 4px;}' .
                '.toggle-acs {position: absolute;right: -129px;top: 4px;}' .
                '.main-content {margin-bottom: 100px}' .
                '</style>'
            )
            ->addJsFile('https://code.jquery.com/jquery-3.4.1.min.js')
            ->addJsFile('./app/js/tenant_crud_tools.js')
        ;
        $assets = parent::configureAssets($assets);
        return $assets;
    }


    public function configureFields(string $pageName): iterable
    {
        if ($pageName === Crud::PAGE_INDEX) {
            return [
                Field::new('organizationName'),

                TextField::new('hasCustomMetadataAsHtml')
                    ->setLabel('Custom?')
                    ->setCustomOption(TextField::OPTION_RENDER_AS_HTML, true),

                Field::new('isMerge')
                    ->setLabel('Merge?')
                    ->setCustomOption(TextField::OPTION_RENDER_AS_HTML, true),

                TextField::new('hasTestIdpAsHtml')
                    ->setLabel('Additional IDP')
                    ->setCustomOption(TextField::OPTION_MAX_LENGTH, 500)
                    ->setCustomOption(TextField::OPTION_RENDER_AS_HTML, true),

                TextField::new('openLoginUri')
                    ->setCustomOption(TextField::OPTION_MAX_LENGTH, 500),

                TextField::new('metadataHtmlLink')
                    ->setLabel('Entity Id, ACS, SLO')
                    ->setCustomOption(TextField::OPTION_MAX_LENGTH, 500)
                    ->setCustomOption(TextField::OPTION_RENDER_AS_HTML, true),
//                Field::new('spidLevel'),
            ];
        }

        $fields = parent::configureFields($pageName);
        $fields[] = CollectionField::new('certificates')
            ->allowAdd()
            ->allowDelete()
            ->setEntryType(CertificateFormType::class);
        $fields[] = CollectionField::new('assertionConsumerServices')
            ->allowAdd()
            ->allowDelete()
            ->renderExpanded()
            ->setEntryType(AssertionConsumerServiceFormType::class);
        $fields[] = CollectionField::new('attributeConsumingServices')
            ->allowAdd()
            ->allowDelete()
            ->setEntryType(AttributeConsumingServiceFormType::class);
        $fields[] = CollectionField::new('singleLogoutServices')
            ->allowAdd()
            ->allowDelete()
            ->renderExpanded()
            ->setEntryType(SingleLogoutServiceFormType::class);
        $fields[] = AssociationField::new('idpTest', 'Additional IDP');

        return $fields;
    }
}
