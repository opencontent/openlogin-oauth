<?php

namespace App\Controller\Admin;

use App\Entity\RelyingPartyClient;
use App\Form\Type\CertificateFormType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;

class RelyingPartyClientCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return RelyingPartyClient::class;
    }

    public function configureAssets(Assets $assets): Assets
    {
        $assets
            ->addHtmlContentToHead(
                '<style>' .
                '.form-widget input.form-control, .form-widget select.form-control, .form-widget textarea.form-control{max-width: none;!important}' .
                '.form-widget-compound {width: 100%;}' .
                '.create-certificate-link {position: absolute;right: -175px;top: 4px;}' .
                '.main-content {margin-bottom: 100px}' .
                '</style>'
            )
            ->addJsFile('https://code.jquery.com/jquery-3.4.1.min.js')
            ->addJsFile('./app/js/relying_party_client_crud_tools.js')
        ;
        $assets = parent::configureAssets($assets);
        return $assets;
    }


    public function configureFields(string $pageName): iterable
    {
        $fields = parent::configureFields($pageName);
        $fields[] = AssociationField::new('tenant', 'Ente');
        $fields[] = AssociationField::new('federationAuthority', 'Federation Authority');

        $fields[] = CollectionField::new('federationCertificates')
            ->allowAdd()
            ->allowDelete()
            ->setEntryType(CertificateFormType::class);

        $fields[] = CollectionField::new('coreCertificates')
            ->allowAdd()
            ->allowDelete()
            ->setEntryType(CertificateFormType::class);

        return $fields;
    }
}
