<?php

namespace App\Controller\Admin;

use App\Entity\SpidSession;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;

class SpidSessionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SpidSession::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->disable(Action::EDIT, Action::NEW);
        return $actions;
    }

    public function configureFields(string $pageName): iterable
    {
        $fields = parent::configureFields($pageName);
        $fields[] = Field::new('spidUsername');
        $fields[] = Field::new('attributesAsString')->setLabel('Attributes');

        return $fields;
    }
}
