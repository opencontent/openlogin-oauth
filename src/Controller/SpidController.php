<?php

namespace App\Controller;

use App\Cie\Response\ResponseHandlerPlain;
use App\Entity\Tenant;
use App\Spid\SpidClientFactory;
use App\Spid\SpidException;
use App\Spid\SpidResponseException;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\TokenStorage\ClearableTokenStorageInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class SpidController extends AbstractController
{
    private TokenStorageInterface $csrfTokenStorage;
    private SessionInterface $session;

    public function __construct(TokenStorageInterface $csrfTokenStorage, SessionInterface $session)
    {
        $this->csrfTokenStorage = $csrfTokenStorage;
        $this->session = $session;
    }

    /*public function login(Request $request, Environment $twig, SpidClientFactory $spidClientFactory): Response
    {
        try {
            $tenant = $spidClientFactory->getTenantFromRequest();
            if ($tenant->isProxy()){
                $request->query->set('idp', $tenant->getProxyIdentifier());
            }

            if ($request->query->has('idp')) {
                $idp = $request->query->get('idp');
                $client = $spidClientFactory->makeClient();
                $index = (int)$tenant->getLoginIndex();
                $spidLevel = $tenant->getSpidLevel() ?? 2;

                $redirectTo = "https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
                $encryptedRedirectTo = SpidClientFactory::encrypt($redirectTo);
                if (!@$client->login($idp, $index, $index, $spidLevel, $encryptedRedirectTo)) {
                    return $this->redirect('/');
                }
            }

            return new Response($twig->render('spid/login.html.twig', [
                'page_title' => $tenant->getOrganizationName(),
                'idp_list' => SpidClientFactory::getIdpInfoList(),
                'idp_test' => $tenant->getIdpTest()
            ]));
        } catch (SpidException $e) {
            return new Response($twig->render('error.html.twig', ['error' => $e->getMessage()]), 500);
        }
    }*/

    /**
     * @Route("/login", name="spid_login")
     *
     * @param Request $request
     * @param string $domain
     * @param SpidClientFactory $spidClientFactory
     * @return Response
     */
    public function login(Request $request, $domain = 'default', SpidClientFactory $spidClientFactory): Response
    {
        try {
            $tenant = $spidClientFactory->getTenantFromRequest();
            if (!$tenant instanceof Tenant) {
                return new Response($this->renderView('error.html.twig', ['error' => 'Tenant not found']), Response::HTTP_NOT_FOUND);
            }

            if ($tenant->isProxy()){
                $request->query->set('idp', $tenant->getProxyIdentifier());
            }

            if ($request->query->has('idp')) {
                $idp = $request->query->get('idp');
                $client = $spidClientFactory->makeClient();
                $index = (int)$tenant->getLoginIndex();
                $spidLevel = $tenant->getSpidLevel() ?? 2;

                $redirectTo = "https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
                $encryptedRedirectTo = SpidClientFactory::encrypt($redirectTo);
                if (!@$client->login($idp, $index, $index, $spidLevel, $encryptedRedirectTo)) {
                    return $this->redirect('/');
                }
            }

            // stash params state from proxy requests
            // (OIDC generic 2 OIDC SPID)
            $state = $request->query->get('state');
            $this->session->set('state', $state);

            // Todo: viene gestito in OidcCieAuthenticator
            /*$auth = $this->session->get('auth');
            if (
                $auth !== null
                && $auth['userinfo'] !== null
                && $auth['redirect_uri'] !== null
                && $auth['state'] !== null
            ) {
                $userinfoResponse = $auth['userinfo'];
                $redirect_uri = $auth['redirect_uri'];
                $state = $auth['state'];
                $responseHandler = new ResponseHandlerPlain([]);
                $responseHandler->sendResponse($redirect_uri, $userinfoResponse, $state);
                die();
            }*/

            return $this->render('combined/login.html.twig', [
                'tenant' => $tenant,
                'page_title' => $tenant->getOrganizationName(),
                'idp_list' => SpidClientFactory::getIdpInfoList(),
                'idp_test' => $tenant->getIdpTest(),
                'base_url' => '',
                'domain' => $domain,
                'trust_anchor' => $tenant->getRelyingPartyClient() && $tenant->getRelyingPartyClient()->getFederationAuthority() ? base64_encode($tenant->getRelyingPartyClient()->getFederationAuthority()->getUri()) : '',
                //'cie_provider' => base64_encode('http://cie-provider.org:8002/oidc/op/'), // Qui andrà messo il provider del ministero
                //'trust_anchor' => base64_encode('https://preprod.oidc.registry.servizicie.interno.gov.it'),
                //'cie_provider' => base64_encode('https://preproduzione.oidc.idserver.servizicie.interno.gov.it'),
                'cie_provider' => $tenant->getRelyingPartyClient() && $tenant->getRelyingPartyClient()->getFederationAuthority() ? base64_encode($tenant->getRelyingPartyClient()->getFederationAuthority()->getOpenIdProvider()) : '',
            ]);
        } catch (SpidException $e) {
            return new Response($this->renderView('error.html.twig', ['error' => $e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Route("/acs", name="spid_acs", methods={"POST"})
     *
     * @param Environment $twig
     * @param SpidClientFactory $spidClientFactory
     * @param RequestStack $requestStack
     * @param LoggerInterface $logger
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function acs(Environment $twig, SpidClientFactory $spidClientFactory, RequestStack $requestStack, LoggerInterface $logger): Response
    {
        $session = $requestStack->getSession();
        try {
            $client = $spidClientFactory->makeClient();
        } catch (SpidException $e) {
            if (isset($_SESSION['spidSession'])) {
                unset($_SESSION['spidSession']);
            }
            return new Response($twig->render('error.html.twig', ['error' => $e->getMessage()]), 500);
        }

        $error = SpidResponseException::DEFAULT_MESSAGE;
        try {
            if ($client->isAuthenticated()) {
                if ($targetPath = $session->get('_security.main.target_path')) {
                    return new RedirectResponse($targetPath);
                }
                return $this->redirectToRoute('homepage');
            }
        } catch (SpidResponseException $e) {
            $error = $e->getMessage();
            $logger->error($e->getOriginalMessage(), [
                'user_message' => $e->getMessage(),
                'error_trace' => $e->getTraceAsString()
            ]);
        } catch (Exception $e) {
            if (!empty($e->getMessage())) {
                $error = $e->getMessage();
            }
            $logger->error($e->getMessage(), ['error_trace' => $e->getTraceAsString()]);
        }
        $supportMailAddress = $spidClientFactory->getTenantFromRequest()->getEmail();

        if (isset($_SESSION['spidSession'])) {
            unset($_SESSION['spidSession']);
        }

        return new Response($twig->render('error.html.twig', ['error' => $error, 'mail_support' => $supportMailAddress]), 401);
    }

    /**
     * @Route("/logout", name="spid_logout")
     *
     * @param Request $request
     * @param LoggerInterface $logger
     * @param RequestStack $requestStack
     * @param SpidClientFactory $spidClientFactory
     * @return Response
     */
    public function logout(
        Request $request,
        LoggerInterface $logger,
        RequestStack $requestStack,
        SpidClientFactory $spidClientFactory
    ): Response
    {
        $session = $requestStack->getSession();
        $redirectTo = null;
        if ($request->query->has('redirect')) {
            $redirectTo = $request->query->get('redirect');
            $logger->debug("Found after_logout_redirect param: $redirectTo");
            $session->set('after_logout_redirect', $redirectTo);
        }
        try {
            $client = $spidClientFactory->makeClient();
            $tenant = $spidClientFactory->getTenantFromRequest();

            $ignoreLogoutIfIsMerged = (bool)getenv('ENABLE_LOGOUT_WORKAROUND');
            if ($tenant->isMerge() && $ignoreLogoutIfIsMerged){
                return new RedirectResponse($this->generateUrl('spid_slo_logout'));
            }

            $logoutService = false;
            foreach ($tenant->getSingleLogoutServices() as $singleLogoutService) {
                if ($singleLogoutService->getIndex() == $tenant->getLoginIndex()) {
                    $logoutService = $singleLogoutService;
                    break;
                }
            }
            $encryptedRedirectTo = SpidClientFactory::encrypt($redirectTo);

            if (!$logoutService || $logoutService->getIsRedirect()) {
                $client->logout($tenant->getLoginIndex(), $encryptedRedirectTo);
            } else {
                $logoutData = $client->logoutPost($tenant->getLoginIndex(), $encryptedRedirectTo);
                if (strpos($logoutData, '<html') !== false){
                    return new Response($logoutData);
                }
            }
        } catch (SpidException $e) {
            $logger->error($e->getMessage());
        }

        $response = new RedirectResponse($this->generateUrl('homepage'));
        return $this->logoutResponse($response);
    }

    private function logoutResponse(Response $response): Response
    {
        $this->get('session')->invalidate();
        $this->get('security.token_storage')->setToken(null);
        if ($this->csrfTokenStorage instanceof ClearableTokenStorageInterface) {
            $this->csrfTokenStorage->clear();
        }

        return $response;
    }

    /**
     * @Route("/slo", name="spid_slo_logout")
     *
     * @param Environment $twig
     * @param SpidClientFactory $spidClientFactory
     * @param RequestStack $requestStack
     * @param LoggerInterface $logger
     * @return RedirectResponse
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function singleLogout(Environment $twig, SpidClientFactory $spidClientFactory, RequestStack $requestStack, LoggerInterface $logger): Response
    {
        $session = $requestStack->getSession();
        if ($session->has('after_logout_redirect')) {
            $redirectTo = $session->get('after_logout_redirect');
            $session->remove('after_logout_redirect');
            $logger->debug("Redirect to $redirectTo");
        }else{
            $redirectTo = $this->generateUrl('homepage');
        }

        try {
            $client = $spidClientFactory->makeClient();
            if (!$client->isAuthenticated()) {
                if (isset($_SESSION['spidSession'])) {
                    unset($_SESSION['spidSession']);
                }
            }
        } catch (SpidException $e) {
            if (isset($_SESSION['spidSession'])) {
                unset($_SESSION['spidSession']);
            }
            return $this->logoutResponse(
                new Response($twig->render('error.html.twig', ['error' => $e->getMessage()]), 500)
            );
        }

        $response = new RedirectResponse($redirectTo);
        return $this->logoutResponse($response);
    }
}