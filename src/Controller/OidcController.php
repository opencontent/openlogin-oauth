<?php

namespace App\Controller;

use App\Cie\Core\JWT;
use App\Cie\Response\ResponseHandlerPlain;
use App\Entity\FederationAuthority;
use App\Entity\SpidSession;
use App\Entity\SpidUser;
use App\Entity\Tenant;
use App\Repository\FederationAuthorityRepository;
use App\Spid\SpidClientFactory;
use Exception;
use Psr\Log\LoggerInterface;
use App\Cie\Core\Util;
use App\Cie\Federation\EntityStatement;
use App\Cie\Federation\TrustChain;
use App\Cie\OIDC\RP\AuthenticationRequest;
use App\Cie\OIDC\RP\IntrospectionRequest;
use App\Cie\OIDC\RP\RevocationRequest;
use App\Cie\OIDC\RP\TokenRequest;
use App\Cie\OIDC\RP\UserinfoRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Cie\OIDC\RP\Database as RP_Database;
use App\Cie\OIDC\OP\Database as OP_Database;
use App\Cie\OIDC\OP\Metadata as OP_Metadata;

class OidcController extends AbstractController
{

    private $config;

    private array $federationAuthorityUris = [];

    private array $hooks = [
        "pre_authorization_request" => [],
        "pre_token_request" => [],
        "post_token_request" => [],
        "pre_userinfo_request" => [],
        "post_userinfo_request" => []
    ];

    private string $serviceName = '';

    private SessionInterface $session;

    private SpidClientFactory $spidClientFactory;

    private LoggerInterface $logger;
    private string $baseUrl;
    private RP_Database $rpDatabase;
    private OP_Database $opDatabase;


    public function __construct(SessionInterface $session, SpidClientFactory $spidClientFactory, LoggerInterface $logger, FederationAuthorityRepository $federationAuthorityRepository)
    {
        // Database di appoggio utilizzato dal rp
        $this->rpDatabase = new RP_Database(__DIR__ . '/../../data/store-rp.sqlite');

        // Database di appoggio utilizzato dal op
        $this->opDatabase = new OP_Database(__DIR__ . '/../../data/store-op.sqlite');


        foreach ($federationAuthorityRepository->findAll() as $federation) {
            $this->federationAuthorityUris []= $federation->getUri();
        }

        $this->baseUrl = ($this->serviceName === '') ? '' : '/' . $this->serviceName;
        $this->session = $session;
        $this->logger = $logger;
        $this->spidClientFactory = $spidClientFactory;
    }

    /**
     * @Route("/.well-known/openid-federation", name="openid_federation", methods={"GET"})
     * @Route("/{domain}/.well-known/openid-federation", name="openid_domain_federation", methods={"GET"})
     */
    public function openidFederation(Request $request, $domain = 'default'): Response
    {
        try {
            $tenant = $this->checkDomain();
        } catch (\Exception $e) {
            return new Response($this->renderView('error.html.twig', ['error' => 'Domain not found']), Response::HTTP_NOT_FOUND);
        }
        //$config = $this->config['rp_proxy_clients'][$domain];
        $config = $tenant->getRelyingPartyClient()->generateConfig();

        $output = $request->query->get('output');
        $json = strtolower($output) === 'json';
        $mediaType = $json ? 'application/json' : 'application/entity-statement+jwt';
        $jwks = EntityStatement::makeFromConfig($config, $json);
        return new Response($jwks, Response::HTTP_OK, ['Content-Type' => $mediaType]);
    }

    /**
     * @Route("/oidc/resolve", name="openid_resolve", methods={"GET"})
     */
    public function resolve(Request $request): Response
    {
        try {
            $tenant = $this->checkDomain();
        } catch (\Exception $e) {
            return new Response($this->renderView('error.html.twig', ['error' => 'Domain not found']), Response::HTTP_NOT_FOUND);
        }
        //$config = $this->config['rp_proxy_clients'][$domain];
        $config = $tenant->getRelyingPartyClient()->generateConfig();
        $key = $config['cert_private'];
        $key_jwk = JWT::getJWKFromJSON(file_get_contents($key));

        $header = array(
            "typ" => "entity-statement+jwt",
            "alg" => "RS256",
            "kid" => $key_jwk->get('kid')
        );
        $jws = JWT::makeJWS($header, array("test"), $key_jwk);
        return new Response($jws, Response::HTTP_OK, ['Content-Type' => 'application/entity-statement+jwt']);

    }

    /**
     * @Route("/oidc/rp/jwks.json", name="openid_jwks_uri", methods={"GET"})
     */
    public function jwksUri(Request $request): Response
    {
        try {
            $tenant = $this->checkDomain();
        } catch (\Exception $e) {
            return new Response($this->renderView('error.html.twig', ['error' => 'Domain not found']), Response::HTTP_NOT_FOUND);
        }
        //$config = $this->config['rp_proxy_clients'][$domain];
        $config = $tenant->getRelyingPartyClient()->generateConfig();
        $crt = $config['cert_public'];
        $crt_jwk = JWT::getCertificateJWK($crt);
        $result = [
            'keys' => [$crt_jwk]
        ];
        return new Response(json_encode($result), Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/oidc/rp/jwks.jose", name="openid_signed_jwks_uri", methods={"GET"})
     */
    public function signedJwksUri(Request $request): Response
    {
        try {
            $tenant = $this->checkDomain();
        } catch (\Exception $e) {
            return new Response($this->renderView('error.html.twig', ['error' => 'Domain not found']), Response::HTTP_NOT_FOUND);
        }
        //$config = $this->config['rp_proxy_clients'][$domain];
        $config = $tenant->getRelyingPartyClient()->generateConfig();
        $crt = $config['cert_public'];
        $crt_jwk = JWT::getCertificateJWK($crt);
        $result = [
            'keys' => [$crt_jwk]
        ];
        $header = array(
            "typ" => "entity-statement+jwt",
            "alg" => "RS256",
            "kid" => $crt_jwk['kid']
        );

        $key = $config['cert_private'];
        $key_jwk = JWT::getKeyJWK($key);
        $jws = JWT::makeJWS($header, $result, $key_jwk);
        return new Response($jws, Response::HTTP_OK, ['Content-Type' => 'application/jose']);
    }

    /**
     * @Route("/oidc/rp/authz", name="openid_authz", methods={"GET"})
     * @Route("/oidc/rp/{domain}/authz", name="openid_domain_authz", methods={"GET"})
     */
    public function authz(Request $request, $domain = 'default')
    {
        try {
            $tenant = $this->checkDomain();
        } catch (\Exception $e) {
            return new Response($this->renderView('error.html.twig', ['error' => 'Domain not found']), Response::HTTP_NOT_FOUND);
        }
        //$config = $this->config['rp_proxy_clients'][$domain];
        $config = $tenant->getRelyingPartyClient()->generateConfig();

        // stash params state from proxy requests
        // (OIDC generic 2 OIDC SPID)
        $state = $request->query->get('state');
        $this->session->set('state', $state);

        $auth = $this->session->get('auth');
        if (
            $auth !== null
            && $auth['userinfo'] !== null
            && $auth['redirect_uri'] !== null
            && $auth['state'] !== null
        ) {
            $userinfoResponse = $auth['userinfo'];
            $redirect_uri = $auth['redirect_uri'];
            $state = $auth['state'];
            $responseHandler = new ResponseHandlerPlain($config);
            $responseHandler->sendResponse($redirect_uri, $userinfoResponse, $state);
            die();
        }

        return $this->render('oidc/login.html.twig', [
            'base_url' => $this->baseUrl,
            'domain' => $domain,
            'trust_anchor' => $tenant->getRelyingPartyClient() && $tenant->getRelyingPartyClient()->getFederationAuthority() ? base64_encode($tenant->getRelyingPartyClient()->getFederationAuthority()->getUri()) : '',
            'cie_provider' => $tenant->getRelyingPartyClient() && $tenant->getRelyingPartyClient()->getFederationAuthority() ? base64_encode($tenant->getRelyingPartyClient()->getFederationAuthority()->getOpenIdProvider()) : '',
        ]);
    }

    /**
     * @Route("/oidc/rp/authz/{ta}/{op}", name="openid_authz_ta_op", methods={"GET"})
     * @Route("/oidc/rp/{domain}/authz/{ta}/{op}", name="openid_domain_authz_ta_op", methods={"GET"})
     */
    public function authzTaOp(Request $request, $domain = 'default', $ta, $op)
    {
        try {
            $tenant = $this->checkDomain();
        } catch (\Exception $e) {
            return new Response($this->renderView('error.html.twig', ['error' => 'Domain not found']), Response::HTTP_NOT_FOUND);
        }
        //$config = $this->config['rp_proxy_clients'][$domain];
        $config = $tenant->getRelyingPartyClient()->generateConfig();

        $this->logger->info('OIDC', [
            'url' => '/oidc/rp/authz/' . $op,
            'method' => 'GET',
        ]);

        $ta_id = base64_decode($ta);
        $op_id = base64_decode($op);

        // try to get state first from session, if routed from proxy
        $state = $this->session->get('state');
        if ($state === null) {
            $state = $request->query->get('state');
        }
        if ($state === null) {
            $state = 'state';
        }

        $acr = $config['requested_acr'];
        $user_attributes = $config['spid_user_attributes'];
        $redirect_uri = $config['redirect_uri'];
        $req_id = $this->rpDatabase->createRequest($ta_id, $op_id, $redirect_uri, $state, $acr, $user_attributes);
        $request = $this->rpDatabase->getRequest($req_id);
        $code_verifier = $request['code_verifier'];
        $nonce = $request['nonce'];

        if (!in_array($ta_id, $this->federationAuthorityUris, true)) {
            return new Response("Federation non supported: " . $ta_id, Response::HTTP_UNAUTHORIZED);
        }

        // resolve entity statement on federation
        try {
            $trustchain = new TrustChain($config, $this->rpDatabase, $op_id, $ta_id);
            $configuration = $trustchain->resolve();
        } catch (Exception $e) {
            return new Response($e->getMessage(), Response::HTTP_UNAUTHORIZED);
        }

        $authorization_endpoint = $configuration->metadata->openid_provider->authorization_endpoint;
        $op_issuer = $configuration->metadata->openid_provider->issuer;

        $authenticationRequest = new AuthenticationRequest($config, $this->hooks);
        $authenticationRequestURL = $authenticationRequest->send(
            $op_issuer,
            $authorization_endpoint,
            $acr,
            $user_attributes,
            $code_verifier,
            $nonce,
            Util::base64UrlEncode(str_pad($req_id, 32))
        );

        return new RedirectResponse($authenticationRequestURL);
    }

    /**
     * @Route("/oidc/rp/redirect", name="openid_redirect", methods={"GET"})
     * @Route("/oidc/rp/{domain}/redirect", name="openid_domain_redirect", methods={"GET"})
     */
    public function redir(Request $request, $domain = 'default')
    {
        try {
            $tenant = $this->checkDomain();
        } catch (\Exception $e) {
            return new Response($this->renderView('error.html.twig', ['error' => 'Domain not found']), Response::HTTP_NOT_FOUND);
        }
        //$config = $this->config['rp_proxy_clients'][$domain];
        $config = $tenant->getRelyingPartyClient()->generateConfig();

        $this->logger->info('OIDC', [
            'url' => '/oidc/rp/redirect',
            'method' => 'GET',
        ]);

        $error = $request->query->get('error');
        if ($error !== null) {
            $errorDescription = $request->query->get('error_description');
            $escapedErrorDescription = htmlspecialchars($errorDescription, ENT_QUOTES | ENT_HTML5, 'UTF-8');
            return new Response($escapedErrorDescription, Response::HTTP_OK);
        }

        $code = $request->query->get('code');
        $req_id = trim(Util::base64UrlDecode($request->query->get('state')));
        $iss = $request->query->get('iss');

        // recover parameters from saved request
        $request = $this->rpDatabase->getRequest($req_id);
        $ta_id = $request['ta_id'];
        $op_id = $request['op_id'];
        $redirect_uri = $request['redirect_uri'];
        $state = $request['state'];
        $code_verifier = $request['code_verifier'];

        // resolve entity statement on federation
        try {
            $trustchain = new TrustChain($config, $this->rpDatabase, $op_id, $ta_id);
            $configuration = $trustchain->resolve();
        } catch (Exception $e) {
            return new Response($e->getMessage(), Response::HTTP_UNAUTHORIZED);
        }

        $token_endpoint = $configuration->metadata->openid_provider->token_endpoint;
        $userinfo_endpoint = $configuration->metadata->openid_provider->userinfo_endpoint;

        try {
            $tokenRequest = new TokenRequest($config, $this->hooks);
            $tokenResponse = $tokenRequest->send($token_endpoint, $code, $code_verifier);
            $access_token = $tokenResponse->access_token;

            $userinfoRequest = new UserinfoRequest($config, $configuration->metadata->openid_provider, $this->hooks);
            $userinfoResponse = $userinfoRequest->send($userinfo_endpoint, $access_token);

            $this->session->set('auth', array(
                "ta_id" => $ta_id,
                "op_id" => $op_id,
                "access_token" => $access_token,
                "redirect_uri" => $redirect_uri,
                "userinfo" => $userinfoResponse,
                "state" => $state
            ));

            $userinfoResponse->trust_anchor_id = $ta_id;
            $userinfoResponse->provider_id = $op_id;

            $responseHandler = new ResponseHandlerPlain($config);
            $responseHandler->sendResponse($redirect_uri, $userinfoResponse, $state);
        } catch (Exception $e) {
            return new Response($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @Route("/oidc/rp/introspection", name="openid_introspection", methods={"GET"})
     * @Route("/oidc/rp/{domain}/introspection", name="openid_domain_introspection", methods={"GET"})
     */
    public function introspection(Request $request, $domain = 'default')
    {
        try {
            $tenant = $this->checkDomain();
        } catch (\Exception $e) {
            return new Response($this->renderView('error.html.twig', ['error' => 'Domain not found']), Response::HTTP_NOT_FOUND);
        }
        //$config = $this->config['rp_proxy_clients'][$domain];
        $config = $tenant->getRelyingPartyClient()->generateConfig();

        $auth = $this->session->get("auth");

        $ta_id = $auth['ta_id'];
        $op_id = $auth['op_id'];
        $access_token = $auth['access_token'];

        if ($access_token === null) {
            return new Response('Session not found');
        }

        // resolve entity statement on federation
        try {
            $trustchain = new TrustChain($config, $this->rpDatabase, $op_id, $ta_id);
            $configuration = $trustchain->resolve();
        } catch (Exception $e) {
            return new Response($e->getMessage(), Response::HTTP_UNAUTHORIZED);
        }

        try {
            $introspection_endpoint = $configuration->metadata->openid_provider->introspection_endpoint;
            $introspectionRequest = new IntrospectionRequest($config);
            $introspectionResponse = $introspectionRequest->send($introspection_endpoint, $access_token);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), Response::HTTP_UNAUTHORIZED);
        }

        return new JsonResponse($introspectionResponse);

    }

    /**
     * @Route("/oidc/rp/logout", name="openid_logout", methods={"GET"})
     * @Route("/oidc/rp/{domain}/logout", name="openid_domain_logout", methods={"GET"})
     */
    public function logout(Request $request, $domain = 'default')
    {
        try {
            $tenant = $this->checkDomain();
        } catch (\Exception $e) {
            return new Response($this->renderView('error.html.twig', ['error' => 'Domain not found']), Response::HTTP_NOT_FOUND);
        }
        //$config = $this->config['rp_proxy_clients'][$domain];
        $config = $tenant->getRelyingPartyClient()->generateConfig();

        $auth = $this->session->get("auth");

        $ta_id = $auth['ta_id'];
        $op_id = $auth['op_id'];
        $access_token = $auth['access_token'];

        if ($access_token === null) {
            return new RedirectResponse('/oidc/rp/authz');
        }

        // resolve entity statement on federation
        try {
            $trustchain = new TrustChain($config, $this->rpDatabase, $op_id, $ta_id);
            $configuration = $trustchain->resolve();
        } catch (Exception $e) {
            return new Response($e->getMessage(), Response::HTTP_UNAUTHORIZED);
        }

        $revocation_endpoint = $configuration->metadata->openid_provider->revocation_endpoint;

        try {
            $revocationRequest = new RevocationRequest($config);
            $revocationResponse = $revocationRequest->send($revocation_endpoint, $access_token);
        } catch (Exception $e) {
            // do not null
        } finally {
            $this->session->remove('auth');
        }

        $post_logout_redirect_uri = $request->query->get('post_logout_redirect_uri');
        if ($post_logout_redirect_uri === null) {
            $post_logout_redirect_uri = '/oidc/rp/authz';
        }

        return new RedirectResponse($post_logout_redirect_uri);
    }

    /**
     * @Route("/oidc/login-redirect", name="openid_login_redirect", methods={"GET", "POST"})
     *
     * @return Response
     */
    public function acs(): Response
    {
        $error = 'Unauthenticated';
        $auth = $this->session->get("auth");
        try {
            if ($auth) {
                if ($targetPath = $this->session->get('_security.main.target_path')) {
                    return new RedirectResponse($targetPath);
                }
                return $this->redirectToRoute('homepage');
            }
        } catch (Exception $e) {
            if (!empty($e->getMessage())) {
                $error = $e->getMessage();
            }
            $this->logger->error($e->getMessage(), ['error_trace' => $e->getTraceAsString()]);
        }
        return new Response($this->render('error.html.twig', ['error' => $error]), 401);
    }

    /**
     * @Route("/oidc/test", name="openid_test", methods={"GET", "POST"})
     */
    public function test(Request $request, SessionInterface $session)
    {
        $logout = $request->query->has('logout');
        if ($logout) {
            $session->invalidate();
            return $this->redirectToRoute('spid_login');
        }
        return new JsonResponse([
            'session' => $session,
            'request' => $request
        ]);
    }

    /**
     * @return Tenant
     */
    private function checkDomain(): Tenant
    {
        $tenant = $this->spidClientFactory->getTenantFromRequest();
        if ($tenant instanceof Tenant) {
            return $tenant;
        }
        throw new \RuntimeException('Domain not found');
    }

}
