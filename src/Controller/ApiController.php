<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/api")
 */
class ApiController extends AbstractController
{
    /**
     * @Route("/user", name="api_user")
     */
    public function user(): Response
    {
        return new JsonResponse([
            'email' => base64_encode(json_encode($this->getUser())),
        ]);
    }

    /**
     * @Route("/profile", name="api_profile")
     */
    public function profile(): Response
    {
        return new JsonResponse($this->getUser());
    }
}