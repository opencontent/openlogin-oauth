<?php

namespace App\Controller;

use App\Entity\SpidSession;
use App\Entity\SpidUser;
use App\Spid\SpidClientFactory;
use App\Spid\SpidException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     *
     * @param Environment $twig
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function index(Environment $twig): Response
    {
        /** @var SpidUser $user */
        $user = $this->getUser();
        $session = $user->getSessions()->first();

        return new Response($twig->render('default/index.html.twig', [
            'user' => $user,
            'spid_session_id' => $session instanceof SpidSession ? $session->getSessionId() : null,
            'attributes' => $session instanceof SpidSession ? $session->getAttributes() : []
        ]));
    }

    /**
     * @Route("/health", name="health")
     */
    public function health(): Response
    {
        return new Response('');
    }

    /**
     * @Route("/metadata", name="metadata")
     * @param SpidClientFactory $spidClientFactory
     * @param Environment $twig
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function metadata(SpidClientFactory $spidClientFactory, Environment $twig): Response
    {
        try {
            $client = $spidClientFactory->makeClient();
            $tenant = $spidClientFactory->getTenantFromRequest();
            if ($tenant->hasCustomMetadata()){
                $metadata = $tenant->getCustomMetadata();
            }else{
                $metadata = $client->getSPMetadata();
            }
            $response = new Response($metadata);
            $response->headers->set('Content-Type', 'text/xml');
        }catch (SpidException $e){
            return new Response($twig->render('error.html.twig', ['error' => $e->getMessage()]), 500);
        }

        return $response;
    }
}
