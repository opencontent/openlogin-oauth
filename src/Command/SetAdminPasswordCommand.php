<?php

namespace App\Command;

use App\Entity\Admin;
use App\Repository\AdminRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\NotSupported;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

class SetAdminPasswordCommand extends Command
{
    protected static $defaultName = 'set-admin-password';
    protected static $defaultDescription = 'Set new admin password';

    private PasswordHasherFactoryInterface $passwordHasherFactory;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(PasswordHasherFactoryInterface $passwordHasherFactory, EntityManagerInterface $entityManager)
    {
        $this->passwordHasherFactory = $passwordHasherFactory;
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('password', InputArgument::REQUIRED, 'new password')
        ;
    }

    /**
     * @throws NotSupported
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $password = $input->getArgument('password');

        $encoder = $this->passwordHasherFactory->getPasswordHasher(Admin::class);
        $encodedPassword = $encoder->hash($password);


        /** @var AdminRepository $repository */
        $repository = $this->entityManager->getRepository(Admin::class);
        /** @var Admin $admin */
        $admin = $repository->findOneBy(['username' => 'admin']);
        if ($admin instanceof Admin && $repository instanceof PasswordUpgraderInterface) {
            $repository->upgradePassword($admin, $encodedPassword);
            $io->success(sprintf('The new admin password is: %s', $encodedPassword));
        }

        return Command::SUCCESS;
    }
}