<?php

namespace App\Command;

use App\Spid\SpidClientFactory;
use DOMDocument;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateIdpMetadataCommand extends Command
{
    const REGISTRY = 'https://registry.spid.gov.it/entities?entity_type=idp';

    protected static $defaultName = 'update-idp-metadata';

    protected static $defaultDescription = 'Update idp xml metadata';

    protected function configure(): void
    {
        $this
            ->addArgument(
                'target',
                InputArgument::OPTIONAL,
                'target directory',
                __DIR__ . '/../../spid/idp_metadata/'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $idpInfoList = SpidClientFactory::getIdpInfoList();
        $targetPath = realpath($input->getArgument('target'));

        $xmlDoc = new DOMDocument('1.0', 'UTF-8');
        $xmlDoc->load(self::REGISTRY);

        $entities = $xmlDoc->getElementsByTagName('EntityDescriptor');
        /** @var \DOMElement $entity */
        foreach ($entities as $entity) {
            $entityId = $entity->getAttribute('entityID');

            foreach ($idpInfoList as $idpInfo) {
                if ($idpInfo['entity_id'] === $entityId) {
                    $fileName = $targetPath . '/' . $idpInfo['ipa_entity_code'] . '.xml';
                    $output->writeln(sprintf('Store metadata %s in file %s', $entityId, $fileName));
                    $data = $xmlDoc->saveXML($entity);
                    file_put_contents($fileName, '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . PHP_EOL . $data);
                }
            }
        }

        return self::SUCCESS;
    }
}