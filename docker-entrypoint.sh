#!/bin/sh 

composer run-script post-install-cmd
umask 0002
chmod 02775 /srv/var
chown www-user /srv/var -R
chmod 02775 /srv/spid
chown www-user /srv/spid -R
exec caddy

