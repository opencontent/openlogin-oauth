<?php

if (isset($_SERVER['HTTP_X_FORWARDED_USER'])
    && base64_encode(base64_decode($_SERVER['HTTP_X_FORWARDED_USER'])) == $_SERVER['HTTP_X_FORWARDED_USER']) {
    $userData = (array)json_decode(base64_decode($_SERVER['HTTP_X_FORWARDED_USER']), true);
    echo '<h1>Decoded user data</h1>';
    echo '<pre>';
    print_r($userData);
    echo '</pre>';
    foreach ($userData as $key => $value) {
        $_SERVER['HTTP_X_FORWARDED_USER_' . strtoupper($key)] = $value;
    }
    echo '<h1>Server var with user data <em>(see code)</em></h1>';
    echo '<pre>';
    print_r($_SERVER);
    echo '</pre>';
}else{
    echo '<pre>';
    print_r($_SERVER);
    echo '</pre>';
}