$(document).ready(function () {
  function checkOrDie(value, errorMessage) {
    value = value || '';
    if (value.length === 0) {
      throw new Error(errorMessage);
    }
    return value;
  }
  let container = $('form[name="RelyingPartyClient"]')
  container.on('click', '.create-certificate-link', function (e) {
    e.preventDefault();
    let baseName = $(this).data('item');
    if (confirm("Are you sure?") !== true) {
      return;
    }
    try {
      let tenantId = checkOrDie($('#RelyingPartyClient_tenant').val(), 'Missing ente');
      let params = {
        'tenant_id': tenantId,
      }
      $.ajax({
        dataType: "json",
        method: 'get',
        contentType: 'application/json',
        headers: {'Accept': 'application/json'},
        url: '/admin/certificate/' + tenantId,
        xhrFields: {
          withCredentials: true
        },
        success: function (data){
          $('#'+baseName+'_key').val(data.key);
          $('#'+baseName+'_certificate').val(data.cert)
        },
        error: function (){
          throw new Error('Unexpected error');
        }
      });
    }catch (error){
      alert(error);
    }
  })

  let generateLink = function (baseName, suffix){
    let uri = checkOrDie($('#Tenant_openLoginUri').val(), 'Missing open login uri');
    let acs = uri.replace(/\/$/, "") + suffix;
    $('#'+baseName+'_url').val(acs);
  }
  container.on('click', '.generate-acs-link', function (e) {
    generateLink($(this).data('item'), '/acs');
    e.preventDefault();
  })
  container.on('click', '.generate-slo-link', function (e) {
    generateLink($(this).data('item'), '/slo');
    e.preventDefault();
  })
  container.on('click', '.toggle-acs', function (e) {
    let baseName = $(this).data('item');
    $('#'+baseName).find('input[type="checkbox"][name^="Tenant[attributeConsumingServices]"]').trigger('click')
    e.preventDefault();
  })
});