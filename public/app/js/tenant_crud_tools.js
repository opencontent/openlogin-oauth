$(document).ready(function () {
  function checkOrDie(value, errorMessage) {
    value = value || '';
    if (value.length === 0) {
      throw new Error(errorMessage);
    }
    return value;
  }
  let container = $('form[name="Tenant"]')
  container.on('click', '.create-certificate-link', function (e) {
    e.preventDefault();
    let baseName = $(this).data('item');
    if (confirm("Are you sure?") !== true) {
      return;
    }
    try {
      let displayName = checkOrDie($('#Tenant_displayName').val(), 'Missing display name');
      let params = {
        'commonName': displayName.replace('Comune di ', ''),
        'localityName': displayName.replace('Comune di ', ''),
        'uri': checkOrDie($('#Tenant_entityId').val(), 'Missing entity id'),
        'organizationName' : checkOrDie($('#Tenant_organizationName').val(), 'Missing organization name'),
        'organizationIdentifier' : 'PA:IT-'+checkOrDie($('#Tenant_ipaCode').val(), 'Missing ipa code'),
        'countryName' : 'IT',
        'emailAddress' : checkOrDie($('#Tenant_email').val(), 'Missing email'),
      }
      $.ajax({
        dataType: "json",
        method: 'post',
        contentType: 'application/json',
        headers: {'Accept': 'application/json'},
        url: '/admin/certificate',
        xhrFields: {
          withCredentials: true
        },
        data: JSON.stringify(params),
        success: function (data){
          $('#'+baseName+'_key').val(data.key);
          $('#'+baseName+'_certificate').val(data.cert)
        },
        error: function (){
          throw new Error('Unexpected error');
        }
      });
    }catch (error){
      alert(error);
    }
  })

  let generateLink = function (baseName, suffix){
    let uri = checkOrDie($('#Tenant_openLoginUri').val(), 'Missing open login uri');
    let acs = uri.replace(/\/$/, "") + suffix;
    $('#'+baseName+'_url').val(acs);
  }
  container.on('click', '.generate-acs-link', function (e) {
    generateLink($(this).data('item'), '/acs');
    e.preventDefault();
  })
  container.on('click', '.generate-slo-link', function (e) {
    generateLink($(this).data('item'), '/slo');
    e.preventDefault();
  })
  container.on('click', '.toggle-acs', function (e) {
    let baseName = $(this).data('item');
    $('#'+baseName).find('input[type="checkbox"][name^="Tenant[attributeConsumingServices]"]').trigger('click')
    e.preventDefault();
  })
});