#!/bin/bash

[[ $DEBUG == 1 ]] && set -x

echo "==> Checking database availability"
wait-for-it ${DB_HOST}:${DB_PORT}


if [[ $ENABLE_MIGRATIONS == 'true' ]]; then

  echo "==> Running migrations"
  php bin/console doctrine:migrations:migrate --no-interaction
fi


